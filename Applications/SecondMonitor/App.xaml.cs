﻿// ReSharper disable once RedundantUsingDirective

namespace SecondMonitor
{
    using System;
    using System.Windows;
    using Bootstrapping;
    using Ninject;
    using NLog;
    using PluginManager.Core;

    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly StandardKernel _kernel = new StandardKernel();

        private PluginsManager _pluginsManager;

        protected override void OnStartup(StartupEventArgs e)
        {
            try
            {
                base.OnStartup(e);
                AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
                _kernel.LoadCore();
                _kernel.LoadPlugins();
                _kernel.LoadConnectors();
                _pluginsManager = _kernel.Get<PluginsManager>();
                _pluginsManager.InitializePlugins();
                _pluginsManager.Start();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Application experienced an error");
            }
        }

        private void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Logger.Error("Application experienced an unhandled excpetion");
            Logger.Error(e.ExceptionObject);
        }
    }
}