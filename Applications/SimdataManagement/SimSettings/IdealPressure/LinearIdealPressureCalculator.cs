﻿namespace SecondMonitor.SimdataManagement.SimSettings.IdealPressure
{
    using DataModel.Snapshot.Systems;

    public class LinearIdealPressureCalculator : IIdealPressureByTemperatureCalculator
    {
        private const double LinearSlope = 2.0;
        public void FillIdealPressure(WheelInfo wheelInfo)
        {
            double currentPressure = wheelInfo.TyrePressure.ActualQuantity.InKpa;
            double idealMidTemperature = (wheelInfo.LeftTyreTemp.ActualQuantity.InKelvin + wheelInfo.RightTyreTemp.ActualQuantity.InKelvin) / 2.0;
            double actualMidTemperature = wheelInfo.CenterTyreTemp.ActualQuantity.InKelvin;
            double pressureChange = currentPressure - (currentPressure * (actualMidTemperature) / idealMidTemperature);

            wheelInfo.TyrePressure.IdealQuantity.InKpa = currentPressure + (pressureChange * LinearSlope);
            wheelInfo.TyrePressure.IdealQuantityWindow.InKpa = 2;
        }
    }
}