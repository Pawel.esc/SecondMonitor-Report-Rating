﻿namespace SecondMonitor.SimdataManagement.SimSettings
{
    using System.Collections.Generic;
    using Contracts.SimSettings;
    using DataModel.BasicProperties;
    using DataModel.OperationalRange;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Systems;
    using IdealPressure;

    public class SimSettingAdapter : ISimulatorDataSetVisitor
    {
        private readonly ICarSpecificationProvider _carSpecificationProvider;
        private readonly ISimSettingsFactory _settingsFactory;
        private readonly IIdealPressureByTemperatureCalculator _idealPressureByTemperatureCalculator;

        // Need to store these in case user would like to "revert to sim settings"
        private readonly OriginalSimProperties _originalSimProperties;

        private DataSourceProperties _dataSourceProperties;

        private KeyValuePair<string, CarModelProperties> _lastCarProperties;
        private KeyValuePair<string, TyreCompoundProperties> _lastCompound;

        public SimSettingAdapter(ICarSpecificationProvider carSpecificationProvider, ISimSettingsFactory settingsFactory, IIdealPressureByTemperatureCalculator idealPressureByTemperatureCalculator)
        {
            _originalSimProperties = new OriginalSimProperties();
            _carSpecificationProvider = carSpecificationProvider;
            _settingsFactory = settingsFactory;
            _idealPressureByTemperatureCalculator = idealPressureByTemperatureCalculator;
        }

        public CarModelProperties LastUsedCarProperties => _lastCarProperties.Value;

        public TyreCompoundProperties LastUsedCompound => _lastCompound.Value;

        public List<TyreCompoundProperties> GlobalTyreCompoundsProperties
        {
            get => _dataSourceProperties.TyreCompoundsProperties;
            set
            {
                _dataSourceProperties.TyreCompoundsProperties = value;
                _lastCarProperties = new KeyValuePair<string, CarModelProperties>(string.Empty, _lastCarProperties.Value);
                _lastCompound = new KeyValuePair<string, TyreCompoundProperties>(string.Empty, _lastCompound.Value);
                _carSpecificationProvider.SaveDataSourceProperties(_dataSourceProperties);
            }
        }

        public void Visit(SimulatorDataSet simulatorDataSet)
        {
            if (simulatorDataSet?.PlayerInfo?.CarName == null)
            {
                return;
            }

            if (_dataSourceProperties?.SourceName != simulatorDataSet.Source)
            {
                ReloadDataSourceProperties(simulatorDataSet.Source);
                if (!simulatorDataSet.SimulatorSourceInfo.GlobalTyreCompounds && _dataSourceProperties.TyreCompoundsProperties.Count > 0)
                {
                    _dataSourceProperties.TyreCompoundsProperties.Clear();
                }
            }

            CarModelProperties carModel = GetCarModelProperties(simulatorDataSet);
            ApplyCarMode(simulatorDataSet, carModel);
        }

        public void Reset()
        {
        }

        public void ReplaceCarModelProperties(CarModelProperties carModelProperties)
        {
            _dataSourceProperties.ReplaceCarModel(carModelProperties);
            _lastCarProperties = new KeyValuePair<string, CarModelProperties>(carModelProperties.Name, carModelProperties);
            _carSpecificationProvider.SaveDataSourceProperties(_dataSourceProperties);
        }

        private void ApplyCarMode(SimulatorDataSet simulatorDataSet, CarModelProperties carModel)
        {
            Wheels wheels = simulatorDataSet.PlayerInfo.CarInfo.WheelsInfo;
            if (!simulatorDataSet.InputInfo.WheelAngleFilled)
            {
                simulatorDataSet.InputInfo.WheelAngleFilled = true;
                simulatorDataSet.InputInfo.WheelAngle = ((carModel.WheelRotation) / 2.0) * simulatorDataSet.InputInfo.SteeringInput;
            }

            TyreCompoundProperties tyreCompound = GetTyreCompound(simulatorDataSet, wheels.FrontLeft, wheels.RearLeft, carModel);
            ApplyWheelProperty(wheels.FrontLeft, true, carModel, tyreCompound);
            ApplyWheelProperty(wheels.FrontRight, true, carModel, tyreCompound);
            ApplyWheelProperty(wheels.RearLeft, false, carModel, tyreCompound);
            ApplyWheelProperty(wheels.RearRight, false, carModel, tyreCompound);
        }

        private void ApplyWheelProperty(WheelInfo wheelInfo, bool isFront, CarModelProperties carModel, TyreCompoundProperties tyreCompound)
        {
            wheelInfo.BrakeTemperature.IdealQuantity.InCelsius = carModel.OptimalBrakeTemperature.InCelsius;
            wheelInfo.BrakeTemperature.IdealQuantityWindow.InCelsius = carModel.OptimalBrakeTemperatureWindow.InCelsius;

            if (string.IsNullOrWhiteSpace(wheelInfo.TyreType) || wheelInfo.TyreType == "\u0001")
            {
                return;
            }

            if (tyreCompound.DetermineIdealPressureByTemperature)
            {
                _idealPressureByTemperatureCalculator.FillIdealPressure(wheelInfo);
            }
            else
            {
                wheelInfo.TyrePressure.IdealQuantity.InKpa = isFront ? tyreCompound.FrontIdealPressure.InKpa : tyreCompound.RearIdealPressure.InKpa;
                wheelInfo.TyrePressure.IdealQuantityWindow.InKpa = isFront ? tyreCompound.FrontIdealPressureWindow.InKpa : tyreCompound.RearIdealPressureWindow.InKpa;
            }

            wheelInfo.LeftTyreTemp.IdealQuantity.InCelsius = isFront ? tyreCompound.FrontIdealTemperature.InCelsius : tyreCompound.RearIdealTemperature.InCelsius;
            wheelInfo.LeftTyreTemp.IdealQuantityWindow.InCelsius = isFront ? tyreCompound.FrontIdealTemperatureWindow.InCelsius : tyreCompound.RearIdealTemperatureWindow.InCelsius;

            wheelInfo.RightTyreTemp.IdealQuantity.InCelsius = isFront ? tyreCompound.FrontIdealTemperature.InCelsius : tyreCompound.RearIdealTemperature.InCelsius;
            wheelInfo.RightTyreTemp.IdealQuantityWindow.InCelsius = isFront ? tyreCompound.FrontIdealTemperatureWindow.InCelsius : tyreCompound.RearIdealTemperatureWindow.InCelsius;

            wheelInfo.CenterTyreTemp.IdealQuantity.InCelsius = isFront ? tyreCompound.FrontIdealTemperature.InCelsius : tyreCompound.RearIdealTemperature.InCelsius;
            wheelInfo.CenterTyreTemp.IdealQuantityWindow.InCelsius = isFront ? tyreCompound.FrontIdealTemperatureWindow.InCelsius : tyreCompound.RearIdealTemperatureWindow.InCelsius;

            wheelInfo.TyreCoreTemperature.IdealQuantity.InCelsius = isFront ? tyreCompound.FrontIdealTemperature.InCelsius : tyreCompound.RearIdealTemperature.InCelsius;
            wheelInfo.TyreCoreTemperature.IdealQuantityWindow.InCelsius = isFront ? tyreCompound.FrontIdealTemperatureWindow.InCelsius : tyreCompound.RearIdealTemperatureWindow.InCelsius;

            wheelInfo.TyreWear.NoWearWearLimit = tyreCompound.NoWearLimit;
            wheelInfo.TyreWear.LightWearLimit = tyreCompound.LowWearLimit;
            wheelInfo.TyreWear.HeavyWearLimit = tyreCompound.HeavyWearLimit;
        }

        private TyreCompoundProperties GetTyreCompound(SimulatorDataSet simulatorDataSet, WheelInfo frontWheel, WheelInfo rearWheel, CarModelProperties carModel)
        {
            string compoundName = frontWheel.TyreType;
            if (_lastCompound.Key == compoundName)
            {
                return _lastCompound.Value;
            }

            TyreCompoundProperties tyreCompound = carModel.GetTyreCompound(compoundName);
            if (tyreCompound != null)
            {
                StoreSimDefaultValues(frontWheel, rearWheel);
                ApplyDefaultSimPropertiesToTyreCompound(tyreCompound, frontWheel, rearWheel);
                _lastCompound = new KeyValuePair<string, TyreCompoundProperties>(tyreCompound.CompoundName, tyreCompound);
                return tyreCompound;
            }

            tyreCompound = _dataSourceProperties.GetTyreCompound(compoundName);
            if (tyreCompound == null)
            {
                tyreCompound = CreateTyreCompound(simulatorDataSet.Source, frontWheel, rearWheel);
                if (simulatorDataSet.SimulatorSourceInfo.GlobalTyreCompounds)
                {
                    _dataSourceProperties.AddTyreCompound(tyreCompound);
                }
                else
                {
                    carModel.AddTyreCompound(tyreCompound);
                }

                _carSpecificationProvider.SaveDataSourceProperties(_dataSourceProperties);
            }

            StoreSimDefaultValues(frontWheel, rearWheel);
            ApplyDefaultSimPropertiesToTyreCompound(tyreCompound, frontWheel, rearWheel);

            _lastCompound = new KeyValuePair<string, TyreCompoundProperties>(tyreCompound.CompoundName, tyreCompound);
            return tyreCompound;
        }

        private void StoreSimDefaultValues(WheelInfo frontWheel, WheelInfo rearWheel)
        {
            _originalSimProperties.IdealBrakeTemperature = frontWheel.BrakeTemperature.IdealQuantity.InCelsius;
            _originalSimProperties.IdealBrakeTemperatureWindow = frontWheel.BrakeTemperature.IdealQuantityWindow.InCelsius;

            _originalSimProperties.FrontIdealTyreTemp = frontWheel.CenterTyreTemp.IdealQuantity.InCelsius;
            _originalSimProperties.FrontIdealTyreTempWindow = frontWheel.CenterTyreTemp.IdealQuantityWindow.InCelsius;
            _originalSimProperties.FrontIdealTyrePressure = frontWheel.TyrePressure.IdealQuantity.InKpa;
            _originalSimProperties.FrontIdealTyrePressureWindow = frontWheel.TyrePressure.IdealQuantityWindow.InKpa;

            _originalSimProperties.RearIdealTyreTemp = rearWheel.CenterTyreTemp.IdealQuantity.InCelsius;
            _originalSimProperties.RearIdealTyreTempWindow = rearWheel.CenterTyreTemp.IdealQuantityWindow.InCelsius;
            _originalSimProperties.RearIdealTyrePressure = rearWheel.TyrePressure.IdealQuantity.InKpa;
            _originalSimProperties.RearIdealTyrePressureWindow = rearWheel.TyrePressure.IdealQuantityWindow.InKpa;
        }

        private CarModelProperties GetCarModelProperties(SimulatorDataSet simulatorDataSet)
        {
            string carName = simulatorDataSet.PlayerInfo.CarName;

            if (carName == _lastCarProperties.Key)
            {
                return _lastCarProperties.Value;
            }

            CarModelProperties carModelProperties = _dataSourceProperties.GetCarModel(carName);

            if (carModelProperties == null || carModelProperties.OriginalContainsOptimalTemperature != simulatorDataSet.SimulatorSourceInfo.TelemetryInfo.ContainsOptimalTemperatures)
            {
                StoreSimDefaultValues(simulatorDataSet.PlayerInfo.CarInfo.WheelsInfo.FrontLeft, simulatorDataSet.PlayerInfo.CarInfo.WheelsInfo.RearLeft);
                _dataSourceProperties.CarModelsProperties.RemoveAll(x => x.Name == carName);
                carModelProperties = CreateNewCarModelProperties(carName, simulatorDataSet);
            }

            _lastCarProperties = new KeyValuePair<string, CarModelProperties>(carModelProperties.Name, carModelProperties);
            return carModelProperties;
        }

        private void ApplyDefaultSimPropertiesToTyreCompound(TyreCompoundProperties tyreCompound, WheelInfo frontWheel, WheelInfo rearWheel)
        {
            if (!tyreCompound.IsSimOriginal)
            {
                return;
            }

            tyreCompound.FrontIdealPressure = Pressure.FromKiloPascals(_originalSimProperties.FrontIdealTyrePressure);
            tyreCompound.FrontIdealPressureWindow = Pressure.FromKiloPascals(_originalSimProperties.FrontIdealTyrePressureWindow);

            tyreCompound.RearIdealPressure = Pressure.FromKiloPascals(_originalSimProperties.RearIdealTyrePressure);
            tyreCompound.RearIdealPressureWindow = Pressure.FromKiloPascals(_originalSimProperties.RearIdealTyrePressureWindow);

            tyreCompound.RearIdealTemperature = Temperature.FromCelsius(_originalSimProperties.FrontIdealTyreTemp);
            tyreCompound.RearIdealTemperatureWindow = Temperature.FromCelsius(_originalSimProperties.FrontIdealTyreTempWindow);

            tyreCompound.FrontIdealTemperature = Temperature.FromCelsius(_originalSimProperties.RearIdealTyreTemp);
            tyreCompound.FrontIdealTemperatureWindow = Temperature.FromCelsius(_originalSimProperties.RearIdealTyreTempWindow);

            tyreCompound.NoWearLimit = frontWheel.TyreWear.NoWearWearLimit;
            tyreCompound.LowWearLimit = frontWheel.TyreWear.LightWearLimit;
            tyreCompound.HeavyWearLimit = frontWheel.TyreWear.HeavyWearLimit;
        }

        private CarModelProperties CreateNewCarModelProperties(string carName, SimulatorDataSet simulatorDataSet)
        {
            CarModelProperties newCarModelProperties = new CarModelProperties { Name = carName };
            ApplyDefaultSimPropertiesToCarModelProperties(newCarModelProperties, simulatorDataSet);

            _dataSourceProperties.AddCarModel(newCarModelProperties);
            _carSpecificationProvider.SaveDataSourceProperties(_dataSourceProperties);
            return newCarModelProperties;
        }

        private void ApplyDefaultSimPropertiesToCarModelProperties(CarModelProperties carModel, SimulatorDataSet simulatorDataSet)
        {
            carModel.OriginalContainsOptimalTemperature = simulatorDataSet.SimulatorSourceInfo.TelemetryInfo.ContainsOptimalTemperatures;
            carModel.OptimalBrakeTemperature = Temperature.FromCelsius(_originalSimProperties.IdealBrakeTemperature);
            carModel.OptimalBrakeTemperatureWindow = Temperature.FromCelsius(_originalSimProperties.IdealBrakeTemperatureWindow);
        }

        private TyreCompoundProperties CreateTyreCompound(string simulatorName, WheelInfo frontWheel, WheelInfo rearWheel)
        {
            var simSettings = _settingsFactory.Create(simulatorName);
            TyreCompoundProperties tyreCompound = new TyreCompoundProperties()
            {
                CompoundName = frontWheel.TyreType,
                IsSimOriginal = true,
                DetermineIdealPressureByTemperature = simSettings.DetermineIdealPressureByTemperature,
            };

            ApplyDefaultSimPropertiesToTyreCompound(tyreCompound, frontWheel, rearWheel);

            return tyreCompound;
        }

        private void ReloadDataSourceProperties(string sourceName)
        {
            _dataSourceProperties = _carSpecificationProvider.GetSimulatorProperties(sourceName);
        }

        public void ResetCarSettings(CarModelProperties originalModel, SimulatorDataSet dataSet)
        {
            ApplyDefaultSimPropertiesToCarModelProperties(originalModel, dataSet);
            originalModel.TyreCompoundsProperties.Clear();
            originalModel.TyreCompoundsProperties.Add(CreateTyreCompound(dataSet.Source, dataSet.PlayerInfo.CarInfo.WheelsInfo.FrontLeft, dataSet.PlayerInfo.CarInfo.WheelsInfo.RearLeft));
        }
    }
}