﻿namespace SecondMonitor.Remote.Connector
{
    using System;
    using System.IO;
    using System.Net;
    using System.Threading;
    using System.Threading.Tasks;
    using Common.Adapter;
    using Common.Model;
    using DataModel.Snapshot;
    using LiteNetLib;
    using LiteNetLib.Utils;
    using NLog;
    using ProtoBuf;
    using SecondMonitor.PluginsConfiguration.Common.Controller;
    using SecondMonitor.PluginsConfiguration.Common.DataModel;

    internal class RemoteClient : IRemoteClient
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly Lazy<IPluginSettingsProvider> _pluginSettingsProvider;
        private readonly IDatagramPayloadUnPacker _datagramPayloadUnPacker;
        private readonly EventBasedNetListener _listener;
        private readonly NetManager _client;

        private bool _firstPackage;
        private NetPeer _serverPeer;
        private Task _clientCheckLoopTask;
        private IPEndPoint _serverEndPoint;

        public RemoteClient(Lazy<IPluginSettingsProvider> pluginSettingsProvider, IDatagramPayloadUnPacker datagramPayloadUnPacker)
        {
            _listener = new EventBasedNetListener();
            _listener.ConnectionRequestEvent += request => request.AcceptIfKey(DatagramPayload.Version);
            _client = new NetManager(_listener)
            {
                BroadcastReceiveEnabled = true
            };
            _pluginSettingsProvider = pluginSettingsProvider;
            _datagramPayloadUnPacker = datagramPayloadUnPacker;
            _firstPackage = true;
        }

        public event EventHandler Disconnected;

        public event EventHandler<SimulatorDataSet> SessionStarted;
        public event EventHandler<SimulatorDataSet> DataLoaded;

        private RemoteConfiguration RemoteConfiguration => _pluginSettingsProvider.Value.RemoteConfiguration;
        private bool IsConnected { get; set; }

        public bool TryConnect()
        {
            if (!RemoteConfiguration.IsRemoteConnectorEnabled)
            {
                return false;
            }

            Logger.Info("Remote Connector Is Enabled");

            if (_serverPeer?.ConnectionState == ConnectionState.Connected)
            {
                Logger.Info($"Connected to {_serverPeer.EndPoint.Address}:{_serverPeer.EndPoint.Address}");
                return true;
            }

            if (_clientCheckLoopTask == null || this._clientCheckLoopTask.Status != TaskStatus.Running)
            {
                StartConnector();
            }

            if (RemoteConfiguration.IsFindInLanEnabled)
            {
                TryConnectUsingDiscovery();
            }
            else
            {
                try
                {
                    _serverEndPoint = NetUtils.MakeEndPoint(RemoteConfiguration.HostAddress, RemoteConfiguration.Port);
                    ConnectToServer();
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                }
            }

            Thread.Sleep(200);

            if (_serverPeer?.ConnectionState != ConnectionState.Connected)
            {
                Logger.Info("Connection state is still in progress");
                return false;
            }

            Logger.Info($"Connected to {_serverPeer.EndPoint.Address}:{_serverPeer.EndPoint.Address}");
            return true;
        }

        private void TryConnectUsingDiscovery()
        {
            if (_serverEndPoint != null)
            {
                ConnectToServer();
                return;
            }

            Logger.Info($"Sending Discovery Request: {DatagramPayload.Version}");

            NetDataWriter netDataWriter = new NetDataWriter();
            netDataWriter.Put(DatagramPayload.Version);
            _client.SendBroadcast(netDataWriter, RemoteConfiguration.Port);
            netDataWriter.Reset();
        }

        private void ConnectToServer()
        {
            if (_serverEndPoint == null)
            {
                Logger.Info("Server end point is null");
                return;
            }

            if (_serverPeer?.ConnectionState == ConnectionState.Outgoing)
            {
                Logger.Info($"Trying to Connecto to Server {_serverEndPoint.Address}:{_serverEndPoint.Port}, server peer is In Progress");
                return;
            }

            Logger.Info($"Trying to Connecto to Server {_serverEndPoint.Address}:{_serverEndPoint.Port}");
            _serverPeer = _client.Connect(_serverEndPoint, DatagramPayload.Version);
        }

        public void StartClientLoop()
        {
            IsConnected = true;
        }

        private void StartConnector()
        {
            _client.Start();
            SubscribeEventBasedListener();
            _clientCheckLoopTask = ClientCheckLoop();
        }

        private void SubscribeEventBasedListener()
        {
            if (_listener == null)
            {
                return;
            }

            _listener.PeerConnectedEvent += EventBasedNetListenerOnPeerConnectedEvent;
            _listener.PeerDisconnectedEvent += EventBasedNetListenerOnPeerDisconnectedEvent;
            _listener.NetworkReceiveEvent += EventBasedNetListenerOnNetworkReceiveEvent;
            _listener.NetworkReceiveUnconnectedEvent += EventBasedNetListenerOnNetworkReceiveUnconnectedEvent;
        }

        private void EventBasedNetListenerOnNetworkReceiveUnconnectedEvent(IPEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType)
        {
            if (messageType != UnconnectedMessageType.Broadcast)
            {
                return;
            }

            string discoveryMessage = reader.GetString();

            Logger.Info($"Discovery ResponseFrom from {remoteEndPoint.Address}:{remoteEndPoint.Port}, Message:{discoveryMessage}");
            if (discoveryMessage != DatagramPayload.Version)
            {
                Logger.Info("Version Do not Match - Ignoring");
                return;
            }

            _serverEndPoint = remoteEndPoint;
            Logger.Info("Version Do Match - Will be used as server");
        }

        private void EventBasedNetListenerOnNetworkReceiveEvent(NetPeer peer, NetDataReader reader, DeliveryMethod deliveryMethod)
        {
            //Not the used connector = end
            if (!IsConnected)
            {
                return;
            }

            DatagramPayload payload;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                memoryStream.Write(reader.RawData, reader.UserDataOffset, reader.UserDataSize);
                memoryStream.Seek(0, SeekOrigin.Begin);
                payload = Serializer.Deserialize<DatagramPayload>(memoryStream);
            }

            SimulatorDataSet simulatorDataSet = _datagramPayloadUnPacker.UnpackDatagramPayload(payload);

            if (payload.PayloadKind == DatagramPayloadKind.SessionStart || _firstPackage)
            {
                _firstPackage = false;
                SessionStarted?.Invoke(this, simulatorDataSet);
                Logger.Info($"New Session Started: {simulatorDataSet.SessionInfo.TrackInfo.TrackFullName}");
            }
            else
            {
                DataLoaded?.Invoke(this, simulatorDataSet);
            }
        }

        private void EventBasedNetListenerOnPeerDisconnectedEvent(NetPeer peer, DisconnectInfo disconnectinfo)
        {
            if (disconnectinfo.Reason == DisconnectReason.ConnectionFailed)
            {
                return;
            }

            Logger.Info($"Server Disconnected - {disconnectinfo.Reason}");
            Disconnected?.Invoke(this, EventArgs.Empty);
        }

        private void EventBasedNetListenerOnPeerConnectedEvent(NetPeer peer)
        {
        }

        private async Task ClientCheckLoop()
        {
            try
            {
                while (true)
                {
                    await Task.Delay(5);
                    _client.PollEvents();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "RemoteClient CheckLoop fell over");
            }
            finally
            {
                Logger.Info("Disconnecting RemoteClient due to error");
                _client.DisconnectAll();
                Disconnected?.Invoke(this, EventArgs.Empty);
            }
        }
    }
}
