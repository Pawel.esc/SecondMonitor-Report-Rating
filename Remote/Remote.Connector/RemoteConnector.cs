﻿namespace SecondMonitor.Remote.Connector
{
    using System;
    using System.Threading.Tasks;
    using Contracts.NInject;
    using DataModel.Snapshot;
    using PluginManager.GameConnector;

    public class RemoteConnector : IGameConnector
    {
        private readonly IRemoteClient _remoteClient;

        public RemoteConnector(IRemoteClient remoteClient)
        {
            _remoteClient = remoteClient;
            _remoteClient.Disconnected += ClientDisconnected;
            _remoteClient.SessionStarted += ClientSessionStarted;
            _remoteClient.DataLoaded += ClientDataLoaded;
        }

        public event EventHandler<DataEventArgs> DataLoaded;
        public event EventHandler<EventArgs> ConnectedEvent;
        public event EventHandler<EventArgs> Disconnected;
        public event EventHandler<DataEventArgs> SessionStarted;
        public event EventHandler<MessageArgs> DisplayMessage
        {
            add { } remove { }
        }

        public bool IsConnected { get; private set; }

        public bool TryConnect()
        {
            return _remoteClient.TryConnect();
        }

        public Task FinnishConnectorAsync()
        {
            return Task.CompletedTask;
        }

        public void StartConnectorLoop()
        {
            IsConnected = true;
            ConnectedEvent?.Invoke(this, EventArgs.Empty);
            _remoteClient.StartClientLoop();
        }

        private void ClientDisconnected(object sender, EventArgs eventArgs)
        {
            Disconnected?.Invoke(this, new EventArgs());
        }

        private void ClientSessionStarted(object sender, SimulatorDataSet simulatorDataSet)
        {
            SessionStarted?.Invoke(this, new DataEventArgs(simulatorDataSet));
        }

        private void ClientDataLoaded(object sender, SimulatorDataSet simulatorDataSet)
        {
            DataLoaded?.Invoke(this, new DataEventArgs(simulatorDataSet));
        }
    }
}