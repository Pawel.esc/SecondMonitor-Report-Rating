﻿namespace SecondMonitor.Remote.Connector
{
    using Ninject;
    using Ninject.Activation;
    using Ninject.Modules;
    using PluginsConfiguration.Common.Controller;
    using PluginsConfiguration.Common.DataModel;

    public class RemoteConnectorModule : NinjectModule
    {
        public override void Load()
        {
            Bind<RemoteClient>().To<RemoteClient>();
            Bind<RemoteClientV2>().To<RemoteClientV2>();
            Bind<IRemoteClient>().ToMethod(GetRemoteClient);
        }

        private static IRemoteClient GetRemoteClient(IContext context)
        {
            var settingsProvider = context.Kernel.Get<IPluginSettingsProvider>();
            IRemoteClient remoteClient = null;
            
            switch (settingsProvider.RemoteConfiguration.ProtocolVersion)
            {
                case ProtocolVersion.V1:
                    remoteClient = context.Kernel.Get<RemoteClient>();
                    break;
                case ProtocolVersion.V2:
                    remoteClient = context.Kernel.Get<RemoteClientV2>();
                    break;
            }

            return remoteClient;
        }
    }
}
