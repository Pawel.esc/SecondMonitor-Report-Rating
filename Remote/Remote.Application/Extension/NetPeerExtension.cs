﻿namespace SecondMonitor.Remote.Application.Extension
{
    using LiteNetLib;

    public static class NetPeerExtension
    {
        public static string GetIdentifier(this NetPeer netPeer)
        {
            return $"Host:{netPeer.EndPoint.Address}:{netPeer.EndPoint.Port}";
        }
    }
}