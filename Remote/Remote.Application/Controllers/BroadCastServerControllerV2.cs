namespace SecondMonitor.Remote.Application.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading;
    using System.Threading.Tasks;
    using Common.Adapter;
    using Common.Model;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using LiteNetLib;
    using LiteNetLib.Utils;
    using NLog;
    using PluginsConfiguration.Common.Controller;
    using ViewModels;

    public class BroadCastServerControllerV2 : IBroadCastServerController
    {
        private const string defaultSource = "Remote";
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly IPluginSettingsProvider _pluginSettingsProvider;
        private readonly IServerOverviewViewModel _serverOverviewViewModel;
        private readonly IDatagramPayloadPackerV2 _datagramPayloadPacker;
        private readonly Queue<NetPeer> _newPeers;
        private readonly TimeSpan _packedDelay;
        private readonly TimeSpan _playerInfoDelay;
        private readonly TimeSpan _driversInfoDelay;
        private readonly object _queueLock = new object();
        private EventBasedNetListener _eventBasedNetListener;
        private NetManager _server;
        private Task _checkLoop;
        private CancellationTokenSource _serverCheckLoopSource;
        private Stopwatch _lastPackedStopWatch;
        private Stopwatch _playerInfoDelayTimer;
        private Stopwatch _driversInfoDelayTimer;

        public BroadCastServerControllerV2(
            IPluginSettingsProvider pluginSettingsProvider,
            IServerOverviewViewModel serverOverviewViewModel,
            IDatagramPayloadPackerV2 datagramPayloadPacker)
        {
            _newPeers = new Queue<NetPeer>();
            _pluginSettingsProvider = pluginSettingsProvider;
            _serverOverviewViewModel = serverOverviewViewModel;
            _datagramPayloadPacker = datagramPayloadPacker;
            var broadcastLimitSettings = pluginSettingsProvider.RemoteConfiguration.BroadcastLimitSettings;
            _packedDelay = TimeSpan.FromMilliseconds(broadcastLimitSettings.MinimumPackageInterval);
            _playerInfoDelay = TimeSpan.FromMilliseconds(broadcastLimitSettings.PlayerTimingPackageInterval);
            _driversInfoDelay = TimeSpan.FromMilliseconds(broadcastLimitSettings.OtherDriversTimingPackageInterval);
        }
        
        private bool IsNetworkConservationEnabled => _pluginSettingsProvider.RemoteConfiguration.BroadcastLimitSettings.IsEnabled;

        private bool ShouldSendPlayerInfo => !IsNetworkConservationEnabled || _playerInfoDelayTimer.Elapsed > _playerInfoDelay;

        private bool ShouldSendDriversInfo => !IsNetworkConservationEnabled || _driversInfoDelayTimer.Elapsed > _driversInfoDelay;

        public Task StartControllerAsync()
        {
            StartListeningServer();
            return Task.CompletedTask;
        }

        public async Task StopControllerAsync()
        {
            await StopListeningServer();
        }

        public void SendSessionStartedPackage(SimulatorDataSet simulatorDataSet)
        {
            _lastPackedStopWatch.Restart();
            using (var memoryStream = new MemoryStream())
            {
                _datagramPayloadPacker.PackSessionStart(memoryStream, simulatorDataSet);
                SendPackage(memoryStream);
            }
        }

        public void SendRegularDataPackage(SimulatorDataSet simulatorDataSet)
        {
            if (!IsNetworkConservationEnabled || _lastPackedStopWatch.Elapsed <= _packedDelay)
            {
                return;
            }

            using (var memoryStream = new MemoryStream())
            {
                lock (_queueLock)
                {
                    if (_newPeers.Any())
                    {
                        this._datagramPayloadPacker.PackSessionStart(memoryStream, simulatorDataSet);
                        while (this._newPeers.Count > 0)
                        {
                            SendPackage(memoryStream, this._newPeers.Dequeue());
                        }
                    }
                }

                memoryStream.SetLength(0);
                _datagramPayloadPacker.PackSessionInfo(memoryStream, simulatorDataSet);
                SendPackage(memoryStream);

                if (ShouldSendPlayerInfo)
                {
                    UpdateViewModelInputs(simulatorDataSet.InputInfo, simulatorDataSet.Source);
                    memoryStream.SetLength(0);
                    _datagramPayloadPacker.PackPlayerInfo(memoryStream, simulatorDataSet);
                    SendPackage(memoryStream);
                    _playerInfoDelayTimer.Restart();
                }

                if (ShouldSendDriversInfo)
                {
                    memoryStream.SetLength(0);
                    _datagramPayloadPacker.PackDriversTimingInfo(memoryStream, simulatorDataSet);
                    SendPackage(memoryStream);
                    _driversInfoDelayTimer.Restart();
                }
            }
            
            _lastPackedStopWatch.Restart();
        }

        private void StartListeningServer()
        {
            _eventBasedNetListener = new EventBasedNetListener();
            // TODO validate requests to join differently
            _eventBasedNetListener.ConnectionRequestEvent += request => request.AcceptIfKey(DatagramPayload.Version2);
            _server = new NetManager(_eventBasedNetListener)
            {
                BroadcastReceiveEnabled = true,
                EnableStatistics = true
            };

            SubscribeEventBasedListener();
            int port = _pluginSettingsProvider.RemoteConfiguration.Port;

            Logger.Info($"Starting Listening Server on port:{port}");
            if (!_server.Start(port))
            {
                Logger.Info($"Unable to start server, unknown error");
                return;
            }

            _serverOverviewViewModel.IsRunning = true;
            Logger.Info($"Server Started:{port}");

            _serverCheckLoopSource = new CancellationTokenSource();
            _checkLoop = ServerLoop(_serverCheckLoopSource.Token);
            _lastPackedStopWatch = Stopwatch.StartNew();
            _playerInfoDelayTimer = Stopwatch.StartNew();
            _driversInfoDelayTimer = Stopwatch.StartNew();
        }

        private async Task StopListeningServer()
        {
            Logger.Info("Stopping Listening Server");
            UnSubscribeEventBasedListener();
            _server.Stop();
            _serverCheckLoopSource.Cancel();

            try
            {
                await _checkLoop;
            }
            catch (TaskCanceledException)
            {
            }
        }

        private void SubscribeEventBasedListener()
        {
            if (_eventBasedNetListener == null)
            {
                return;
            }

            _eventBasedNetListener.PeerConnectedEvent += EventBasedNetListenerOnPeerConnectedEvent;
            _eventBasedNetListener.PeerDisconnectedEvent += EventBasedNetListenerOnPeerDisconnectedEvent;
            _eventBasedNetListener.NetworkReceiveEvent += EventBasedNetListenerOnNetworkReceiveEvent;
            _eventBasedNetListener.NetworkReceiveUnconnectedEvent += EventBasedNetListenerOnNetworkReceiveUnconnectedEvent;
            _eventBasedNetListener.NetworkErrorEvent += EventBasedNetListenerOnNetworkErrorEvent;
        }

        private void UnSubscribeEventBasedListener()
        {
            if (_eventBasedNetListener == null)
            {
                return;
            }

            _eventBasedNetListener.PeerConnectedEvent -= EventBasedNetListenerOnPeerConnectedEvent;
            _eventBasedNetListener.PeerDisconnectedEvent -= EventBasedNetListenerOnPeerDisconnectedEvent;
            _eventBasedNetListener.NetworkReceiveEvent -= EventBasedNetListenerOnNetworkReceiveEvent;
            _eventBasedNetListener.NetworkReceiveUnconnectedEvent -= EventBasedNetListenerOnNetworkReceiveUnconnectedEvent;
            _eventBasedNetListener.NetworkErrorEvent -= EventBasedNetListenerOnNetworkErrorEvent;
        }

        private async Task ServerLoop(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                await Task.Delay(5, token);
                _server.PollEvents();
                if (_lastPackedStopWatch.Elapsed.TotalMilliseconds > 1000)
                {
                    SendKeepAlivePacket();
                    _lastPackedStopWatch.Restart();
                }
            }
        }

        private void SendKeepAlivePacket()
        {
            UpdateViewModelInputs(null, defaultSource);
            using (var memoryStream = new MemoryStream())
            {
                _datagramPayloadPacker.PackHandshake(memoryStream);
                SendPackage(memoryStream);
            }
        }

        private void SendPackage(MemoryStream source)
        {
            NetDataWriter package = new NetDataWriter();
            package.Put(source.ToArray());
            _server.SendToAll(package, DeliveryMethod.ReliableOrdered);
            _serverOverviewViewModel.SampleStats(_server.Statistics);
        }

        private void SendPackage(MemoryStream source, NetPeer peer)
        {
            NetDataWriter package = new NetDataWriter();
            package.Put(source.ToArray());
            peer.Send(package, DeliveryMethod.ReliableOrdered);
        }

        private void EventBasedNetListenerOnNetworkErrorEvent(IPEndPoint endpoint, SocketError socketErrorCode)
        {
            Logger.Info($"Socket Error -  {endpoint.Address}:{endpoint.Port} - Error Code :{socketErrorCode}");
        }

        private void EventBasedNetListenerOnNetworkReceiveUnconnectedEvent(IPEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType)
        {
            if (messageType != UnconnectedMessageType.Broadcast)
            {
                return;
            }

            string discoveryMessage = reader.GetString();
            Logger.Info($"Discovery Request from {remoteEndPoint.Address}:{remoteEndPoint.Port}, Message:{discoveryMessage}");

            if (discoveryMessage != DatagramPayload.Version2)
            {
                Logger.Info("Versions do not match - ignored");
                return;
            }

            NetDataWriter response = new NetDataWriter();
            response.Put(DatagramPayload.Version2);
            _server.SendBroadcast(response, remoteEndPoint.Port);
        }

        private void EventBasedNetListenerOnNetworkReceiveEvent(NetPeer peer, NetDataReader reader, DeliveryMethod deliveryMethod)
        {
            Logger.Info($"Data:{reader.GetString()}");
        }

        private void EventBasedNetListenerOnPeerDisconnectedEvent(NetPeer peer, DisconnectInfo disconnectInfo)
        {
            Logger.Info($"Client Disconnected: {peer.EndPoint.Address}:{peer.EndPoint.Port}");
            _serverOverviewViewModel.RemoveClient(peer);
        }

        private void EventBasedNetListenerOnPeerConnectedEvent(NetPeer peer)
        {
            Logger.Info($"New Client Connected: {peer.EndPoint.Address}:{peer.EndPoint.Port}");
            _serverOverviewViewModel.AddClient(peer);
            lock (_queueLock)
            {
                _newPeers.Enqueue(peer);
            }
        }

        private void UpdateViewModelInputs(InputInfo inputInfo, string source)
        {
            if (inputInfo != null)
            {
                _serverOverviewViewModel.ThrottleInput = inputInfo.ThrottlePedalPosition * 100;
                _serverOverviewViewModel.ClutchInput = inputInfo.ClutchPedalPosition * 100;
                _serverOverviewViewModel.BrakeInput = inputInfo.BrakePedalPosition * 100;
            }

            _serverOverviewViewModel.ConnectedSimulator = source == defaultSource ? "None" : source;
        }
    }
}