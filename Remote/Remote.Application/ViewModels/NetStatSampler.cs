﻿namespace SecondMonitor.Remote.Application.ViewModels
{
    using System;

    internal class NetStatSampler
    {
        private const int SampleCapacity = 64;

        private static readonly string[] SizeSuffixes =
            { "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

        private readonly long[] _bytesSamples = new long[SampleCapacity];
        private readonly long[] _millisecondSamples = new long[SampleCapacity];

        private int _head = 0;
        private int _tail = 0;
        private int _size = 0;

        public long BytesPerSecond { get; private set; } = 0L;

        public string SpeedPerSecond
        {
            get
            {
                int decimalPlaces = 1;
                int i = 0;
                decimal dValue = BytesPerSecond;
                while (Math.Round(dValue, decimalPlaces) >= 1000)
                {
                    dValue /= 1024;
                    i++;
                }

                return string.Format("{0:n" + decimalPlaces + "} {1}", dValue, SizeSuffixes[i]);
            }
        }

        public void AddSample(long bytes, long millis)
        {
            if (_size == SampleCapacity)
            {
                _head = (_head + 1) % SampleCapacity;
            }
            else
            {
                _size++;
            }

            _bytesSamples[_tail] = bytes;
            _millisecondSamples[_tail] = millis;

            _tail = (_tail + 1) % SampleCapacity;

            int index = _head;

            long bytesTotal = 0L;
            long millisecondsTotal = 0L;
            for (var i = 0; i <= _size; i++, index = (index + 1) % SampleCapacity)
            {
                bytesTotal += _bytesSamples[index];
                millisecondsTotal += _millisecondSamples[index];
            }

            // Ensuring value is calculated as bytes per second instead of per millisecond
            BytesPerSecond = bytesTotal * 1000L / millisecondsTotal;
        }
    }
}