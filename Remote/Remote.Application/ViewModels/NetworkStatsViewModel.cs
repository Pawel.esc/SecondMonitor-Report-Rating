﻿namespace SecondMonitor.Remote.Application.ViewModels
{
    using System;
    using System.Diagnostics;
    using LiteNetLib;
    using SecondMonitor.ViewModels;

    public class NetworkStatsViewModel : AbstractViewModel<NetStatistics>, INetworkStatsViewModelExtended
    {
        private readonly Stopwatch _stopwatch = Stopwatch.StartNew();
        private readonly NetStatSampler _uploadSampler = new NetStatSampler();
        private readonly NetStatSampler _downloadSampler = new NetStatSampler();

        private string _uploadDownloadSpeed;

        public long UploadBytesPerSecond { get; private set; }
        public long DownloadBytesPerSecond { get; private set;  }

        public string UploadDownloadSpeed
        {
            get => _uploadDownloadSpeed;
            private set => SetProperty(ref _uploadDownloadSpeed, value);
        }

        protected override void ApplyModel(NetStatistics model)
        {
            var elapsedSample = _stopwatch.ElapsedMilliseconds;
            _stopwatch.Restart();
            var uploadSample = model.BytesSent;
            var downloadSample = model.BytesReceived;
            model.Reset();

            _uploadSampler.AddSample(uploadSample, elapsedSample);
            _downloadSampler.AddSample(downloadSample, elapsedSample);

            UploadBytesPerSecond = _uploadSampler.BytesPerSecond;
            DownloadBytesPerSecond = _downloadSampler.BytesPerSecond;
            UploadDownloadSpeed = $"↑ {_uploadSampler.SpeedPerSecond}/↓ {_downloadSampler.SpeedPerSecond}";
        }

        public override NetStatistics SaveToNewModel()
        {
            throw new NotImplementedException();
        }
    }
}
