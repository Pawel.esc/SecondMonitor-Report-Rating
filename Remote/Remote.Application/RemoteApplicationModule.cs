﻿namespace SecondMonitor.Remote.Application
{
    using Controllers;
    using Ninject;
    using Ninject.Activation;
    using Ninject.Modules;
    using PluginsConfiguration.Common.Controller;
    using PluginsConfiguration.Common.DataModel;
    using ViewModels;

    public class RemoteApplicationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<BroadCastServerController>().To<BroadCastServerController>();
            Bind<BroadCastServerControllerV2>().To<BroadCastServerControllerV2>();
            Bind<IServerOverviewViewModel>().To<ServerOverviewViewModel>().InSingletonScope();
            Bind<IClientViewModel>().To<ClientViewModel>();
            Bind<INetworkStatsViewModelExtended, INetworkStatsViewModel>().To<NetworkStatsViewModel>().InSingletonScope();

            Bind<IBroadCastServerController>().ToMethod(GetBroadcastServerController);
        }

        private static IBroadCastServerController GetBroadcastServerController(IContext context)
        {
            var settingsProvider = context.Kernel.Get<IPluginSettingsProvider>();
            IBroadCastServerController broadCastServerController = null;
            
            switch (settingsProvider.RemoteConfiguration.ProtocolVersion)
            {
                case ProtocolVersion.V1:
                    broadCastServerController = context.Kernel.Get<BroadCastServerController>();
                    break;
                case ProtocolVersion.V2:
                    broadCastServerController = context.Kernel.Get<BroadCastServerControllerV2>();
                    break;
            }

            return broadCastServerController;
        }
    }
}