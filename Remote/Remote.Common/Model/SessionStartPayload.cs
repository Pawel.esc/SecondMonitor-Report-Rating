namespace SecondMonitor.Remote.Common.Model
{
    using DataModel.Snapshot;
    using ProtoBuf;

    [ProtoContract]
    public class SessionStartPayload
    {
        [ProtoMember(1, IsRequired = true)]
        public string Source { get; set; }
        
        [ProtoMember(2, IsRequired = true)]
        public SimulatorSourceInfo SimulatorSourceInfo { get; set; }
        [ProtoMember(3, IsRequired = true)]
        public SessionInfo SessionInfo { get; set; }
    }
}