namespace SecondMonitor.Remote.Common.Model
{
    public enum DatagramPayloadKindV2 : byte
    {
        Heartbeat,
        SessionStart,
        SessionInfo,
        PlayerInfo,
        DriversInfo
    }
}
