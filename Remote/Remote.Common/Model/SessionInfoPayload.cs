namespace SecondMonitor.Remote.Common.Model
{
    using DataModel.Snapshot;
    using ProtoBuf;

    [ProtoContract]
    public class SessionInfoPayload
    {
        [ProtoMember(1, IsRequired = true)]
        public SessionInfo SessionInfo { get; set; }
    }
}