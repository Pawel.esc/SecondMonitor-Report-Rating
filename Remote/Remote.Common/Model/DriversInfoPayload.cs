namespace SecondMonitor.Remote.Common.Model
{
    using DataModel.Snapshot.Drivers;
    using ProtoBuf;

    [ProtoContract]
    public class DriversInfoPayload
    {
        [ProtoMember(1, IsRequired = true)]
        
        public DriverInfo[] DriversInfo { get; set; }
        
        [ProtoMember(2, IsRequired = true)]
        public DriverInfo LeaderInfo { get; set; }
    }
}
