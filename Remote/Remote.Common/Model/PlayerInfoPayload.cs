namespace SecondMonitor.Remote.Common.Model
{
    using DataModel.BasicProperties;
    using DataModel.Snapshot.Drivers;
    using ProtoBuf;

    [ProtoContract]
    public class PlayerInfoPayload
    {
        [ProtoMember(1, IsRequired = true)]
        public InputInfo InputInfo { get; set; }
        
        [ProtoMember(2, IsRequired = true)]
        public DriverInfo PlayerInfo { get; set; }
    }
}