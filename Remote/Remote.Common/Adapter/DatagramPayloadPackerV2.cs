namespace SecondMonitor.Remote.Common.Adapter
{
    using System.IO;
    using DataModel.Snapshot;
    using Model;
    using ProtoBuf;
    
    internal class DatagramPayloadPackerV2 : IDatagramPayloadPackerV2
    {
        public void PackHandshake(Stream destination)
        {
            PackDatagramType(destination, DatagramPayloadKindV2.Heartbeat);
        }
        
        public void PackSessionStart(Stream destination, SimulatorDataSet simulatorDataSet)
        {
            PackDatagramType(destination, DatagramPayloadKindV2.SessionStart);
            var payload = new SessionStartPayload()
            {
                Source = simulatorDataSet.Source,
                SimulatorSourceInfo = simulatorDataSet.SimulatorSourceInfo,
                SessionInfo = simulatorDataSet.SessionInfo
            };

            Serializer.Serialize(destination, payload);
        }

        public void PackSessionInfo(Stream destination, SimulatorDataSet simulatorDataSet)
        {
            PackDatagramType(destination, DatagramPayloadKindV2.SessionInfo);
            var payload = new SessionInfoPayload()
            {
                SessionInfo = simulatorDataSet.SessionInfo
            };

            Serializer.Serialize(destination, payload);
        }

        public void PackPlayerInfo(Stream destination, SimulatorDataSet simulatorDataSet)
        {
            PackDatagramType(destination, DatagramPayloadKindV2.PlayerInfo);
            var payload = new PlayerInfoPayload()
            {
                InputInfo = simulatorDataSet.InputInfo,
                PlayerInfo = simulatorDataSet.PlayerInfo
            };
            
            Serializer.Serialize(destination, payload);
        }

        public void PackDriversTimingInfo(Stream destination, SimulatorDataSet simulatorDataSet)
        {
            PackDatagramType(destination, DatagramPayloadKindV2.DriversInfo);
            var payload = new DriversInfoPayload()
            {
                DriversInfo = simulatorDataSet.DriversInfo,
                LeaderInfo = simulatorDataSet.LeaderInfo
            };

            Serializer.Serialize(destination, payload);
        }

        private void PackDatagramType(Stream destination, DatagramPayloadKindV2 payloadKindV2)
        {
            destination.WriteByte((byte)payloadKindV2);
        }
    }
}