namespace SecondMonitor.Remote.Common.Adapter
{
    using System.IO;
    using DataModel.Snapshot;
    public interface IDatagramPayloadPackerV2
    {
        void PackHandshake(Stream destination);
        void PackSessionStart(Stream destination, SimulatorDataSet simulatorDataSet);
        void PackSessionInfo(Stream destination, SimulatorDataSet simulatorDataSet);
        void PackPlayerInfo(Stream destination, SimulatorDataSet simulatorDataSet);
        void PackDriversTimingInfo(Stream destination, SimulatorDataSet simulatorDataSet);
    }
}