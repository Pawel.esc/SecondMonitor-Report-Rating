namespace SecondMonitor.Remote.Common.Adapter
{
    using System.IO;
    using Model;
    public interface IDatagramPayloadUnpackerV2
    {
        DatagramPayloadKindV2 UnpackKind(Stream source);

        SessionStartPayload UnpackSessionStartPayload(Stream source);

        SessionInfoPayload UnpackSessionInfoPayload(Stream source);

        PlayerInfoPayload UnpackPlayerInfoPayload(Stream source);

        DriversInfoPayload UnpackDriversInfoPayload(Stream source);
    }
}