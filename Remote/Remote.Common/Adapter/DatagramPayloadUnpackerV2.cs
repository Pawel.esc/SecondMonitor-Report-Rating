namespace SecondMonitor.Remote.Common.Adapter
{
    using System.IO;
    using DataModel.Snapshot.Drivers;
    using Model;
    using ProtoBuf;

    internal class DatagramPayloadUnpackerV2 : IDatagramPayloadUnpackerV2
    {
        public DatagramPayloadKindV2 UnpackKind(Stream source)
        {
            return (DatagramPayloadKindV2)source.ReadByte();
        }

        public SessionStartPayload UnpackSessionStartPayload(Stream source)
        {
            return Serializer.Deserialize<SessionStartPayload>(source);
        }

        public SessionInfoPayload UnpackSessionInfoPayload(Stream source)
        {
            return Serializer.Deserialize<SessionInfoPayload>(source);
        }

        public PlayerInfoPayload UnpackPlayerInfoPayload(Stream source)
        {
            return Serializer.Deserialize<PlayerInfoPayload>(source);
        }

        public DriversInfoPayload UnpackDriversInfoPayload(Stream source)
        {
            return Serializer.Deserialize<DriversInfoPayload>(source);
        }
    }
}