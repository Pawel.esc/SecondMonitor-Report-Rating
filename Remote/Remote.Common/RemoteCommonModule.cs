﻿namespace SecondMonitor.Remote.Common
{
    using Adapter;
    using Comparators;
    using Model;
    using Ninject.Modules;
    using ViewModels.PluginsSettings;

    public class RemoteCommonModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IDatagramPayloadUnPacker>().To<DatagramPayloadUnPacker>();
            Bind<IDatagramPayloadPacker>().To<DatagramPayloadPacker>();
            Bind<IDatagramPayloadPackerV2>().To<DatagramPayloadPackerV2>();
            Bind<IDatagramPayloadUnpackerV2>().To<DatagramPayloadUnpackerV2>();
            Bind<ISimulatorSourceInfoComparator>().To<SimulatorSourceInfoComparator>();
            Rebind<IHostAddressValidator>().To<LiteNetLibHostAddressValidator>();
        }
    }
}