﻿namespace SecondMonitor.Timing.Application.SessionTiming.Drivers.Presentation.ViewModel
{
    public enum PitStopCountGeneralizationKind
    {
        None,
        SameAsPlayer,
        LessThanPlayer,
        MoreThanPLayer,
    }
}