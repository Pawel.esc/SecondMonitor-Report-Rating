﻿namespace SecondMonitor.Timing.Application.SessionTiming.Drivers.Presentation.ViewModel
{
    using System;
    using Common.SessionTiming.Drivers;
    using Common.SessionTiming.Drivers.Lap;
    using Contracts.Extensions;
    using ViewModels;

    public class LapTimeDeltaViewModel : AbstractViewModel
    {
        private string _deltaToSessionBest;
        private string _deltaToPersonalBest;
        private string _deltaToClassBest;
        private bool _isCurrentlySessionBest;
        private bool _isCurrentlySessionClassBest;
        private bool _isCurrentlyPersonalBest;
        private string _deltaToCombined;
        private TimeSpan _deltaToPersonalBestSpan;
        private TimeSpan _deltaToSessionBestSpan;
        private string _predictedLapTime;
        private TimeSpan _predictedLapTimeSpan;

        public string DeltaToSessionBest
        {
            get => _deltaToSessionBest;
            set => SetProperty(ref _deltaToSessionBest, value);
        }

        public string DeltaToPersonalBest
        {
            get => _deltaToPersonalBest;
            set => SetProperty(ref _deltaToPersonalBest, value);
        }

        public bool IsCurrentlySessionBest
        {
            get => _isCurrentlySessionBest;
            set => SetProperty(ref _isCurrentlySessionBest, value);
        }

        public bool IsCurrentlySessionClassBest
        {
            get => _isCurrentlySessionClassBest;
            set => SetProperty(ref _isCurrentlySessionClassBest, value);
        }

        public bool IsCurrentlyPersonalBest
        {
            get => _isCurrentlyPersonalBest;
            set => SetProperty(ref _isCurrentlyPersonalBest, value);
        }

        public string DeltaToClassBest
        {
            get => _deltaToClassBest;
            set => SetProperty(ref _deltaToClassBest, value);
        }

        public string DeltaToCombined
        {
            get => _deltaToCombined;
            set => SetProperty(ref _deltaToCombined, value);
        }

        public string PredictedLapTime
        {
            get => _predictedLapTime;
            set => SetProperty(ref _predictedLapTime, value);
        }

        public void Update(DriverTimingViewModel driverTimingViewModel)
        {
            DriverTiming driverTiming = driverTimingViewModel.DriverTiming;
            if (driverTiming.CurrentLap == null || double.IsNaN(driverTiming.CurrentLap.CompletedDistance) || driverTimingViewModel.InPits || driverTiming.CurrentLap.LapTelemetryInfo?.PortionTimes == null)
            {
                IsCurrentlyPersonalBest = false;
                DeltaToPersonalBest = string.Empty;
                IsCurrentlySessionBest = false;
                DeltaToSessionBest = string.Empty;
                IsCurrentlySessionClassBest = false;
                DeltaToClassBest = string.Empty;
                DeltaToCombined = string.Empty;
                PredictedLapTime = string.Empty;
                return;
            }

            TimeSpan lastPortionTime = driverTiming.CurrentLap.LapTelemetryInfo.PortionTimes.GetLastRecordedTime();
            int currentPortionIndex = driverTiming.CurrentLap.LapTelemetryInfo.PortionTimes.LastTrackedPortion;

            if (lastPortionTime == TimeSpan.Zero)
            {
                return;
            }

            UpdateDeltaToPersonal(driverTiming, lastPortionTime, currentPortionIndex);
            if (_deltaToPersonalBestSpan != TimeSpan.Zero && driverTiming.BestLap != null)
            {
                _predictedLapTimeSpan = driverTiming.BestLap.LapTime + _deltaToPersonalBestSpan;
                PredictedLapTime = driverTiming.CurrentLap.Valid ? TimeSpanFormatHelper.FormatTimeSpan(_predictedLapTimeSpan) : $"(I) {TimeSpanFormatHelper.FormatTimeSpan(_predictedLapTimeSpan)}";
                UpdateDeltaToSession(driverTiming);
                UpdateDeltaToClassBest(driverTiming);
            }

            if (IsCurrentlySessionBest)
            {
                DeltaToCombined = _deltaToSessionBest;
            }
            else if (IsCurrentlySessionClassBest)
            {
                DeltaToCombined = _deltaToClassBest;
            }
            else
            {
                DeltaToCombined = _deltaToPersonalBest; }
        }

        private void UpdateDeltaToPersonal(DriverTiming driverTiming, TimeSpan currentPortionTime, int currentPortionIndex)
        {
            ILapInfo lapToCompare = driverTiming.BestLap;
            TimeSpan bestLapAtDistance = GetLapTimeAtDistance(driverTiming, lapToCompare, currentPortionIndex);
            if (bestLapAtDistance == TimeSpan.Zero)
            {
                IsCurrentlyPersonalBest = false;
                DeltaToPersonalBest = string.Empty;
                return;
            }

            _deltaToPersonalBestSpan = currentPortionTime - bestLapAtDistance;

            DeltaToPersonalBest = TimeSpanFormatHelper.FormatTimeSpanOnlySeconds(_deltaToPersonalBestSpan, true);
            IsCurrentlyPersonalBest = bestLapAtDistance > currentPortionTime;
        }

        private void UpdateDeltaToSession(DriverTiming driverTiming)
        {
            ILapInfo lapToCompare = driverTiming.Session.SessionBestTimesViewModel.BestLap;
            if (lapToCompare == null || lapToCompare.LapTime == TimeSpan.Zero)
            {
                IsCurrentlySessionBest = false;
                DeltaToSessionBest = string.Empty;
                _deltaToSessionBestSpan = TimeSpan.Zero;
                return;
            }

            _deltaToSessionBestSpan = _predictedLapTimeSpan - lapToCompare.LapTime;
            DeltaToSessionBest = TimeSpanFormatHelper.FormatTimeSpanOnlySeconds(_deltaToSessionBestSpan, true);
            IsCurrentlySessionBest = _predictedLapTimeSpan < lapToCompare.LapTime;
        }

        private void UpdateDeltaToClassBest(DriverTiming driverTiming)
        {
            ILapInfo lapToCompare = driverTiming.Session.GetBestTimesForClass(driverTiming.CarClassId).BestLap;
            if (lapToCompare == null || lapToCompare.LapTime == TimeSpan.Zero)
            {
                IsCurrentlySessionBest = false;
                DeltaToSessionBest = string.Empty;
                _deltaToSessionBestSpan = TimeSpan.Zero;
                return;
            }

            DeltaToClassBest = TimeSpanFormatHelper.FormatTimeSpanOnlySeconds(_predictedLapTimeSpan - lapToCompare.LapTime, true);
            IsCurrentlySessionClassBest = _predictedLapTimeSpan < lapToCompare.LapTime;
        }

        private TimeSpan GetLapTimeAtDistance(DriverTiming driverTiming, ILapInfo lapToCompare, int currentPortionIndex)
        {
            if (lapToCompare == null || driverTiming.CurrentLap == null || lapToCompare.LapTelemetryInfo == null || lapToCompare.LapTelemetryInfo.IsPortionTimesPurged)
            {
                return TimeSpan.Zero;
            }

            TimeSpan bestLapAtDistance = lapToCompare.LapTelemetryInfo.PortionTimes.GetTimeAtPortion(currentPortionIndex);
            return bestLapAtDistance;
        }
    }
}