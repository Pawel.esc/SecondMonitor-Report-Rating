﻿namespace SecondMonitor.Timing.Application.SessionTiming.Drivers.Presentation.ViewModel
{
    using Common.SessionTiming.Drivers;
    using DataModel.Snapshot;
    using ViewModels;

    public interface IPitStopInfoViewModel : IViewModel<DriverTiming>
    {
        bool HasValidInformation { get; }

        bool CanBeUsed(SimulatorDataSet dataSet);

        void UpdatePitInformation(DriverTiming driverTiming, SimulatorDataSet dataSet);
    }
}