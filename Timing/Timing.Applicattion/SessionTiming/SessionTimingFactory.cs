﻿namespace SecondMonitor.Timing.Application.SessionTiming
{
    using System.Collections.Generic;
    using Application.ViewModel;
    using Common.SessionTiming;
    using Common.SessionTiming.Drivers;
    using Common.SessionTiming.Drivers.Lap.SectorTracker;
    using Controllers;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Drivers;
    using Drivers.ViewModel;
    using LapTimings;
    using NLog;
    using Rating.Application.Championship;
    using Rating.Application.Rating.RatingProvider;
    using Telemetry;
    using TrackRecords.Controller;
    using ViewModel;
    using ViewModels.Factory;
    using ViewModels.SessionEvents;
    using ViewModels.Settings;

    public class SessionTimingFactory
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly ISessionTelemetryControllerFactory _sessionTelemetryControllerFactory;
        private readonly ISessionRatingProvider _sessionRatingProvider;
        private readonly ITrackRecordsController _trackRecordsController;
        private readonly IChampionshipCurrentEventPointsProvider _championshipCurrentEventPointsProvider;
        private readonly ISessionEventProvider _sessionEventProvider;
        private readonly DriverLapSectorsTrackerFactory _driverLapSectorsTrackerFactory;
        private readonly MapManagementController _mapManagementController;
        private readonly IViewModelFactory _viewModelFactory;
        private readonly IBestLapEventProvider _bestLapEventProvider;
        private readonly ISettingsProvider _settingsProvider;

        public SessionTimingFactory(ISessionTelemetryControllerFactory sessionTelemetryControllerFactory, ISessionRatingProvider sessionRatingProvider, ITrackRecordsController trackRecordsController, IChampionshipCurrentEventPointsProvider championshipCurrentEventPointsProvider,
        ISessionEventProvider sessionEventProvider, DriverLapSectorsTrackerFactory driverLapSectorsTrackerFactory, MapManagementController mapManagementController, IViewModelFactory viewModelFactory, IBestLapEventProvider bestLapEventProvider, ISettingsProvider settingsProvider)
        {
            _viewModelFactory = viewModelFactory;
            _bestLapEventProvider = bestLapEventProvider;
            _settingsProvider = settingsProvider;
            _sessionTelemetryControllerFactory = sessionTelemetryControllerFactory;
            _sessionRatingProvider = sessionRatingProvider;
            _trackRecordsController = trackRecordsController;
            _championshipCurrentEventPointsProvider = championshipCurrentEventPointsProvider;
            _sessionEventProvider = sessionEventProvider;
            _driverLapSectorsTrackerFactory = driverLapSectorsTrackerFactory;
            _mapManagementController = mapManagementController;
        }

        public SessionTimingController Create(SimulatorDataSet dataSet, TimingApplicationViewModel timingApplicationViewModel, bool invalidateFirstLap)
        {
            Logger.Info($"New Seesion Started :{dataSet.SessionInfo.SessionType.ToString()}");
            SessionTimingController timing = new SessionTimingController(timingApplicationViewModel, _sessionTelemetryControllerFactory.Create(dataSet), _sessionRatingProvider,
                _trackRecordsController, _championshipCurrentEventPointsProvider, _sessionEventProvider, _driverLapSectorsTrackerFactory, dataSet, _mapManagementController, _viewModelFactory, _bestLapEventProvider, _settingsProvider);
            Dictionary<string, DriverTiming> drivers = new Dictionary<string, DriverTiming>();
            foreach (DriverInfo driverInfo in dataSet.DriversInfo)
            {
                if (drivers.ContainsKey(driverInfo.DriverSessionId))
                {
                    continue;
                }

                //drivers[driverInfo.DriverSessionId] = DriverTiming.FromModel(driverInfo, timing, _driverLapSectorsTrackerFactory, false);
                drivers[driverInfo.DriverSessionId] = DriverTiming.FromModel(driverInfo, timing, _driverLapSectorsTrackerFactory, _bestLapEventProvider, invalidateFirstLap, _settingsProvider);
            }

            timing.SetDrivers(dataSet, drivers.Values);
            return timing;
        }
    }
}