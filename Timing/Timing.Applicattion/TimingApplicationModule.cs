﻿namespace SecondMonitor.Timing.Application
{
    using Common.SessionTiming;
    using Contracts.Session;
    using Contracts.TrackMap;
    using Contracts.TrackRecords;
    using Controllers;
    using DataModel.Snapshot;
    using LapTimings;
    using Layout;
    using Ninject;
    using Ninject.Extensions.Factory;
    using Ninject.Extensions.NamedScope;
    using Ninject.Modules;
    using PitBoard.Controller;
    using PitBoard.DataProviders;
    using PitBoard.ViewModels;
    using SessionTiming;
    using Telemetry;
    using TimingGrid.Columns;
    using TrackRecords.Controller;
    using ViewModel;
    using ViewModels.CarStatus;
    using ViewModels.Layouts.Factory;
    using ViewModels.PitBoard.Controller;
    using ViewModels.Track.SituationOverview.Controller;

    public class TimingApplicationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ITrackRecordsRepositoryFactory>().To<TrackRecordsRepositoryFactory>();
            Bind<ITrackRecordsController, ITrackRecordsProvider>().To<TrackRecordsController>().InSingletonScope();
            Bind<ISessionEventsController>().To<SessionEventsController>();

            Bind<PitBoardViewModel>().ToSelf();
            Bind<IPitBoardController, PitBoardController>().To<PitBoardController>().InSingletonScope();
            Bind<IAutonomousPitBoardDataProvider>().To<GapPitBoardProvider>();
            Bind<IAutonomousPitBoardDataProvider>().To<YellowAheadPitBoardProvider>();
            Bind<IAutonomousPitBoardDataProvider>().To<ClearToRejoinBoardProvider>();
            Bind<SessionTimingFactory>().ToSelf();
            Bind<TimingApplicationViewModel, IPaceProvider>().To<TimingApplicationViewModel>().InSingletonScope();
            Bind<DriverDetailsController>().ToSelf().InSingletonScope();
            Bind<ISessionTelemetryControllerFactory>().To<SessionTelemetryControllerFactory>();
            Bind<IMapManagementController, MapManagementController>().To<MapManagementController>().InSingletonScope();
            Bind<SessionInfoViaTimingViewModelProvider>().To<SessionInfoViaTimingViewModelProvider>().InSingletonScope();
            Bind<ISessionInformationProvider>().ToMethod(c => c.Kernel.Get<SessionInfoViaTimingViewModelProvider>()).WhenInjectedExactlyInto<SituationOverviewController>();

            Bind<SettingsWindowController>().ToSelf().Named("SettingsWindow").DefinesNamedScope("SettingsWindow");
            Bind<IDefaultLayoutFactory>().To<TimingApplicationDefaultLayoutFactory>().WhenAnyAncestorNamed("SettingsWindow").InNamedScope("SettingsWindow");
            Bind<ILayoutElementNamesProvider>().To<TimingApplicationElementNamesProvider>().WhenAnyAncestorNamed("SettingsWindow").InNamedScope("SettingsWindow");
            Bind<ISimulatorDataSetVisitor>().To<ForceSingleClassVisitor>();
            Bind<IBestLapEventProvider>().To<BestLapEventProvider>().InSingletonScope();
            Bind<ISimSettingControllerFactory>().ToFactory();

            Bind<IColumnVisibilityAdapter>().To<ClassVisibilityAdapter>().WithMetadata(BindingMetadataIds.AdapterColumnName, "Class Ribbon");
            Bind<IColumnVisibilityAdapter>().To<ClassVisibilityAdapter>().WithMetadata(BindingMetadataIds.AdapterColumnName, "Class");
            Bind<IColumnVisibilityAdapter>().To<RatingVisibilityAdapter>().WithMetadata(BindingMetadataIds.AdapterColumnName, "Rating");
            Bind<IColumnVisibilityAdapter>().To<PointsVisibilityAdapter>().WithMetadata(BindingMetadataIds.AdapterColumnName, "Points");
            Bind<IColumnVisibilityAdapter>().To<TyreVisibilityAdapter>().WithMetadata(BindingMetadataIds.AdapterColumnName, "Tyre Kind Ribbon");
            Bind<IColumnVisibilityAdapter>().To<PitsVisibilityAdapter>().WithMetadata(BindingMetadataIds.AdapterColumnName, "Pit Information");
            Bind<IColumnVisibilityAdapter>().To<ReputationVisibilityAdapter>().WithMetadata(BindingMetadataIds.AdapterColumnName, "Reputation");
        }
    }
}