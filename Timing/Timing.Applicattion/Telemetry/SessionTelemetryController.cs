﻿namespace SecondMonitor.Timing.Application.Telemetry
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Common.SessionTiming;
    using Common.SessionTiming.Drivers.Lap;
    using Contracts.TaskQueue;
    using DataModel.BasicProperties;
    using DataModel.Snapshot.Drivers;
    using DataModel.Telemetry;
    using NLog;
    using SecondMonitor.Telemetry.TelemetryManagement.DTO;
    using SecondMonitor.Telemetry.TelemetryManagement.Repository;
    using ViewModels.Settings;

    public class SessionTelemetryController : ISessionTelemetryController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly ITelemetryRepository _telemetryRepository;
        private readonly IBestLapEventProvider _bestLapEventProvider;
        private readonly ISettingsProvider _settingsProvider;
        private readonly TaskQueue _taskQueue;
        private SessionInfoDto _sessionInfoDto;
        private ILapInfo _overallBest;

        public SessionTelemetryController(string trackName, SessionType sessionType, ITelemetryRepository telemetryRepository, IBestLapEventProvider bestLapEventProvider, ISettingsProvider settingsProvider)
        {
            _telemetryRepository = telemetryRepository;
            _bestLapEventProvider = bestLapEventProvider;
            _settingsProvider = settingsProvider;
            SessionIdentifier = $"{DateTime.Now:yy-MM-dd-HH-mm}-{trackName}-{sessionType}-{Guid.NewGuid()}";
            _taskQueue = new TaskQueue(true);
        }

        public string SessionIdentifier { get; }

        public void TrySaveLapTelemetry(ILapInfo lapInfo)
        {
            _taskQueue.EnqueueTask(() => TrySaveLapTelemetryInternal(lapInfo));
        }

        private Task<bool> TrySaveLapTelemetryInternal(ILapInfo lapInfo)
        {
            if (lapInfo?.LapTelemetryInfo == null)
            {
                return Task.FromResult(false);
            }

            if (_sessionInfoDto == null)
            {
                _sessionInfoDto = CreateSessionInfo(lapInfo);
            }

            Logger.Info($"Saving Telemetry for Lap:{lapInfo.LapNumber}");
            if (lapInfo.LapTelemetryInfo.IsTelemetryPurged)
            {
                Logger.Error("Lap Is PURGED! Cannot Save");
                return Task.FromResult(false);
            }

            Task<bool> returnTask = Task.Run(() => SaveLapTelemetrySync(lapInfo));
            return returnTask;
        }

        public Task StartControllerAsync()
        {
            _bestLapEventProvider.BestLapPersonalChanged += BestLapEventProviderOnBestLapPersonalChanged;
            return Task.CompletedTask;
        }

        public Task StopControllerAsync()
        {
            _bestLapEventProvider.BestLapPersonalChanged -= BestLapEventProviderOnBestLapPersonalChanged;
            return Task.CompletedTask;
        }

        private bool SaveLapTelemetrySync(ILapInfo lapInfo)
        {
            return TrySaveLap(lapInfo, string.Empty);
        }

        private bool SaveLapTelemetrySync(ILapInfo lapInfo, string lapName)
        {
            if (_sessionInfoDto == null)
            {
                _sessionInfoDto = CreateSessionInfo(lapInfo);
            }

            return TrySaveLap(lapInfo, lapName);
        }

        private bool TrySaveLap(ILapInfo lapInfo, string customName)
        {
            try
            {
                Logger.Info($"Saving Telemetry Lap - {customName}");
                LapSummaryDto lapSummaryDto = CreateLapSummary(lapInfo, customName);
                LapTelemetryDto lapTelemetryDto = CreateLapTelemetryDto(lapInfo, lapSummaryDto);

                _sessionInfoDto.LapsSummary.Where(x => x.FileName == lapSummaryDto.FileName).ToList().ForEach(x => _sessionInfoDto.LapsSummary.Remove(x));
                _sessionInfoDto.LapsSummary.Add(lapSummaryDto);

                _telemetryRepository.SaveRecentSessionInformation(_sessionInfoDto, SessionIdentifier);
                _telemetryRepository.SaveRecentSessionLap(lapTelemetryDto, SessionIdentifier, lapSummaryDto.FileName);
                Logger.Info($"Completed Saving Telemetry Lap - {customName}");
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Uanble to Save Telemetry");
                return false;
            }
        }

        private LapTelemetryDto CreateLapTelemetryDto(ILapInfo lapInfo, LapSummaryDto lapSummaryDto)
        {
            var lapTelemetry = lapInfo.LapTelemetryInfo.TimedTelemetrySnapshots.Snapshots.ToList();

            TimedTelemetrySnapshot fistSnapshotsByDistance = lapTelemetry.First(x => x.PlayerData.LapDistance < _sessionInfoDto.LayoutLength * 0.5);

            LapTelemetryDto lapTelemetryDto = new LapTelemetryDto(LapTelemetryDto.CurrentVersion)
            {
                LapSummary = lapSummaryDto,
                DataPoints = lapTelemetry.Skip(lapTelemetry.ToList().IndexOf(fistSnapshotsByDistance)).ToList()
            };
            return lapTelemetryDto;
        }

        private LapSummaryDto CreateLapSummary(ILapInfo lapInfo)
        {
            return CreateLapSummary(lapInfo, string.Empty);
        }

        private LapSummaryDto CreateLapSummary(ILapInfo lapInfo, string lapName)
        {
            LapSummaryDto lapSummaryDto = new LapSummaryDto()
            {
                Id = lapInfo.LapGuid.ToString(),
                LapNumber = lapInfo.LapNumber,
                LapTimeSeconds = lapInfo.LapTime.TotalSeconds,
                Sector1Time = lapInfo.Sector1?.Duration ?? TimeSpan.Zero,
                Sector2Time = lapInfo.Sector2?.Duration ?? TimeSpan.Zero,
                Sector3Time = lapInfo.Sector3?.Duration ?? TimeSpan.Zero,
                SessionIdentifier = SessionIdentifier,
                Simulator = _sessionInfoDto.Simulator,
                TrackName = _sessionInfoDto.TrackName,
                LayoutName = _sessionInfoDto.LayoutName,
                Stint = lapInfo.StintNumber,
                IsPlayer = lapInfo.Driver.IsPlayer,
            };

            if (!string.IsNullOrEmpty(lapName))
            {
                lapSummaryDto.CustomDisplayName = lapName;
                lapSummaryDto.FileName = string.Join("_", (lapName + TelemetryRepository.FileSuffix).Split(Path.GetInvalidFileNameChars()));
            }
            else
            {
                lapSummaryDto.FileName = $"{lapSummaryDto.LapNumber}-{lapSummaryDto.Id}{TelemetryRepository.FileSuffix}";
            }

            return lapSummaryDto;
        }

        private SessionInfoDto CreateSessionInfo(ILapInfo lapInfo)
        {
            DriverInfo driverToUse = lapInfo.Driver.Session.Player?.DriverInfo ?? lapInfo.Driver.DriverInfo;
            SessionInfoDto sessionInfoDto = new SessionInfoDto()
            {
                CarName = driverToUse.CarName,
                Id = SessionIdentifier,
                TrackName = lapInfo.Driver.Session.LastSet.SessionInfo.TrackInfo.TrackName,
                LayoutName = lapInfo.Driver.Session.LastSet.SessionInfo.TrackInfo.TrackLayoutName,
                LayoutLength = lapInfo.Driver.Session.LastSet.SessionInfo.TrackInfo.LayoutLength.InMeters,
                PlayerName = driverToUse.DriverLongName,
                Simulator = lapInfo.Driver.Session.LastSet.Source,
                SessionRunDateTime = DateTime.Now,
                LapsSummary = new List<LapSummaryDto>(),
                SessionType = lapInfo.Driver.Session.SessionType.ToString()
            };
            return sessionInfoDto;
        }

        private void BestLapEventProviderOnBestLapPersonalChanged(object sender, BestLapChangedArgs e)
        {
            if (!e.NewLap.Driver.IsPlayer && e.NewLap.LapTelemetryInfo?.TimedTelemetrySnapshots != null && e.NewLap.LapTelemetryInfo.TimedTelemetrySnapshots.Snapshots.Any())
            {
                _taskQueue.EnqueueTask(() => BestLapEventProviderOnBestLapPersonalChangedInternal(e));
            }
        }

        private async Task BestLapEventProviderOnBestLapPersonalChangedInternal(BestLapChangedArgs e)
        {
            ILapInfo bestLap = e.NewLap;
            if (!bestLap.Valid || bestLap.LapTime == TimeSpan.Zero)
            {
                return;
            }

            try
            {
                if (!bestLap.Driver.IsPlayer && (_overallBest == null || _overallBest.LapTime == TimeSpan.Zero || _overallBest.LapTime > bestLap.LapTime))
                {
                    _overallBest = bestLap;
                    await TryExportOverallBest();
                }

                await TryExportPersonalBest(bestLap);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Uanble to Save Telemetry");
            }

            Logger.Trace("Personal best saved");
        }

        private Task<bool> TryExportPersonalBest(ILapInfo bestLap)
        {
            if (bestLap.Driver.Session.Player != null && bestLap.Driver.Session.Player.CarClassId != bestLap.Driver.CarClassId && _settingsProvider.DisplaySettingsViewModel.TelemetrySettingsViewModel.LogOnlyPlayerClass)
            {
                return Task.FromResult(false);
            }

            if (!_settingsProvider.DisplaySettingsViewModel.TelemetrySettingsViewModel.LogBestForEachDriver || !_settingsProvider.DisplaySettingsViewModel.TelemetrySettingsViewModel.IsTelemetryLoggingEnabled)
            {
                return Task.FromResult(false);
            }

            string lapName = $"Best - {bestLap.Driver.DriverShortName}";
            Task<bool> returnTask = Task.Run(() => SaveLapTelemetrySync(bestLap, lapName));
            return returnTask;
        }

        private Task<bool> TryExportOverallBest()
        {
            if (_overallBest.Driver.Session.Player != null && _overallBest.Driver.Session.Player.CarClassId != _overallBest.Driver.CarClassId && _settingsProvider.DisplaySettingsViewModel.TelemetrySettingsViewModel.LogOnlyPlayerClass)
            {
                return Task.FromResult(false);
            }

            if (!_settingsProvider.DisplaySettingsViewModel.TelemetrySettingsViewModel.LogOpponentsBest || !_settingsProvider.DisplaySettingsViewModel.TelemetrySettingsViewModel.IsTelemetryLoggingEnabled)
            {
                return Task.FromResult(false);
            }

            string lapName = $"Best Opponents";
            Task<bool> returnTask = Task.Run(() => SaveLapTelemetrySync(_overallBest, lapName));
            return returnTask;
        }
    }
}