﻿namespace SecondMonitor.Timing.Application.Telemetry
{
    using Common.SessionTiming;
    using DataModel.Snapshot;
    using LapTimings;
    using SecondMonitor.Telemetry.TelemetryApplication.Repository;
    using ViewModels.Settings;

    public class SessionTelemetryControllerFactory : ISessionTelemetryControllerFactory
    {
        private readonly ITelemetryRepositoryFactory _telemetryRepositoryFactory;
        private readonly ISettingsProvider _settingsProvider;
        private readonly IBestLapEventProvider _bestLapEventProvider;

        public SessionTelemetryControllerFactory(ITelemetryRepositoryFactory telemetryRepositoryFactory, ISettingsProvider settingsProvider, IBestLapEventProvider bestLapEventProvider)
        {
            _telemetryRepositoryFactory = telemetryRepositoryFactory;
            _settingsProvider = settingsProvider;
            _bestLapEventProvider = bestLapEventProvider;
        }

        public ISessionTelemetryController Create(SimulatorDataSet simulatorDataSet)
        {
            return new SessionTelemetryController(simulatorDataSet.SessionInfo.TrackInfo.TrackName,
                simulatorDataSet.SessionInfo.SessionType,
                _telemetryRepositoryFactory.Create(_settingsProvider),
                _bestLapEventProvider,
                _settingsProvider);
        }
    }
}