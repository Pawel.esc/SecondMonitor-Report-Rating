﻿namespace SecondMonitor.Timing.Application.PitBoard.DataProviders
{
    using System.Collections.Generic;
    using Common.SessionTiming.Drivers;
    using Controller;
    using DataModel.Snapshot;
    using SessionTiming.Drivers.ViewModel;

    public interface IAutonomousPitBoardDataProvider
    {
        void StartDataProvider(PitBoardController pitBoardController);

        void OnPlayerCompletedLap(SimulatorDataSet dataSet, IReadOnlyCollection<DriverTiming> driverTimingsModels);
        void Reset();
    }
}