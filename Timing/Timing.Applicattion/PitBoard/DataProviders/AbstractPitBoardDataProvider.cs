﻿namespace SecondMonitor.Timing.Application.PitBoard.DataProviders
{
    using System.Collections.Generic;
    using Common.SessionTiming.Drivers;
    using Controller;
    using DataModel.Snapshot;
    using SessionTiming.Drivers.ViewModel;

    public abstract class AbstractPitBoardDataProvider : IAutonomousPitBoardDataProvider
    {
        protected PitBoardController PitBoardController { get; private set; }

        protected bool IsStarted => PitBoardController != null;

        public virtual void StartDataProvider(PitBoardController pitBoardController)
        {
            PitBoardController = pitBoardController;
        }

        public abstract void OnPlayerCompletedLap(SimulatorDataSet dataSet, IReadOnlyCollection<DriverTiming> driverTimingsModels);
        public abstract void Reset();
    }
}