﻿namespace SecondMonitor.Timing.Application.PitBoard.DataProviders
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.SessionTiming.Drivers;
    using Contracts.Extensions;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using SecondMonitor.ViewModels.PitBoard;
    using SecondMonitor.ViewModels.Settings;

    public class GapPitBoardProvider : AbstractPitBoardDataProvider
    {
        private readonly ISettingsProvider _settingsProvider;
        private readonly Dictionary<string, TimeSpan> _lastGapToPlayer;

        public GapPitBoardProvider(ISettingsProvider settingsProvider)
        {
            _settingsProvider = settingsProvider;
            PitBoard = new RacePitBoardViewModel();
            _lastGapToPlayer = new Dictionary<string, TimeSpan>();
        }

        private RacePitBoardViewModel PitBoard { get; }

        public override void OnPlayerCompletedLap(SimulatorDataSet dataSet, IReadOnlyCollection<DriverTiming> driverTimingsModels)
        {
            if (dataSet.SessionInfo.SessionType == SessionType.Race)
            {
                UpdateAndShowBoard(driverTimingsModels);
            }
        }

        private void UpdateAndShowBoard(IReadOnlyCollection<DriverTiming> driverTimingsModels)
        {
            if (!IsStarted)
            {
                return;
            }

            var orderedDriverTimings = driverTimingsModels.Where(x => x.IsActive).OrderBy(x => x.Position).ToArray();
            var player = orderedDriverTimings.FirstOrDefault(x => x.IsPlayer);
            if (player == null)
            {
                return;
            }

            PitBoard.Lap = "L" + (player.CompletedLaps + 1);
            PitBoard.Position = "P" + player.PositionInClass;

            int playerIndex = Array.IndexOf(orderedDriverTimings, player);
            int driverBeforeIndex = playerIndex - 1;
            while (driverBeforeIndex >= 0 && orderedDriverTimings[driverBeforeIndex].InPits)
            {
                driverBeforeIndex--;
            }

            if (driverBeforeIndex >= 0)
            {
                var driverBefore = orderedDriverTimings[driverBeforeIndex];
                PitBoard.GapAhead = driverBefore.GapToPlayerAbsolute.Duration().FormatTimeSpanOnlySecondNoMiliseconds(false);
                PitBoard.GapAheadChange = (-GetGapForDriverChange(driverBefore)).FormatTimeSpanOnlySecondNoMiliseconds(true);
            }
            else
            {
                PitBoard.GapAhead = string.Empty;
                PitBoard.GapAheadChange = string.Empty;
            }

            int driverAfterIndex = playerIndex + 1;
            while (driverAfterIndex < orderedDriverTimings.Length && orderedDriverTimings[driverAfterIndex].InPits)
            {
                driverAfterIndex++;
            }

            if (driverAfterIndex < orderedDriverTimings.Length)
            {
                var driverAfter = orderedDriverTimings[driverAfterIndex];
                PitBoard.GapBehind = driverAfter.GapToPlayerAbsolute.Duration().FormatTimeSpanOnlySecondNoMiliseconds(false);
                PitBoard.GapBehindChange = GetGapForDriverChange(driverAfter).FormatTimeSpanOnlySecondNoMiliseconds(true);
            }
            else
            {
                PitBoard.GapBehind = string.Empty;
                PitBoard.GapBehindChange = string.Empty;
            }

            UpdateGapToPlayer(orderedDriverTimings);
            if (_settingsProvider.DisplaySettingsViewModel.PitBoardSettingsViewModel.IsEnabled)
            {
                PitBoardController.RequestToShowPitBoard(PitBoard, 1, TimeSpan.FromSeconds(_settingsProvider.DisplaySettingsViewModel.PitBoardSettingsViewModel.DisplaySeconds));
            }
        }

        private TimeSpan GetGapForDriverChange(DriverTiming driver)
        {
            if (_lastGapToPlayer.TryGetValue(driver.DriverId, out TimeSpan timeSpan))
            {
                return driver.GapToPlayerAbsolute - timeSpan;
            }

            return driver.GapToPlayerAbsolute.Duration();
        }

        private void UpdateGapToPlayer(DriverTiming[] driverTimingViewModels)
        {
            foreach (DriverTiming driverTimingViewModel in driverTimingViewModels)
            {
                if (driverTimingViewModel.IsPlayer)
                {
                    continue;
                }

                _lastGapToPlayer[driverTimingViewModel.DriverId] = driverTimingViewModel.GapToPlayerAbsolute;
            }
        }

        public override void Reset()
        {
            _lastGapToPlayer.Clear();
        }
    }
}