﻿namespace SecondMonitor.Timing.Application.PitBoard.DataProviders
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using Common.SessionTiming.Drivers;
    using Controller;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Drivers;
    using SecondMonitor.ViewModels.PitBoard;
    using SecondMonitor.ViewModels.SessionEvents;
    using SecondMonitor.ViewModels.Settings;
    using SessionTiming.Drivers.ViewModel;

    public class YellowAheadPitBoardProvider : AbstractPitBoardDataProvider
    {
        private readonly ISettingsProvider _settingsProvider;
        private readonly ISessionEventProvider _sessionEventProvider;
        private bool _isPitBoardShown;
        private Timer _refreshTimer;

        public YellowAheadPitBoardProvider(ISettingsProvider settingsProvider, ISessionEventProvider sessionEventProvider)
        {
            _settingsProvider = settingsProvider;
            _sessionEventProvider = sessionEventProvider;
            PitBoard = new YellowAheadPitBoardViewModel();
        }

        private YellowAheadPitBoardViewModel PitBoard { get; }

        public override void StartDataProvider(PitBoardController pitBoardController)
        {
            base.StartDataProvider(pitBoardController);
            _sessionEventProvider.FlagStateChanged += SessionEventProviderOnFlagStateChanged;
            /*PitBoardController.RequestToShowPitBoard(PitBoard, 2, TimeSpan.FromSeconds(10));*/
        }

        private void SessionEventProviderOnFlagStateChanged(object sender, DataSetArgs e)
        {
            if (!_settingsProvider.DisplaySettingsViewModel.PitBoardSettingsViewModel.IsYellowBoardEnabled)
            {
                return;
            }

            if (!_isPitBoardShown && IsYellowFlagCondition(e.DataSet))
            {
                _refreshTimer = new Timer(UpdateOnTimer, null, 50, 50);
            }
        }

        private void ShowBoard()
        {
            _isPitBoardShown = true;
            PitBoardController.RequestToShowPitBoard(PitBoard, 2, () => _isPitBoardShown);
        }

        private void UpdateOnTimer(object state)
        {
            if (!IsYellowFlagCondition(_sessionEventProvider.LastDataSet))
            {
                _isPitBoardShown = false;
                _refreshTimer.Dispose();
                return;
            }

            bool anyDriverCausingYellow = TryGetDriverCausingYellow(_sessionEventProvider.LastDataSet, out DriverInfo driverCausingYellow);
            if (anyDriverCausingYellow)
            {
                UpdateViewModel(driverCausingYellow);
            }

            if (_isPitBoardShown && !anyDriverCausingYellow)
            {
                _isPitBoardShown = false;
                return;
            }

            if (!_isPitBoardShown && anyDriverCausingYellow)
            {
                ShowBoard();
            }
        }

        private bool TryGetDriverCausingYellow(SimulatorDataSet dataSet, out DriverInfo driverCausingYellow)
        {
            driverCausingYellow = dataSet.DriversInfo.Where(x => !x.IsPlayer && x.IsCausingYellow && x.DistanceToPlayer < 0 && x.DistanceToPlayer > -800).OrderByDescending(x => x.DistanceToPlayer).FirstOrDefault();
            return driverCausingYellow != null;
        }

        private bool IsYellowFlagCondition(SimulatorDataSet dataSet)
        {
            if (dataSet.PlayerInfo.InPits)
            {
                return false;
            }

            FlagKind activeFlags = dataSet.SessionInfo.ActiveFlags;
            return activeFlags.HasFlag(FlagKind.YellowSector1) || activeFlags.HasFlag(FlagKind.YellowSector2) || activeFlags.HasFlag(FlagKind.YellowSector3);
        }

        private void UpdateViewModel(DriverInfo causingYellowDriver)
        {
            PitBoard.DriverName = $"P{causingYellowDriver.Position}  {causingYellowDriver.DriverShortName}";
            PitBoard.Distance = Distance.FromMeters(-causingYellowDriver.DistanceToPlayer).GetByUnit(_settingsProvider.DisplaySettingsViewModel.DistanceUnitsSmall).ToString("N0") + Distance.GetUnitsSymbol(_settingsProvider.DisplaySettingsViewModel.DistanceUnitsSmall);
        }

        public override void OnPlayerCompletedLap(SimulatorDataSet dataSet, IReadOnlyCollection<DriverTiming> driverTimingsModels)
        {
        }

        public override void Reset()
        {
            _isPitBoardShown = false;
        }
    }
}