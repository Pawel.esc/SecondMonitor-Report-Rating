﻿namespace SecondMonitor.Timing.Application.PitBoard.Controller
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Common.SessionTiming.Drivers;
    using DataModel.Snapshot;
    using DataProviders;
    using SecondMonitor.ViewModels;
    using SecondMonitor.ViewModels.Factory;
    using SecondMonitor.ViewModels.PitBoard.Controller;
    using ViewModels;

    public class PitBoardController : IPitBoardController
    {
        private readonly List<IAutonomousPitBoardDataProvider> _pitBoardDataProviders;
        private readonly object _lockObject;
        private readonly List<PitBoardEntry> _pitBoards;
        private Timer _pitBoardTimer;

        public PitBoardController(IViewModelFactory viewModelFactory, List<IAutonomousPitBoardDataProvider> pitBoardDataProviders)
        {
            _pitBoardDataProviders = pitBoardDataProviders;
            PitBoardViewModel = viewModelFactory.Create<PitBoardViewModel>();
            _lockObject = new object();
            _pitBoards = new List<PitBoardEntry>();
        }

        public PitBoardViewModel PitBoardViewModel { get; }

        public Task StartControllerAsync()
        {
            _pitBoardDataProviders.ForEach(x => x.StartDataProvider(this));
            _pitBoardTimer = new Timer(RefreshPitBoard, null, 50, 50);
            return Task.CompletedTask;
        }

        public Task StopControllerAsync()
        {
            _pitBoardTimer?.Dispose();
            return Task.CompletedTask;
        }

        public void OnPlayerCompletedLap(SimulatorDataSet dataSet, IReadOnlyCollection<DriverTiming> driverTimingsModels)
        {
            _pitBoardDataProviders.ForEach(x => x.OnPlayerCompletedLap(dataSet, driverTimingsModels));
        }

        public void Reset()
        {
            _pitBoardDataProviders.ForEach(x => x.Reset());
        }

        public void RequestToShowPitBoard(IViewModel viewModel, int priority)
        {
            RequestToShowPitBoard(viewModel, priority, () => true);
        }

        public void RequestToShowPitBoard(IViewModel viewModel, int priority, Func<bool> keepVisibleFunc)
        {
            lock (_lockObject)
            {
                PitBoardEntry newPitBoard = new PitBoardEntry(priority, viewModel, keepVisibleFunc);
                int insertionIndex = _pitBoards.FindIndex(x => x.Priority < newPitBoard.Priority);
                if (insertionIndex == -1)
                {
                    _pitBoards.Add(newPitBoard);
                }
                else
                {
                    _pitBoards.Insert(insertionIndex, newPitBoard);
                }

                RefreshPitBoard(null);
            }
        }

        public void RequestToShowPitBoard(IViewModel viewModel, int priority, TimeSpan displayTime)
        {
            Stopwatch sw = Stopwatch.StartNew();
            RequestToShowPitBoard(viewModel, priority, () => sw.Elapsed < displayTime);
        }

        public void HidePitBoard(IViewModel viewModel)
        {
            lock (_lockObject)
            {
                if (_pitBoards.Count == 0)
                {
                    return;
                }

                _pitBoards.Where(x => x.ViewModel == viewModel).ToList().ForEach(x => _pitBoards.Remove(x));
                if (_pitBoards.Count == 0)
                {
                    PitBoardViewModel.IsPitBoardVisible = false;
                    return;
                }

                PitBoardViewModel.IsPitBoardVisible = true;
                PitBoardViewModel.PitBoard = _pitBoards[0].ViewModel;
            }
        }

        private void RefreshPitBoard(object stateInfo)
        {
            lock (_lockObject)
            {
                if (_pitBoards.Count == 0)
                {
                    return;
                }

                _pitBoards.Where(x => !x.KeepVisibleFunc()).ToList().ForEach(x => _pitBoards.Remove(x));
                if (_pitBoards.Count == 0)
                {
                    PitBoardViewModel.IsPitBoardVisible = false;
                    return;
                }

                PitBoardViewModel.IsPitBoardVisible = true;
                PitBoardViewModel.PitBoard = _pitBoards[0].ViewModel;
            }
        }

        private class PitBoardEntry
        {
            public PitBoardEntry(int priority, IViewModel viewModel, Func<bool> keepVisibleFunc)
            {
                Priority = priority;
                ViewModel = viewModel;
                KeepVisibleFunc = keepVisibleFunc;
            }

            public int Priority { get; }
            public IViewModel ViewModel { get; }
            public Func<bool> KeepVisibleFunc { get; }
        }
    }
}