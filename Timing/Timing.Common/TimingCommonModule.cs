﻿namespace SecondMonitor.Timing.Common
{
    using Contracts.NInject;
    using Ninject.Modules;
    using SessionTiming.Drivers.Lap.SectorTracker;
    using SessionTiming.Drivers.Ordering;
    using ViewModels.Settings.Model;

    public class TimingCommonModule : NinjectModule
    {
        public override void Load()
        {
            Bind<DriverLapSectorsTrackerFactory>().ToSelf();
            Bind<DriversOrderingFactory>().ToSelf();
            Bind<IDriversOrdering>().To<AbsoluteDriversOrdering>().WithMetadata(BindingMetadataIds.DriversOrdering, DriverOrderKind.Absolute.ToString());
            Bind<IDriversOrdering>().To<RelativeDriversOrdering>().WithMetadata(BindingMetadataIds.DriversOrdering, DriverOrderKind.Relative.ToString());
            Bind<IDriversOrdering>().To<AbsoluteByClassOrdering>().WithMetadata(BindingMetadataIds.DriversOrdering, DriverOrderKind.AbsoluteByClass.ToString());
        }
    }
}