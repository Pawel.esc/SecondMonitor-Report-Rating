﻿namespace SecondMonitor.Timing.Common.SessionTiming
{
    using System;
    using Drivers.Lap;

    public interface IBestLapEventProvider
    {
        event EventHandler<BestLapChangedArgs> BestLapPersonalChanged;
        event EventHandler<BestLapChangedArgs> BestLapChanged;
        event EventHandler<BestLapChangedArgs> BestClassLapChanged;
        event EventHandler<BestSectorChangedArgs> BestSectorChanged;
        event EventHandler<BestSectorChangedArgs> BestSectorClassChanged;

        void NotifyBestLapChanged(object sender, ILapInfo oldLap, ILapInfo newLap);
        void NotifyBestClassLapChanged(object sender, ILapInfo oldLap, ILapInfo newLap);
        void NotifyBestLapPersonalChanged(object sender, ILapInfo oldLap, ILapInfo newLap);

        void NotifyBestSectorChanged(object sender, SectorTiming oldSector, SectorTiming newSector);
        void NotifyBestSectorClassChanged(object sender, SectorTiming oldSector, SectorTiming newSector);
    }
}