﻿namespace SecondMonitor.Timing.Common.SessionTiming
{
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using Drivers;
    using Drivers.Lap;
    using ViewModels.Settings.ViewModel;

    public interface ISessionInfo
    {
        int PaceLaps { get; }
        bool RetrieveAlsoInvalidLaps { get; }
        BestTimesSetViewModel SessionBestTimesViewModel { get; }
        SimulatorDataSet LastSet { get; }
        DriverTiming Player { get; }
        DriverTiming Leader { get; }
        SessionType SessionType { get; }
        bool DisplayBindTimeRelative { get; }
        DisplaySettingsViewModel DisplaySettingsViewModel { get; }

        bool DisplayGapToPlayerRelative { get; set; }

        ILapInfo GetBestLap();

        ILapInfo GetBestLap(string classId);
        BestTimesSetViewModel GetBestTimesForClass(string carClassId);
    }
}