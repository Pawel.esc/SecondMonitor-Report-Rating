﻿namespace SecondMonitor.Timing.Common.SessionTiming.Drivers.Lap
{
    public enum LapCompletionMethod
    {
        None,
        ByLapNumber,
        ByCrossingTheLine,
        ByChangingValidity,
        ByChangingValidity2,
        ByChangingValidity3,
        DriverFinished,
        LapCompleted
    }
}