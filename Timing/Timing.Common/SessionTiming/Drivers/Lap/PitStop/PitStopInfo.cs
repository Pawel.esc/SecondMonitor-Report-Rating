﻿namespace SecondMonitor.Timing.Common.SessionTiming.Drivers.Lap.PitStop
{
    using System;
    using Contracts.Extensions;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Telemetry;
    using Drivers;
    using Lap;

    public class PitStopInfo
    {
        private readonly TelemetrySnapshot _pitStopStarTelemetrySnapshot;
        private TelemetrySnapshot _pitStopEndTelemetrySnapshot;

        public PitStopInfo(SimulatorDataSet set, DriverTiming driver, ILapInfo entryLap)
        {
            Driver = driver;
            EntryLap = entryLap;
            Phase = PitPhase.Entry;
            PitEntry = set.SessionInfo.SessionTime;
            PitStopDuration = TimeSpan.Zero;
            PitExit = PitEntry;
            PitStopStart = PitEntry;
            PitStopEnd = PitEntry;
            FuelTaken = Volume.FromLiters(0);
            PitStopStallDuration = TimeSpan.Zero;
            NewFrontCompound = string.Empty;
            NewRearCompound = string.Empty;
            IsPitWindowStop = set.SessionInfo.PitWindow.PitWindowState == PitWindowState.InPitWindow;
            if (set.SessionInfo.SessionType == SessionType.Race)
            {
                _pitStopStarTelemetrySnapshot = new TelemetrySnapshot(set);
            }
        }

        public PitPhase Phase { get; private set; }

        public bool Completed => Phase == PitPhase.Completed;

        public bool WasDriveThrough { get; private set; }

        public DriverTiming Driver { get; }

        public ILapInfo EntryLap { get; }

        public TimeSpan PitEntry { get; }

        public TimeSpan PitExit { get; private set; }

        public TimeSpan PitStopStart { get; private set; }

        public TimeSpan PitStopEnd { get; private set; }

        public TimeSpan PitStopDuration { get; private set; }

        public TimeSpan PitStopStallDuration { get; private set; }

        public string NewFrontCompound { get; private set; }

        public string NewRearCompound { get; private set; }

        public Volume FuelTaken { get; private set; }

        public bool IsFrontTyresChanged { get; private set; }

        public bool IsRearTyresChanged { get; private set; }

        public bool IsPitWindowStop { get; }

        public string PitInfoFormatted
        {
            get
            {
                switch (Phase)
                {
                    case PitPhase.Entry:
                        return "-->" + PitStopDuration.FormatTimeSpanOnlySecondNoMiliseconds(false);
                    case PitPhase.InPits:
                        return "---" + PitStopDuration.FormatTimeSpanOnlySecondNoMiliseconds(false) + "---";
                    case PitPhase.Exit:
                        return PitStopDuration.FormatTimeSpanOnlySecondNoMiliseconds(false) + " -->";
                    case PitPhase.Completed:
                        return PitStopDuration.FormatTimeSpanOnlySecondNoMiliseconds(false);
                    default:
                        return string.Empty;
                }
            }
        }

        public void Tick(SimulatorDataSet set)
        {
            if (Phase == PitPhase.Completed)
            {
                return;
            }

            if (Phase == PitPhase.Entry && Driver.DriverInfo.Speed.InKph < 10)
            {
                Phase = PitPhase.InPits;
                PitStopStart = set.SessionInfo.SessionTime;
            }

            if (Phase == PitPhase.InPits && Driver.DriverInfo.Speed.InKph > 20 && (set.SessionInfo.SessionTime - PitStopStart).TotalSeconds > 2)
            {
                Phase = PitPhase.Exit;
                PitStopEnd = set.SessionInfo.SessionTime;
                PitStopStallDuration = PitStopEnd - PitStopStart;
                if (set.SessionInfo.SessionType == SessionType.Race)
                {
                    _pitStopEndTelemetrySnapshot = new TelemetrySnapshot(set);
                }

                CalculatePitStopProperties();
            }

            if (!Driver.DriverInfo.InPits)
            {
                WasDriveThrough = Phase == PitPhase.Entry;
                Phase = PitPhase.Completed;
                PitExit = set.SessionInfo.SessionTime;
                PitStopDuration = PitExit.Subtract(PitEntry);
            }

            if (Phase == PitPhase.Entry)
            {
                PitStopStart = set.SessionInfo.SessionTime;
                PitStopEnd = set.SessionInfo.SessionTime;
            }

            if (Phase == PitPhase.InPits)
            {
                PitStopEnd = set.SessionInfo.SessionTime;
            }

            if (Phase != PitPhase.Completed)
            {
                PitExit = set.SessionInfo.SessionTime;
                PitStopDuration = PitExit.Subtract(PitEntry);
            }
        }

        private void CalculatePitStopProperties()
        {
            if (_pitStopStarTelemetrySnapshot == null || _pitStopEndTelemetrySnapshot == null)
            {
                return;
            }

            if (_pitStopEndTelemetrySnapshot.PlayerData.CarInfo.FuelSystemInfo.FuelRemaining > _pitStopStarTelemetrySnapshot.PlayerData.CarInfo.FuelSystemInfo.FuelRemaining)
            {
                FuelTaken = Volume.FromLiters(_pitStopEndTelemetrySnapshot.PlayerData.CarInfo.FuelSystemInfo.FuelRemaining.InLiters - _pitStopStarTelemetrySnapshot.PlayerData.CarInfo.FuelSystemInfo.FuelRemaining.InLiters);
            }

            IsFrontTyresChanged = _pitStopEndTelemetrySnapshot.PlayerData.CarInfo.WheelsInfo.FrontLeft.TyreWear.ActualWear < _pitStopStarTelemetrySnapshot.PlayerData.CarInfo.WheelsInfo.FrontLeft.TyreWear.ActualWear;
            IsRearTyresChanged = _pitStopEndTelemetrySnapshot.PlayerData.CarInfo.WheelsInfo.RearLeft.TyreWear.ActualWear < _pitStopStarTelemetrySnapshot.PlayerData.CarInfo.WheelsInfo.RearLeft.TyreWear.ActualWear;

            if (IsFrontTyresChanged)
            {
                NewFrontCompound = _pitStopEndTelemetrySnapshot.PlayerData.CarInfo.WheelsInfo.FrontLeft.TyreType;
            }

            if (IsRearTyresChanged)
            {
                NewRearCompound = _pitStopEndTelemetrySnapshot.PlayerData.CarInfo.WheelsInfo.RearLeft.TyreType;
            }
        }
    }
}
