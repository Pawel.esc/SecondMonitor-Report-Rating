﻿namespace SecondMonitor.Timing.Common.SessionTiming.Drivers.Lap.PitStop
{
    public enum PitPhase
    {
        Entry,
        InPits,
        Exit,
        Completed
    }
}