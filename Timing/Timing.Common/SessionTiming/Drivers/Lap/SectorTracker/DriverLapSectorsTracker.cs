﻿namespace SecondMonitor.Timing.Common.SessionTiming.Drivers.Lap.SectorTracker
{
    using System;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using ViewModels.SessionEvents;

    public class DriverLapSectorsTracker : IDriverLapSectorsTracker
    {
        private const int SectionLength = 5;
        private readonly DriverTiming _driverTiming;
        private readonly ISessionEventProvider _sessionEventProvider;
        private readonly TimeSpan[] _sections;
        private int _lastSectionIndex;

        public DriverLapSectorsTracker(DriverTiming driverTiming, ISessionEventProvider sessionEventProvider)
        {
            _driverTiming = driverTiming;
            _sessionEventProvider = sessionEventProvider;
            _lastSectionIndex = -1;
            _sections = new TimeSpan[(int)(sessionEventProvider.LastDataSet.SessionInfo.TrackInfo.LayoutLength.InMeters / SectionLength) + 2];
        }

        public static TimeSpan GetRelativeGapTo(DriverTiming driverInFront, DriverTiming driverInBack)
        {
            double lapDistance = driverInBack.DriverInfo.LapDistance;
            TimeSpan driverInFrontTime = driverInFront.DriverLapSectorsTracker.GetSectionTime(lapDistance);
            TimeSpan driverInBackTime = driverInBack.DriverLapSectorsTracker.GetSectionTime(lapDistance);
            if (driverInFront.InPits && !driverInBack.InPits && driverInFront.LastPitStop != null)
            {
                driverInFrontTime += TimeSpan.FromSeconds(driverInFront.LastPitStop.PitStopDuration.TotalSeconds * 0.7);
                if (driverInFrontTime > driverInBackTime)
                {
                    driverInFrontTime = driverInBackTime;
                }
            }

            return driverInFrontTime - driverInBackTime;
        }

        public void Update()
        {
            SimulatorDataSet lastDataSet = _sessionEventProvider.LastDataSet;
            if (lastDataSet.SessionInfo.SessionType != SessionType.Race && _driverTiming.DriverInfo.InPits)
            {
                return;
            }

            int currentSectionIndex = (int)_driverTiming.DriverInfo.LapDistance / SectionLength;
            if (_lastSectionIndex < 0 && currentSectionIndex > _sections.Length / 2)
            {
                return;
            }

            if (currentSectionIndex < 0 || currentSectionIndex >= _sections.Length)
            {
                return;
            }

            if (currentSectionIndex == _lastSectionIndex)
            {
                _sections[_lastSectionIndex] = lastDataSet.SessionInfo.SessionTime;
                return;
            }

            if (currentSectionIndex < _lastSectionIndex && _lastSectionIndex - currentSectionIndex > _sections.Length / 2)
            {
                ResetDistance();
            }

            if (currentSectionIndex < _lastSectionIndex)
            {
               _lastSectionIndex = currentSectionIndex - 1;
            }

            while (_lastSectionIndex != currentSectionIndex && _lastSectionIndex + 1 < _sections.Length)
            {
                _lastSectionIndex++;
                _sections[_lastSectionIndex] = lastDataSet.SessionInfo.SessionTime;
            }
        }

        public TimeSpan GetSectionTime(double lapDistance)
        {
            int currentSectionIndex = Math.Min((int)lapDistance / SectionLength, _sections.Length - 1);
            return (currentSectionIndex >= _sections.Length) || currentSectionIndex < 0 ? TimeSpan.Zero : _sections[currentSectionIndex];
        }

        public TimeSpan GetRelativeGapToPlayer()
        {
            DriverTiming playerTiming = _driverTiming.Session?.Player;
            if (playerTiming == null)
            {
                return TimeSpan.Zero;
            }

            if (Math.Abs(_driverTiming.DistanceToPlayer) < 10)
            {
                return TimeSpan.Zero;
            }

            if (_driverTiming.DistanceToPlayer < 0)
            {
                return GetRelativeGapTo(_driverTiming, playerTiming);
            }
            else
            {
                return -GetRelativeGapTo(playerTiming, _driverTiming);
            }
        }

        public TimeSpan GetGapToDriverRelative(DriverTiming otherDriver)
        {
            if (_sessionEventProvider.LastDataSet == null || otherDriver?.DriverInfo == null)
            {
                return TimeSpan.Zero;
            }

            double distanceToOtherDriver = otherDriver.DriverInfo.ComputeRelativeDistanceTo(_driverTiming.DriverInfo, _sessionEventProvider.LastDataSet.SessionInfo.TrackInfo.LayoutLength.InMeters);
            if (Math.Abs(distanceToOtherDriver) < 10)
            {
                return TimeSpan.Zero;
            }

            if (distanceToOtherDriver >= 0)
            {
                return GetRelativeGapTo(_driverTiming, otherDriver);
            }
            else
            {
                return -GetRelativeGapTo(otherDriver, _driverTiming);
            }
        }

        public TimeSpan GetRelativeGapToLeader()
        {
            DriverTiming leaderTiming = _driverTiming.Session?.Leader;
            if (leaderTiming == null)
            {
                return TimeSpan.Zero;
            }

            return -GetRelativeGapTo(leaderTiming, _driverTiming);
        }

        public TimeSpan GetGapToPlayerAbsolute() => GetGapToDriverAbsolute(_driverTiming?.Session?.Player);

        public TimeSpan GetGapToDriverAbsolute(DriverTiming otherDriver)
        {
            if (otherDriver == null)
            {
                return TimeSpan.Zero;
            }

            if (_driverTiming.Position < otherDriver.Position)
            {
                return GetRelativeGapTo(_driverTiming, otherDriver);
            }
            else
            {
                return -GetRelativeGapTo(otherDriver, _driverTiming);
            }
        }

        public void ResetDistance()
        {
            SimulatorDataSet lastDataSet = _sessionEventProvider.LastDataSet;
            if (_lastSectionIndex > _sections.Length / 2)
            {
                for (int i = _lastSectionIndex; i < _sections.Length; i++)
                {
                    _sections[i] = lastDataSet.SessionInfo.SessionTime;
                }
            }

            _lastSectionIndex = -1;
        }
    }
}
