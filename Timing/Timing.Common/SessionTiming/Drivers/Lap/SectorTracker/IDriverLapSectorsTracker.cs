﻿namespace SecondMonitor.Timing.Common.SessionTiming.Drivers.Lap.SectorTracker
{
    using System;

    public interface IDriverLapSectorsTracker
    {
        void Update();

        TimeSpan GetSectionTime(double lapDistance);

        TimeSpan GetRelativeGapToPlayer();

        TimeSpan GetGapToPlayerAbsolute();
        void ResetDistance();
        TimeSpan GetRelativeGapToLeader();

        TimeSpan GetGapToDriverAbsolute(DriverTiming otherDriver);

        TimeSpan GetGapToDriverRelative(DriverTiming otherDriver);
    }
}