﻿namespace SecondMonitor.Timing.Common.SessionTiming.Drivers.Lap.SectorTracker
{
    using ViewModels.SessionEvents;

    public class DriverLapSectorsTrackerFactory
    {
        private readonly ISessionEventProvider _sessionEventProvider;

        public DriverLapSectorsTrackerFactory(ISessionEventProvider sessionEventProvider)
        {
            _sessionEventProvider = sessionEventProvider;
        }

        public IDriverLapSectorsTracker Build(DriverTiming driverTiming)
        {
            return new DriverLapSectorsTracker(driverTiming, _sessionEventProvider);
        }
    }
}