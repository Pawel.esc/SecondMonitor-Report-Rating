﻿namespace SecondMonitor.AMS2Connector
{
    using System;
    using System.Diagnostics;
    using System.Threading;
    using System.Threading.Tasks;
    using DataConvertor;
    using DataModel;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using NLog;
    using PluginManager.GameConnector;
    using PluginsConfiguration.Common.Controller;
    using SecondMonitor.PluginManager.GameConnector.SharedMemory;
    using SharedMemory;

    public class Ams2Connector : AbstractGameConnector
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static readonly string[] AMS2Executables = { "AMS2", "AMS2AVX" };
        private static readonly string SharedMemoryName = "$pcars2$";

        private readonly MappedBuffer<Ams2SharedMemory> _sharedMemory;
        private readonly Ams2DataConvertor _ams2DataConvertor;
        private readonly Stopwatch _stopwatch;
        private readonly Ams2UdpReceiver _udpReceiver;

        private Task _udpReceiverTask;
        private CancellationTokenSource _udpCancellationTokenSource;

        private bool _isConnected;

        private Ams2SessionType _lastRawAms2SessionType;
        private SessionType _lastSessionType;

        public Ams2Connector(IPluginSettingsProvider pluginSettingsProvider)
            : base(AMS2Executables)
        {
            _sharedMemory = new MappedBuffer<Ams2SharedMemory>(SharedMemoryName);

            _ams2DataConvertor = new Ams2DataConvertor();
            _lastRawAms2SessionType = Ams2SessionType.SessionInvalid;
            _lastSessionType = SessionType.Na;
            _stopwatch = new Stopwatch();
            _udpReceiver = new Ams2UdpReceiver(pluginSettingsProvider.PCars2Configurations);
        }

        protected override string ConnectorName => SimulatorsNameMap.AMS2SimName;

        protected internal TimeSpan SessionTime => _stopwatch?.Elapsed ?? TimeSpan.Zero;

        public override bool IsConnected => _isConnected;

        protected override void OnConnection()
        {
            ResetConnector();

            try
            {
                _sharedMemory.Connect();
                _isConnected = true;
                _udpCancellationTokenSource = new CancellationTokenSource();
                _udpReceiverTask = _udpReceiver.ReceiveLoop(OnNewUdpData, _udpCancellationTokenSource.Token);
            }
            catch (Exception)
            {
                Disconnect();
                throw;
            }
        }

        protected override void ResetConnector()
        {
            _stopwatch.Restart();
            _lastRawAms2SessionType = Ams2SessionType.SessionInvalid;
            _lastSessionType = SessionType.Na;
        }

        protected override async Task DaemonMethod(CancellationToken cancellationToken)
        {
            while (!ShouldDisconnect)
            {
                await Task.Delay(TickTime, cancellationToken).ConfigureAwait(false);
                Ams2SharedMemory rawData = ReadAllBuffers();

                if (!_stopwatch.IsRunning && ((GameState)rawData.mGameState == GameState.GameInGamePlaying || (GameState)rawData.mGameState == GameState.GameInGameInMenuTimeTicking))
                {
                    _stopwatch.Start();
                }

                if (_stopwatch.IsRunning && ((GameState)rawData.mGameState != GameState.GameInGamePlaying && (GameState)rawData.mGameState != GameState.GameInGameInMenuTimeTicking))
                {
                    _stopwatch.Stop();
                }

                if ((GameState)rawData.mGameState == GameState.GameFrontEndReplay || (GameState)rawData.mGameState == GameState.GameInGameReplay)
                {
                    continue;
                }

                // This a state that sometimes occurs when saving pit preset during race
                // This state is ignored, otherwise it would trigger a session reset
                if (rawData.mSessionState == 0 && (rawData.mGameState == 2 || rawData.mGameState == 3))
                {
                    continue;
                }

                SimulatorDataSet dataSet = _ams2DataConvertor.CreateSimulatorDataSet(rawData, TimeSpan.FromMilliseconds(_stopwatch.ElapsedMilliseconds));

                if (CheckSessionStarted(rawData, dataSet))
                {
                    _stopwatch.Restart();
                    dataSet.SessionInfo.SessionTime = _stopwatch.Elapsed;
                    RaiseSessionStartedEvent(dataSet);
                }

                RaiseDataLoadedEvent(dataSet);

                if (!IsProcessRunning())
                {
                    ShouldDisconnect = true;
                }
            }

            try
            {
                _udpCancellationTokenSource.Cancel();
                await _udpReceiverTask;
            }
            catch (OperationCanceledException)
            {
            }

            Disconnect();
            RaiseDisconnectedEvent();
        }

        private void OnNewUdpData(UDPTelemetryData data)
        {
            _ams2DataConvertor.UdpTelemetryData = data;
        }

        private Ams2SharedMemory ReadAllBuffers()
        {
            if (!IsConnected)
            {
                throw new InvalidOperationException("Not connected");
            }

            return _sharedMemory.GetMappedDataUnSynchronized();
        }

        private void Disconnect()
        {
            _sharedMemory.Disconnect();
            _isConnected = false;
        }

        private bool CheckSessionStarted(Ams2SharedMemory rawData, SimulatorDataSet dataSet)
        {
            if (_lastRawAms2SessionType != (Ams2SessionType)rawData.mSessionState || _lastSessionType != dataSet.SessionInfo.SessionType)
            {
                Logger.Info($"Session doesn match, last packet session: {_lastSessionType}, current session: {dataSet.SessionInfo.SessionType} ");
                _lastSessionType = dataSet.SessionInfo.SessionType;
                _lastRawAms2SessionType = (Ams2SessionType)rawData.mSessionState;
                return true;
            }

            return false;
        }
    }
}