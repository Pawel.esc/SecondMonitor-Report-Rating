﻿namespace SecondMonitor.Rating.Application.Championship.Controller
{
    using System.Threading.Tasks;
    using Common.DataModel.Championship;
    using Contracts.Commands;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Drivers;
    using Filters;
    using NLog;
    using Operations;
    using SecondMonitor.ViewModels.Controllers;
    using SecondMonitor.ViewModels.PitBoard.Controller;
    using SecondMonitor.ViewModels.SessionEvents;
    using Timing.Common.SessionTiming;
    using Timing.Common.SessionTiming.Drivers.Lap;
    using ViewModels;

    public class ChampionshipEventController : AbstractChildController<IChampionshipController>, IChampionshipEventController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly IChampionshipManipulator _championshipManipulator;
        private readonly ISessionEventProvider _sessionEventProvider;
        private readonly IChampionshipEligibilityEvaluator _championshipEligibilityEvaluator;
        private readonly IChampionshipDialogProvider _championshipDialogProvider;
        private readonly IPitBoardController _pitBoardController;
        private readonly IBestLapEventProvider _bestLapEventProvider;
        private readonly OpenLastResultPitBoardViewModel _openLastResultPitBoardViewModel;
        private string _lastTrack;
        private bool _isSessionRace;
        private bool _hasPlayerFinished;

        private ILapInfo _bestLap;

        public ChampionshipEventController(IChampionshipManipulator championshipManipulator, ISessionEventProvider sessionEventProvider, IChampionshipEligibilityEvaluator championshipEligibilityEvaluator,
            IChampionshipDialogProvider championshipDialogProvider, IPitBoardController pitBoardController, IBestLapEventProvider bestLapEventProvider)
        {
            _championshipManipulator = championshipManipulator;
            _sessionEventProvider = sessionEventProvider;
            _championshipEligibilityEvaluator = championshipEligibilityEvaluator;
            _championshipDialogProvider = championshipDialogProvider;
            _pitBoardController = pitBoardController;
            _bestLapEventProvider = bestLapEventProvider;
            _openLastResultPitBoardViewModel = new OpenLastResultPitBoardViewModel();
            _openLastResultPitBoardViewModel.OpenCommand = new RelayCommand(OpenLastEventResults);
        }

        public bool IsChampionshipActive { get; private set; }

        public ChampionshipDto CurrentChampionship { get; private set; }

        public void StartNextEvent(ChampionshipDto championship)
        {
            Logger.Info($"Starting new Event {championship.ChampionshipName}");
            CurrentChampionship = championship;
            if (CurrentChampionship.ChampionshipState == ChampionshipState.NotStarted)
            {
                _championshipManipulator.StartChampionship(championship, _sessionEventProvider.LastDataSet);
            }
            else
            {
                _championshipManipulator.UpdateDriversProperties(championship, _sessionEventProvider.LastDataSet);
            }

            _championshipManipulator.StartNextEvent(championship, _sessionEventProvider.LastDataSet);
            InitializePropertiesOnSessionStart(_sessionEventProvider.LastDataSet);

            IsChampionshipActive = true;
            ShowWelcomeScreen(_sessionEventProvider.LastDataSet);
        }

        public bool TryResumePreviousChampionship(SimulatorDataSet dataSet)
        {
            IsChampionshipActive = dataSet.PlayerInfo.FinishStatus != DriverFinishStatus.Finished && CurrentChampionship != null && _championshipEligibilityEvaluator.EvaluateChampionship(CurrentChampionship, dataSet) != RequirementResultKind.DoesNotMatch;
            Logger.Info($"TryResumePreviousChampionship result is {IsChampionshipActive}");

            if (IsChampionshipActive)
            {
                InitializePropertiesOnSessionStart(_sessionEventProvider.LastDataSet);
                _championshipManipulator.UpdateDriversProperties(CurrentChampionship, dataSet);
            }

            if (IsChampionshipActive && dataSet.SessionInfo.TrackInfo.TrackFullName != _lastTrack)
            {
                ShowWelcomeScreen(dataSet);
            }

            return IsChampionshipActive;
        }

        public override Task StartControllerAsync()
        {
            _sessionEventProvider.PlayerFinishStateChanged += SessionEventProviderOnPlayerFinishStateChanged;
            _sessionEventProvider.SessionTypeChange += SessionEventProviderOnSessionTypeChange;
            _sessionEventProvider.DriversAdded += SessionEventProviderOnDriversChanged;
            _sessionEventProvider.DriversRemoved += SessionEventProviderOnDriversRemoved;
            _bestLapEventProvider.BestClassLapChanged += BestLapEventProviderOnBestClassLapChanged;
            _bestLapEventProvider.BestLapChanged += BestLapEventProviderOnBestLapChanged;
            return Task.CompletedTask;
        }

        public override Task StopControllerAsync()
        {
            _sessionEventProvider.PlayerFinishStateChanged -= SessionEventProviderOnPlayerFinishStateChanged;
            _sessionEventProvider.SessionTypeChange -= SessionEventProviderOnSessionTypeChange;
            _sessionEventProvider.DriversAdded -= SessionEventProviderOnDriversChanged;
            _sessionEventProvider.DriversRemoved -= SessionEventProviderOnDriversRemoved;
            _bestLapEventProvider.BestClassLapChanged -= BestLapEventProviderOnBestClassLapChanged;
            _bestLapEventProvider.BestLapChanged -= BestLapEventProviderOnBestLapChanged;
            return Task.CompletedTask;
        }

        private void SessionEventProviderOnDriversRemoved(object sender, DriversArgs e)
        {
            SessionEventProviderOnDriversChanged(sender, e);
        }

        private void SessionEventProviderOnDriversChanged(object sender, DriversArgs e)
        {
            SimulatorDataSet dataSet = e.PreviousDataSet;
            if (!IsChampionshipActive || dataSet == null)
            {
                return;
            }

            if (dataSet.DriversInfo.Length == 0 || !TryResumePreviousChampionship(e.DataSet))
            {
                Logger.Info("Drivers changed - ending championship");
                FinishCurrentEvent(e.DataSet);
                return;
            }

            if (dataSet.SessionInfo.SessionType == SessionType.Race)
            {
                Logger.Info("Drivers chagned in race");
                if (dataSet.GetDriversInfoByMultiClass().Count != CurrentChampionship.TotalDrivers)
                {
                    Logger.Warn($"Drivers count missmatch, unable to update. In DataSet {dataSet.GetDriversInfoByMultiClass().Count}, in Championship: {CurrentChampionship.TotalDrivers}");
                    return;
                }

                _championshipManipulator.AddResultsForCurrentSession(CurrentChampionship, dataSet, null, shiftPlayerToLastPlace: true);
                CurrentChampionship.ChampionshipState = ChampionshipState.LastSessionCanceled;
                //Finish with actual dataset - so the actual dataset is used for championship reeavluation
                FinishCurrentEvent(e.DataSet);
            }
        }

        private void SessionEventProviderOnSessionTypeChange(object sender, DataSetArgs e)
        {
            if (_openLastResultPitBoardViewModel.OriginalModel != null && !_openLastResultPitBoardViewModel.AlreadyOpened)
            {
                OpenLastEventResults();
            }

            if (!IsChampionshipActive || _sessionEventProvider.BeforeLastDataSet == null)
            {
                return;
            }

            if (_sessionEventProvider.BeforeLastDataSet.SessionInfo.SessionType == SessionType.Race)
            {
                Logger.Info("Session Type Chagned, and is in race");
                _championshipManipulator.AddResultsForCurrentSession(CurrentChampionship, _sessionEventProvider.BeforeLastDataSet, null, shiftPlayerToLastPlace: true);
                CurrentChampionship.ChampionshipState = ChampionshipState.LastSessionCanceled;
                FinishCurrentEvent(_sessionEventProvider.BeforeLastDataSet);
                return;
            }

            if (e.DataSet.SessionInfo.SessionType != SessionType.Na && _championshipEligibilityEvaluator.EvaluateChampionship(CurrentChampionship, e.DataSet) == RequirementResultKind.DoesNotMatch)
            {
                Logger.Info("Session type changed, and current championship is not elligible");
                FinishCurrentEvent(_sessionEventProvider.BeforeLastDataSet);
                return;
            }

            InitializePropertiesOnSessionStart(e.DataSet);
        }

        private void SessionEventProviderOnPlayerFinishStateChanged(object sender, DataSetArgs e)
        {
            if (!IsChampionshipActive)
            {
                return;
            }

            if (e.DataSet.PlayerInfo.FinishStatus == DriverFinishStatus.Finished && _sessionEventProvider.BeforeLastDataSet?.SessionInfo?.SessionType == SessionType.Race && _isSessionRace && !_hasPlayerFinished)
            {
                Logger.Info("Drivers finished in race");
                _hasPlayerFinished = true;
                _championshipManipulator.AddResultsForCurrentSession(CurrentChampionship, e.DataSet, _bestLap);
                _championshipManipulator.CommitLastSessionResults(CurrentChampionship);
                _openLastResultPitBoardViewModel.FromModel(CurrentChampionship);
                _pitBoardController.RequestToShowPitBoard(_openLastResultPitBoardViewModel, 10);
                FinishCurrentEvent(e.DataSet);
                CurrentChampionship = null;
            }

            _hasPlayerFinished = e.DataSet.PlayerInfo.FinishStatus == DriverFinishStatus.Finished;
        }

        private void OpenLastEventResults()
        {
            _pitBoardController.HidePitBoard(_openLastResultPitBoardViewModel);
            if (_openLastResultPitBoardViewModel.OriginalModel == null)
            {
                return;
            }

            _openLastResultPitBoardViewModel.AlreadyOpened = true;
            _championshipDialogProvider.ShowLastEvenResultWindow(_openLastResultPitBoardViewModel.OriginalModel);
        }

        private void FinishCurrentEvent(SimulatorDataSet simulatorDataSet)
        {
            Logger.Info($"Finishing event for {CurrentChampionship.ChampionshipName}");
            IsChampionshipActive = false;
            ParentController.EventFinished(CurrentChampionship, simulatorDataSet);
        }

        private void ShowWelcomeScreen(SimulatorDataSet dataSet)
        {
            _lastTrack = dataSet.SessionInfo.TrackInfo.TrackFullName;
            _championshipDialogProvider.ShowWelcomeScreen(dataSet, CurrentChampionship);
        }

        private void InitializePropertiesOnSessionStart(SimulatorDataSet dataSet)
        {
            _isSessionRace = dataSet.SessionInfo.SessionType == SessionType.Race;
            _hasPlayerFinished = dataSet.PlayerInfo?.FinishStatus == DriverFinishStatus.Finished;
        }

        private void BestLapEventProviderOnBestLapChanged(object sender, BestLapChangedArgs e)
        {
            if (_sessionEventProvider.LastDataSet.SessionInfo.IsMultiClass)
            {
                return;
            }

            _bestLap = e.NewLap;
        }

        private void BestLapEventProviderOnBestClassLapChanged(object sender, BestLapChangedArgs e)
        {
            if (!_sessionEventProvider.LastDataSet.SessionInfo.IsMultiClass || e.NewLap.Driver.CarClassId != _sessionEventProvider.LastDataSet.PlayerInfo.CarClassId)
            {
                return;
            }

            _bestLap = e.NewLap;
        }
    }
}