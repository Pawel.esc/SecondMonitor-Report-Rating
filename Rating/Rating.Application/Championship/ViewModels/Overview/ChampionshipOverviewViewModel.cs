﻿namespace SecondMonitor.Rating.Application.Championship.ViewModels.Overview
{
    using System;
    using Common.DataModel.Championship;
    using Contracts.Rating;
    using SecondMonitor.ViewModels;

    public class ChampionshipOverviewViewModel : AbstractViewModel<ChampionshipDto>
    {
        private readonly ISimulatorRatingProvider _simulatorRatingProvider;

        public ChampionshipOverviewViewModel(ISimulatorRatingProvider simulatorRatingProvider)
        {
            _simulatorRatingProvider = simulatorRatingProvider;
        }

        public ChampionshipState ChampionshipState { get; set; }

        public string Name { get; private set; }

        public string Progress { get; private set; }

        public string Position { get; private set; }

        public string NextTrack { get; set; }

        public string Simulator { get; set; }

        public string ClassName { get; set; }

        public int Difficulty { get; set; }

        public bool IsDifficultyVisible { get; set; }

        protected override void ApplyModel(ChampionshipDto model)
        {
            Name = model.ChampionshipName;
            Progress = model.ChampionshipState == ChampionshipState.Finished ? "Completed" : "Next Race: " + model.CompletedRaces + "/" + model.TotalEvents;
            string nextTrack = model.NextTrack;
            var currentEvent = model.GetCurrentOrLastEvent();
            if (currentEvent.IsMysteryTrack)
            {
                nextTrack += " (Mystery Track)";
            }

            if (!string.IsNullOrWhiteSpace(currentEvent.EventName))
            {
                Progress += "\n" + currentEvent.EventName;
            }

            Progress += "\nOn: " + nextTrack;
            Position = model.Position == 0 ? "-" : "Pos: " + model.Position + "/" + model.TotalDrivers;
            NextTrack = nextTrack;
            Simulator = model.SimulatorName;
            ChampionshipState = model.ChampionshipState;
            ClassName = model.ClassName;
            IsDifficultyVisible = _simulatorRatingProvider.TryGetSuggestedDifficulty(model.SimulatorName, model.ClassName, out int difficulty);
            Difficulty = difficulty;
            if (difficulty != 0)
            {
                Progress += "\nDifficulty: " + difficulty;
            }
        }

        public override ChampionshipDto SaveToNewModel()
        {
            throw new NotImplementedException();
        }
    }
}