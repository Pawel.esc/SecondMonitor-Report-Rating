﻿namespace SecondMonitor.Rating.Application.Rating.Controller.SimulatorRating.RatingUpdater
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Common.DataModel;
    using Common.DataModel.Player;
    using DataModel.Extensions;
    using DataModel.Summary;
    using Glicko2;
    using NLog;
    using SecondMonitor.ViewModels.Settings;

    public class RatingUpdater : IRatingUpdater
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly ISimulatorRatingController _ratingController;
        private readonly ISettingsProvider _settingsProvider;

        public RatingUpdater(ISimulatorRatingController ratingController, ISettingsProvider settingsProvider)
        {
            _ratingController = ratingController;
            _settingsProvider = settingsProvider;
        }

        public async Task<(DriversRating newSimulatorRating, DriversRating newClassRating, DriversRating newDifficultyRating)> UpdateRatingsByResults(Dictionary<string, DriversRating> ratings, DriversRating difficultyRating, DriversRating simulatorRating, SessionFinishState sessionFinishState, int difficulty)
        {
            DriverFinishState player = sessionFinishState.DriverFinishStates.First(x => x.IsPlayer);
            Logger.Info("---- CALCULATING NEW CLASS RATINGS ---");
            GlickoPlayer newClassRating = CalculateNewRatingByResult(ratings, ratings[player.DriverId], sessionFinishState, player, false);
            Logger.Info("---- CALCULATING NEW SIM RATINGS ---");
            GlickoPlayer newSimRating = CalculateNewRatingByResult(ratings, simulatorRating, sessionFinishState, player, false);
            Logger.Info("---- CALCULATING NEW DIFFICULTY RATINGS ---");
            GlickoPlayer newDifficultyRating = CalculateNewRatingByResult(ratings, difficultyRating, sessionFinishState, player, true);

            await ComputeNewRatingsAndNotify(newClassRating.FromGlicko(), newDifficultyRating.FromGlicko(), newSimRating.FromGlicko(), difficulty, player, sessionFinishState.TrackName);
            return (newSimRating.FromGlicko(), newClassRating.FromGlicko(), newDifficultyRating.FromGlicko());
        }

        public async Task<(DriversRating newSimulatorRating, DriversRating newClassRating, DriversRating newDifficultyRating)> UpdateRatingsAsLoss(Dictionary<string, DriversRating> ratings, DriversRating difficultyRating, DriversRating simulatorRating, int difficulty, Driver player, string trackName)
        {
            DriverFinishState playerFinishState = new DriverFinishState(player.DriverId, true, player.DriverLongName, player.CarName, player.ClassName, ratings.Count, player.FinishingPosition);
            GlickoPlayer newClassRating = CalculateNewAsLoss(ratings, ratings[player.DriverId], player);
            GlickoPlayer newSimRating = CalculateNewAsLoss(ratings, simulatorRating, player);

            await ComputeNewRatingsAndNotify(newClassRating.FromGlicko(), difficultyRating, newSimRating.FromGlicko(), difficulty, playerFinishState, trackName);
            return (newSimRating.FromGlicko(), newClassRating.FromGlicko(), difficultyRating);
        }

        private static Dictionary<string, GlickoPlayer> TransformToGlickoPlayers(Dictionary<string, DriversRating> ratings)
        {
            return ratings.Select(x => new KeyValuePair<string, GlickoPlayer>(x.Key, x.Value.ToGlicko(x.Key))).ToDictionary(x => x.Key, x => x.Value);
        }

        private static void LogRatings(Dictionary<string, DriversRating> ratings)
        {
            ratings.ForEach(x => Logger.Info($"Rating for {x.Key} : {x.Value.Rating} - {x.Value.Deviation} -  {x.Value.Volatility}"));
        }

        private GlickoPlayer CalculateNewAsLoss(Dictionary<string, DriversRating> ratings, DriversRating rating,  Driver player)
        {
            var playerRating = rating.ToGlicko(player.DriverLongName);
            var glickoRatings = TransformToGlickoPlayers(ratings);
            var opponents = glickoRatings.Where(x => x.Key != player.DriverId).Select(x => new GlickoOpponent(x.Value, 0)).ToList();
            return CalculateNewRating(playerRating, opponents);
        }

        private GlickoPlayer CalculateNewRatingByResult(Dictionary<string, DriversRating> ratings, DriversRating rating, SessionFinishState sessionFinishState, DriverFinishState player, bool useAveragePosition)
        {
            Logger.Info("---- CALCULATING NEW RATINGS ---");
            Logger.Info($"Players Rating: {rating.Rating}-{rating.Deviation}-{rating.Volatility}");
            LogRatings(ratings);
            var playerRating = rating.ToGlicko(player.DriverId);
            var glickoRatings = TransformToGlickoPlayers(ratings);

            Func<DriverFinishState, double> finishPositionFunc = useAveragePosition ? x => x.AveragePosition : new Func<DriverFinishState, double>(x => x.FinishPosition);

            DriverFinishState[] eligibleDrivers = sessionFinishState.DriverFinishStates.Where(x => !x.IsPlayer && ratings.ContainsKey(x.DriverId)).ToArray();
            var opponents = eligibleDrivers.Select(x => new GlickoOpponent(glickoRatings[x.DriverId], finishPositionFunc(x) < finishPositionFunc(player) ? 0 : 1)).ToList();
            return CalculateNewRating(playerRating, opponents);
        }

        private GlickoPlayer CalculateNewRating(GlickoPlayer player, List<GlickoOpponent> opponents)
        {
            double ratingBeforeChange = player.Rating;
            GlickoPlayer newRating = GlickoCalculator.CalculateRanking(player, opponents);
            if (_settingsProvider.DisplaySettingsViewModel.RatingSettingsViewModel.NormalizedFieldSize == 0)
            {
                return newRating;
            }

            double normalizationFactor = (double)_settingsProvider.DisplaySettingsViewModel.RatingSettingsViewModel.NormalizedFieldSize / opponents.Count;
            Logger.Info($"Rating Normalization Factor is {normalizationFactor:F2}");
            double ratingChange = newRating.Rating - ratingBeforeChange;
            double normalizedRatingChange = ratingChange * normalizationFactor;
            Logger.Info($"Rating Change: {ratingChange:F2}, Normalized Rating Change: {normalizedRatingChange:F2}");
            newRating.Rating = ratingBeforeChange + normalizedRatingChange;
            return newRating;
        }

        private async Task ComputeNewRatingsAndNotify(DriversRating newPlayerClassRating, DriversRating newDifficultyRating, DriversRating newPlayerSimRatingRating, int difficulty, DriverFinishState playerFinishState, string trackName)
        {
            await _ratingController.UpdateRating(newPlayerClassRating, newDifficultyRating, newPlayerSimRatingRating, difficulty, trackName, playerFinishState);
        }
    }
}