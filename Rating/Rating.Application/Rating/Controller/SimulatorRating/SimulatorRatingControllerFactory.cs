﻿namespace SecondMonitor.Rating.Application.Rating.Controller.SimulatorRating
{
    using System;
    using System.Linq;
    using DataModel;
    using Ninject;
    using Ninject.Parameters;
    using Ninject.Syntax;

    public class SimulatorRatingControllerFactory : ISimulatorRatingControllerFactory
    {
        private readonly IResolutionRoot _resolutionRoot;
        public static readonly string[] SupportedSimulators = { SimulatorsNameMap.R3ESimName, SimulatorsNameMap.ACSimName, SimulatorsNameMap.AMSSimName, SimulatorsNameMap.RF2SimName, SimulatorsNameMap.PCars2SimName, SimulatorsNameMap.AMS2SimName, SimulatorsNameMap.ACCSimName, SimulatorsNameMap.PCarsSimName, SimulatorsNameMap.GTR2SimName };
        //private static readonly string[] SupportedSimulators = new[] { "R3E", "Assetto Corsa", "RFactor 2", "AMS" };

        public SimulatorRatingControllerFactory(IResolutionRoot resolutionRoot)
        {
            _resolutionRoot = resolutionRoot;
        }

        public bool IsSimulatorSupported(string simulatorName)
        {
            return SupportedSimulators.Contains(simulatorName);
        }

        public ISimulatorRatingController CreateController(string simulatorName)
        {
            if (!IsSimulatorSupported(simulatorName))
            {
                throw new ArgumentException($"Simulator : {simulatorName}, is not supported");
            }

            return _resolutionRoot.Get<ISimulatorRatingController>(new ConstructorArgument("simulatorName", simulatorName, false));
        }
    }
}