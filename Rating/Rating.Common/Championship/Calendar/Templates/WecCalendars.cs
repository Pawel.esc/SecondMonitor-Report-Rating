﻿namespace SecondMonitor.Rating.Common.Championship.Calendar.Templates
{
    using Tracks;

    public class WecCalendars
    {
        public static CalendarTemplateGroup AllWecCalendars => new CalendarTemplateGroup("World Endurance Championship", new[] { Wec1920Calendar });

        public static CalendarTemplate Wec1920Calendar => new CalendarTemplate("WEC 2019 - 2020", new[]
        {
            new EventTemplate(TracksTemplates.SilverstoneGpPresent, "4 Hours of Silverstone"),
            new EventTemplate(TracksTemplates.FujiGpPresent, "6 Hours of Fuji"),
            new EventTemplate(TracksTemplates.ShanghaiGp, "4 Hours of Shanghai"),
            new EventTemplate(TracksTemplates.BahrainGP, "Bapco 8 Hours of Bahrain"),
            new EventTemplate(TracksTemplates.CotaGP, "Lone Star Le Mans (6 hours)"),
            new EventTemplate(TracksTemplates.SpaPresent, "6 Hours of Spa-Francorchamps"),
            new EventTemplate(TracksTemplates.CircuitdelaSarthePresent, "24 Hours of Le Mans"),
        });
    }
}