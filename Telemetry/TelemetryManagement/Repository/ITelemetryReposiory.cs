﻿namespace SecondMonitor.Telemetry.TelemetryManagement.Repository
{
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using DTO;

    public interface ITelemetryRepository
    {
        IReadOnlyCollection<SessionInfoDto> GetAllRecentSessions();
        IReadOnlyCollection<SessionInfoDto> GetAllArchivedSessions();
        IEnumerable<SessionInfoDto> LoadPreviouslyLoadedSessions(List<SessionInfoDto> sessions);

        void SaveRecentSessionInformation(SessionInfoDto sessionInfoDto, string sessionIdentifier);
        void SaveRecentSessionInformationDirectly(SessionInfoDto sessionInfoDto, string fullFilePath);
        void SaveRecentSessionLap(LapTelemetryDto lapTelemetry, string sessionIdentifier, string fileName);

        void Save(LapTelemetryDto lapTelemetryDto, string path);

        SessionInfoDto OpenRecentSession(string sessionIdentifier);
        void CloseSession(string sessionIdentifier);
        LapTelemetryDto LoadLapTelemetryDtoFromAnySession(LapSummaryDto lapSummaryDto);
        LapTelemetryDto LoadLapTelemetryDto(FileInfo file);
        string GetLastRecentSessionIdentifier();

        Task ArchiveSessions(SessionInfoDto sessionInfoDto);

        Task OpenSessionFolder(SessionInfoDto sessionInfoDto);
        void DeleteSession(SessionInfoDto sessionInfoDto);

        Task<SessionInfoDto> ImportTelemetry(string fileName);
    }
}