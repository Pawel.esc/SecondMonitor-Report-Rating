﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Controllers.Settings.DefaultCarProperties
{
    using System;
    using System.Linq;
    using DataModel;
    using DataModel.BasicProperties;
    using TelemetryApplication.Settings.DTO.DefaultCarProperties;
    using TelemetryManagement.Settings;

    public class R3EDefaultCarPropertiesProvider : IDefaultCarPropertiesProvider
    {
        private readonly DefaultR3ECarSettingsRepository _defaultR3ECarSettingsRepository;
        private readonly Lazy<DefaultR3ECarsProperties> _r3ECarPropertiesLazy;

        public R3EDefaultCarPropertiesProvider(DefaultR3ECarSettingsRepository defaultR3ECarSettingsRepository)
        {
            _defaultR3ECarSettingsRepository = defaultR3ECarSettingsRepository;
            _r3ECarPropertiesLazy = new Lazy<DefaultR3ECarsProperties>(LoadCarProperties);
        }

        protected DefaultR3ECarsProperties CarProperties => _r3ECarPropertiesLazy.Value;

        private DefaultR3ECarsProperties LoadCarProperties()
        {
            return _defaultR3ECarSettingsRepository.LoadRatingsOrCreateNew();
        }

        public bool TryGetDefaultSettings(string simName, string carName, out CarPropertiesDto carPropertiesDto)
        {
            if (simName != SimulatorsNameMap.R3ESimName)
            {
                carPropertiesDto = null;
                return false;
            }

            var r3EProperties = CarProperties.CarsProperties.FirstOrDefault(x => x.CarName == carName.Replace(" ", string.Empty).ToLower());
            if (r3EProperties == null)
            {
                carPropertiesDto = null;
                return false;
            }

            carPropertiesDto = new CarPropertiesDto()
            {
                CarName = carName,
                Simulator = simName,
                FrontLeftTyre = { BumpTransition = Velocity.FromMs(r3EProperties.BumpTransitionFront) },
                FrontRightTyre = { BumpTransition = Velocity.FromMs(r3EProperties.BumpTransitionFront) },
                RearLeftTyre = { BumpTransition = Velocity.FromMs(r3EProperties.BumpTransitionRear) },
                RearRightTyre = { BumpTransition = Velocity.FromMs(r3EProperties.BumpTransitionRear) },
            };

            carPropertiesDto.FrontLeftTyre.ReboundTransition = Velocity.FromMs(r3EProperties.ReboundTransitionFront);
            carPropertiesDto.FrontRightTyre.ReboundTransition = Velocity.FromMs(r3EProperties.ReboundTransitionFront);
            carPropertiesDto.RearLeftTyre.ReboundTransition = Velocity.FromMs(r3EProperties.ReboundTransitionRear);
            carPropertiesDto.RearRightTyre.ReboundTransition = Velocity.FromMs(r3EProperties.ReboundTransitionRear);

            return true;
        }
    }
}