﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Workspaces.Providers.BuildIn.ChartSet
{
    using System;
    using Controllers.MainWindow.GraphPanel;
    using SecondMonitor.ViewModels.Layouts;
    using SecondMonitor.ViewModels.Settings.Model.Layout;

    public class DifferentialChartSetProvider : AbstractChartSetProvider
    {
        public static readonly Guid Guid = Guid.Parse("22521cfd-58b9-4c4d-b882-872e7f696523");
        public override Guid ChartSetGuid => Guid;
        public override string ChartSetName => "Differential";
        protected override LayoutDescription GetLayoutDescription()
        {
            var content = RowsDefinitionSettingBuilder.Create().WithGridSplitters().
                WithNamedContent(SeriesChartNames.LapTimeChartName, SizeKind.Manual, 150).
                WithNamedContent(SeriesChartNames.SpeedChartName, SizeKind.Manual, 250).
                WithNamedContent(SeriesChartNames.CombinedPedalsChartName, SizeKind.Manual, 200).
                WithNamedContent(SeriesChartNames.RearTyresSlip, SizeKind.Manual, 200).
                WithNamedContent(SeriesChartNames.RearTyresRps, SizeKind.Manual, 200).
                WithNamedContent(SeriesChartNames.RearTyresRpsDiff, SizeKind.Manual, 200).
                WithNamedContent(SeriesChartNames.FrontTyresSlip, SizeKind.Manual, 200).
                WithNamedContent(SeriesChartNames.FrontTyresRps, SizeKind.Manual, 200).
                WithNamedContent(SeriesChartNames.FrontTyresRpsDiff, SizeKind.Manual, 200).
                Build();

            return new LayoutDescription()
            {
                RootElement = content
            };
        }
    }
}
