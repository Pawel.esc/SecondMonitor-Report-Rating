﻿namespace SecondMonitor.Telemetry.TelemetryApplication.AggregatedCharts.ScatterPlot.Providers
{
    using Controllers.Settings;
    using Controllers.Synchronization;
    using Extractors;
    using ViewModels.LoadedLapCache;

    public class SpeedToRpmChartProvider : AbstractGearsChartProvider
    {
        public SpeedToRpmChartProvider(ILoadedLapsCache loadedLapsCache, SpeedToRpmScatterPlotExtractor speedToRpmScatterPlotExtractor, IDataPointSelectionSynchronization dataPointSelectionSynchronization, ISettingsController settingsController)
            : base(loadedLapsCache, speedToRpmScatterPlotExtractor, dataPointSelectionSynchronization, settingsController)
        {
        }

        public override string ChartName => "Speed vs RPM";
        public override AggregatedChartKind Kind => AggregatedChartKind.ScatterPlot;
    }
}