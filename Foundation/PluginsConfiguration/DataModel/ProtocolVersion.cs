namespace SecondMonitor.PluginsConfiguration.Common.DataModel
{
    public enum ProtocolVersion
    {
        V1,
        V2
    }
}