﻿namespace SecondMonitor.DataModel.DriversPresentation
{
    using System;
    using BasicProperties;

    [Serializable]
    public sealed class DriverPresentationDto
    {
        public string DriverName { get; set; }
        public bool CustomOutLineEnabled { get; set; }
        public ColorDto OutLineColor { get; set; }
    }
}