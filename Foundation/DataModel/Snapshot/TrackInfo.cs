﻿namespace SecondMonitor.DataModel.Snapshot
{
    using System;
    using System.Xml.Serialization;
    using BasicProperties;
    using ProtoBuf;

    [Serializable]
    [ProtoContract]
    public sealed class TrackInfo
    {
        public TrackInfo()
        {
            TrackName = string.Empty;
            TrackLayoutName = string.Empty;
        }

        [ProtoMember(1, IsRequired = true)]
        public string TrackName { get; set; }

        [ProtoMember(2, IsRequired = true)]
        public string TrackLayoutName { get; set; }

        [ProtoMember(3, IsRequired = true)]
        public Distance LayoutLength { get; set; }

        [XmlIgnore]
        public string TrackFullName => string.IsNullOrEmpty(TrackLayoutName) ? TrackName : $"{TrackName}-{TrackLayoutName}";
    }
}