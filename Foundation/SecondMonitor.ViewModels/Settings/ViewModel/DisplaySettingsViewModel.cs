﻿namespace SecondMonitor.ViewModels.Settings.ViewModel
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using System.Windows.Input;
    using Contracts.Commands;
    using DataModel.BasicProperties;
    using DataModel.BasicProperties.FuelConsumption;
    using DataModel.BasicProperties.Units;
    using Factory;
    using Layouts;
    using Migrations;
    using Model;
    using PluginsSettings;

    public class DisplaySettingsViewModel : AbstractViewModel<DisplaySettings>
    {
        private readonly IPluginsConfigurationViewModel _pluginsConfigurationViewModel;
        private readonly SessionsOptionsViewModel _sessionOptionsViewModel;
        private readonly ICommand _selectCustomResourceFileCommand;
        private readonly PitStopTimeDisplayMap _pitStopTimeDisplayMap;

        private VelocityUnits _velocityUnits;
        private TemperatureUnits _temperatureUnits;
        private PressureUnits _pressureUnits;
        private VolumeUnits _volumeUnits;
        private FuelCalculationScope _fuelCalculationScope;
        private int _paceLaps;
        private int _refreshRate;
        private bool _scrollToPlayer;
        private ReportingSettingsViewModel _reportingSettingsView;
        private bool _animateDriverPosition;
        private bool _animateDeltaTimes;

        private MapDisplaySettingsViewModel _mapDisplaySettingsViewModel;
        private TelemetrySettingsViewModel _telemetrySettingsViewModel;
        private MultiClassDisplayKind _multiClassDisplayKind;
        private ForceUnits _forceUnits;
        private AngleUnits _angleUnits;
        private PowerUnits _powerUnits;
        private TorqueUnits _torqueUnits;
        private bool _isGapVisualizationEnabled;
        private double _minimalGapForVisualization;
        private double _gapHeightForOneSecond;
        private double _maximumGapHeight;
        private RatingSettingsViewModel _ratingSettingsViewModel;
        private PitBoardSettingsViewModel _pitBoardSettingsViewModel;
        private TrackRecordsSettingsViewModel _trackRecordsSettingsViewModel;
        private bool _enablePedalInformation;
        private bool _enableTemperatureInformation;
        private bool _enableNonTemperatureInformation;
        private bool _enableCamberVisualization;
        private string _customResourcesPath;
        private int _driversUpdatedPerTick;
        private bool _isHwAccelerationEnabled;
        private LayoutSettingsViewModel _layoutSettingsViewModel;
        private IViewModel _layoutEditorViewModel;
        private bool _isForceSingleClassEnabled;
        private string _selectedPitStopDisplayMode;
        private bool _showPitStopTimeRelative;

        private WindowLocationSetting _windowLocationSetting;
        private Func<LayoutDescription> _defaultLayoutCallback;

        public DisplaySettingsViewModel(IViewModelFactory viewModelFactory)
        {
            _selectCustomResourceFileCommand = new RelayCommand(SelectCustomResourceFile);
            _sessionOptionsViewModel = viewModelFactory.Create<SessionsOptionsViewModel>();
            _pluginsConfigurationViewModel = viewModelFactory.Create<IPluginsConfigurationViewModel>();
            _pitStopTimeDisplayMap = new PitStopTimeDisplayMap();
            AllowedPitStopDisplayModes = _pitStopTimeDisplayMap.GetAllHumanReadableValue().ToArray();
        }

        public ICommand OpenLogDirectoryCommand => new RelayCommand(OpenLogDirectory);

        public TelemetrySettingsViewModel TelemetrySettingsViewModel
        {
            get => _telemetrySettingsViewModel;
            set
            {
                _telemetrySettingsViewModel = value;
                NotifyPropertyChanged();
            }
        }

        public TemperatureUnits TemperatureUnits
        {
            get => _temperatureUnits;
            set
            {
                _temperatureUnits = value;
                NotifyPropertyChanged();
            }
        }

        public string[] AllowedPitStopDisplayModes { get; }

        public string SelectedPitStopDisplayMode
        {
            get => _selectedPitStopDisplayMode;
            set
            {
                SetProperty(ref _selectedPitStopDisplayMode, value);
                PitStopTimeDisplayKind = _pitStopTimeDisplayMap.FromHumanReadable(value);
            }
        }

        public PitStopTimeDisplayKind PitStopTimeDisplayKind
        {
            get;
            private set;
        }

        public MultiClassDisplayKind MultiClassDisplayKind
        {
            get => _multiClassDisplayKind;
            set
            {
                _multiClassDisplayKind = value;
                NotifyPropertyChanged();
            }
        }

        public PressureUnits PressureUnits
        {
            get => _pressureUnits;
            set
            {
                _pressureUnits = value;
                NotifyPropertyChanged();
            }
        }

        public VolumeUnits VolumeUnits
        {
            get => _volumeUnits;
            set
            {
                _volumeUnits = value;
                NotifyPropertyChanged();
            }
        }

        public VelocityUnits VelocityUnits
        {
            get => _velocityUnits;
            set
            {
                _velocityUnits = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged(nameof(DistanceUnits));
                NotifyPropertyChanged(nameof(FuelPerDistanceUnits));
            }
        }

        public ForceUnits ForceUnits
        {
            get => _forceUnits;
            set => SetProperty(ref _forceUnits, value);
        }

        public AngleUnits AngleUnits
        {
            get => _angleUnits;
            set => SetProperty(ref _angleUnits, value);
        }

        public TorqueUnits TorqueUnits
        {
            get => _torqueUnits;
            set => SetProperty(ref _torqueUnits, value);
        }

        public PowerUnits PowerUnits
        {
            get => _powerUnits;
            set => SetProperty(ref _powerUnits, value);
        }

        public FuelCalculationScope FuelCalculationScope
        {
            get => _fuelCalculationScope;
            set
            {
                _fuelCalculationScope = value;
                NotifyPropertyChanged();
            }
        }

        public SessionsOptionsViewModel SessionOptionsViewModel => _sessionOptionsViewModel;

        public bool EnablePedalInformation
        {
            get => _enablePedalInformation;
            set => SetProperty(ref _enablePedalInformation, value);
        }

        public bool EnableTemperatureInformation
        {
            get => _enableTemperatureInformation;
            set => SetProperty(ref _enableTemperatureInformation, value);
        }

        public bool EnableNonTemperatureInformation
        {
            get => _enableNonTemperatureInformation;
            set => SetProperty(ref _enableNonTemperatureInformation, value);
        }

        public DistanceUnits DistanceUnits
        {
            get
            {
                switch (VelocityUnits)
                {
                    case VelocityUnits.Kph:
                        return DistanceUnits.Kilometers;
                    case VelocityUnits.Mph:
                        return DistanceUnits.Miles;
                    case VelocityUnits.Ms:
                        return DistanceUnits.Meters;
                    default:
                        return DistanceUnits.Kilometers;
                }
            }
        }

        public DistanceUnits DistanceUnitsSmall
        {
            get
            {
                switch (VelocityUnits)
                {
                    case VelocityUnits.Kph:
                        return DistanceUnits.Meters;
                    case VelocityUnits.Mph:
                        return DistanceUnits.Yards;
                    case VelocityUnits.Ms:
                        return DistanceUnits.Meters;
                    default:
                        return DistanceUnits.Meters;
                }
            }
        }

        public DistanceUnits DistanceUnitsVerySmall
        {
            get
            {
                switch (VelocityUnits)
                {
                    case VelocityUnits.Kph:
                        return DistanceUnits.Millimeter;
                    case VelocityUnits.Mph:
                        return DistanceUnits.Inches;
                    case VelocityUnits.Ms:
                        return DistanceUnits.Millimeter;
                    default:
                        return DistanceUnits.Millimeter;
                }
            }
        }

        public VelocityUnits VelocityUnitsVerySmall
        {
            get
            {
                switch (VelocityUnits)
                {
                    case VelocityUnits.Kph:
                        return VelocityUnits.MMPerSecond;
                    case VelocityUnits.Mph:
                        return VelocityUnits.InPerSecond;
                    case VelocityUnits.Ms:
                        return VelocityUnits.MMPerSecond;
                    case VelocityUnits.Fps:
                        return VelocityUnits.InPerSecond;
                    case VelocityUnits.CmPerSecond:
                        return VelocityUnits.MMPerSecond;
                    case VelocityUnits.InPerSecond:
                        return VelocityUnits.InPerSecond;
                    default:
                        return VelocityUnits.MMPerSecond;
                }
            }
        }

        public FuelPerDistanceUnits FuelPerDistanceUnits
        {
            get
            {
                switch (VelocityUnits)
                {
                    case VelocityUnits.Kph:
                        return FuelPerDistanceUnits.LitersPerHundredKm;
                    case VelocityUnits.Mph:
                        return FuelPerDistanceUnits.MilesPerGallon;
                    case VelocityUnits.Ms:
                        return FuelPerDistanceUnits.LitersPerHundredKm;
                    default:
                        return FuelPerDistanceUnits.LitersPerHundredKm;
                }
            }
        }

        public bool ShowPitStopTimeRelative
        {
            get => _showPitStopTimeRelative;
            set => SetProperty(ref _showPitStopTimeRelative, value);
        }

        public int PaceLaps
        {
            get => _paceLaps;
            set
            {
                _paceLaps = value;
                NotifyPropertyChanged();
            }
        }

        public int RefreshRate
        {
            get => _refreshRate;
            set
            {
                _refreshRate = value;
                NotifyPropertyChanged();
            }
        }

        public bool ScrollToPlayer
        {
            get => _scrollToPlayer;
            set
            {
                _scrollToPlayer = value;
                NotifyPropertyChanged();
            }
        }

        public bool AnimateDriversPosition
        {
            get => _animateDriverPosition;
            set
            {
                _animateDriverPosition = value;
                NotifyPropertyChanged();
            }
        }

        public bool AnimateDeltaTimes
        {
            get => _animateDeltaTimes;
            set
            {
                _animateDeltaTimes = value;
                NotifyPropertyChanged();
            }
        }

        public ReportingSettingsViewModel ReportingSettingsView
        {
            get => _reportingSettingsView;
            set
            {
                _reportingSettingsView = value;
                NotifyPropertyChanged();
            }
        }

        public MapDisplaySettingsViewModel MapDisplaySettingsViewModel
        {
            get => _mapDisplaySettingsViewModel;
            set
            {
                _mapDisplaySettingsViewModel = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsGapVisualizationEnabled
        {
            get => _isGapVisualizationEnabled;
            set => SetProperty(ref _isGapVisualizationEnabled, value);
        }

        public double MinimalGapForVisualization
        {
            get => _minimalGapForVisualization;
            set => SetProperty(ref _minimalGapForVisualization, value);
        }

        public double GapHeightForOneSecond
        {
            get => _gapHeightForOneSecond;
            set => SetProperty(ref _gapHeightForOneSecond, value);
        }

        public double MaximumGapHeight
        {
            get => _maximumGapHeight;
            set => SetProperty(ref _maximumGapHeight, value);
        }

        public RatingSettingsViewModel RatingSettingsViewModel
        {
            get => _ratingSettingsViewModel;
            set => SetProperty(ref _ratingSettingsViewModel, value);
        }

        public PitBoardSettingsViewModel PitBoardSettingsViewModel
        {
            get => _pitBoardSettingsViewModel;
            set => SetProperty(ref _pitBoardSettingsViewModel, value);
        }

        public TrackRecordsSettingsViewModel TrackRecordsSettingsViewModel
        {
            get => _trackRecordsSettingsViewModel;
            set => SetProperty(ref _trackRecordsSettingsViewModel, value);
        }

        public WindowLocationSetting WindowLocationSetting
        {
            get => _windowLocationSetting;
            set => _windowLocationSetting = value;
        }

        public bool EnableCamberVisualization
        {
            get => _enableCamberVisualization;
            set => SetProperty(ref _enableCamberVisualization, value);
        }

        public string CustomResourcesPath
        {
            get => _customResourcesPath;
            set => SetProperty(ref _customResourcesPath, value);
        }

        public int DriversUpdatedPerTick
        {
            get => _driversUpdatedPerTick;
            set => SetProperty(ref _driversUpdatedPerTick, value);
        }

        public bool IsHwAccelerationEnabled
        {
            get => _isHwAccelerationEnabled;
            set => SetProperty(ref _isHwAccelerationEnabled, value);
        }

        public LayoutSettingsViewModel LayoutSettingsViewModel
        {
            get => _layoutSettingsViewModel;
            set => SetProperty(ref _layoutSettingsViewModel, value);
        }

        public IViewModel LayoutEditorViewModel
        {
            get => _layoutEditorViewModel;
            set => SetProperty(ref _layoutEditorViewModel, value);
        }

        public Func<LayoutDescription> DefaultLayoutCallback
        {
            get => _defaultLayoutCallback;
            set => _defaultLayoutCallback = value;
        }

        public bool IsForceSingleClassEnabled
        {
            get => _isForceSingleClassEnabled;
            set => SetProperty(ref _isForceSingleClassEnabled, value);
        }

        public IPluginsConfigurationViewModel PluginsConfigurationViewModel => _pluginsConfigurationViewModel;

        public ICommand SelectCustomResourceFileCommand => _selectCustomResourceFileCommand;

        protected override void ApplyModel(DisplaySettings settings)
        {
            TemperatureUnits = settings.TemperatureUnits;
            PressureUnits = settings.PressureUnits;
            VolumeUnits = settings.VolumeUnits;
            VelocityUnits = settings.VelocityUnits;
            FuelCalculationScope = settings.FuelCalculationScope;
            PaceLaps = settings.PaceLaps;
            RefreshRate = settings.RefreshRate;
            ScrollToPlayer = settings.ScrollToPlayer;
            AnimateDeltaTimes = settings.AnimateDeltaTimes;
            AnimateDriversPosition = settings.AnimateDriversPosition;
            MultiClassDisplayKind = settings.MultiClassDisplayKind;
            ForceUnits = settings.ForceUnits;
            AngleUnits = settings.AngleUnits;

            IsGapVisualizationEnabled = settings.IsGapVisualizationEnabled;
            MinimalGapForVisualization = settings.MinimalGapForVisualization;
            MaximumGapHeight = settings.MaximumGapHeight;
            GapHeightForOneSecond = settings.GapHeightForOneSecond;

            MapDisplaySettingsViewModel = new MapDisplaySettingsViewModel();
            MapDisplaySettingsViewModel.FromModel(settings.MapDisplaySettings);

            SessionOptionsViewModel.FromModel(settings);

            ReportingSettingsView = new ReportingSettingsViewModel();
            ReportingSettingsView.FromModel(settings.ReportingSettings);

            TelemetrySettingsViewModel = new TelemetrySettingsViewModel();
            TelemetrySettingsViewModel.FromModel(settings.TelemetrySettings);
            WindowLocationSetting = settings.WindowLocationSetting;

            RatingSettingsViewModel = new RatingSettingsViewModel();
            RatingSettingsViewModel.FromModel(settings.RatingSettings);

            PitBoardSettingsViewModel = new PitBoardSettingsViewModel();
            PitBoardSettingsViewModel.FromModel(settings.PitBoardSettings);

            TrackRecordsSettingsViewModel = new TrackRecordsSettingsViewModel();
            TrackRecordsSettingsViewModel.FromModel(settings.TrackRecordsSettings);

            PowerUnits = settings.PowerUnits;
            TorqueUnits = settings.TorqueUnits;

            EnablePedalInformation = settings.EnablePedalInformation;
            EnableNonTemperatureInformation = settings.EnableNonTemperatureInformation;
            EnableTemperatureInformation = settings.EnableTemperatureInformation;
            EnableCamberVisualization = settings.EnableCamberVisualization;
            CustomResourcesPath = settings.CustomResourcesPath;
            DriversUpdatedPerTick = settings.DriversUpdatedPerTick;
            IsHwAccelerationEnabled = settings.IsHwAccelerationEnabled;

            LayoutSettingsViewModel = new LayoutSettingsViewModel();
            LayoutSettingsViewModel.FromModel(settings.LayoutSettings);

            IsForceSingleClassEnabled = settings.IsForceSingeClassEnabled;
            SelectedPitStopDisplayMode = _pitStopTimeDisplayMap.ToHumanReadable(settings.PitStopTimeDisplayKind);
            ShowPitStopTimeRelative = settings.ShowPitStopTimeRelative;
        }

        public override DisplaySettings SaveToNewModel()
        {
            var displaySettings = new DisplaySettings()
            {
                TemperatureUnits = TemperatureUnits,
                PressureUnits = PressureUnits,
                VolumeUnits = VolumeUnits,
                VelocityUnits = VelocityUnits,
                FuelCalculationScope = FuelCalculationScope,
                PaceLaps = PaceLaps,
                RefreshRate = RefreshRate,
                ScrollToPlayer = ScrollToPlayer,
                ReportingSettings = ReportingSettingsView.ToModel(),
                AnimateDriversPosition = AnimateDriversPosition,
                AnimateDeltaTimes = AnimateDeltaTimes,
                MapDisplaySettings = MapDisplaySettingsViewModel.SaveToNewModel(),
                TelemetrySettings = TelemetrySettingsViewModel.SaveToNewModel(),
                MultiClassDisplayKind = MultiClassDisplayKind,
                ForceUnits = ForceUnits,
                AngleUnits = AngleUnits,
                IsGapVisualizationEnabled = IsGapVisualizationEnabled,
                MinimalGapForVisualization = MinimalGapForVisualization,
                MaximumGapHeight = MaximumGapHeight,
                GapHeightForOneSecond = GapHeightForOneSecond,
                WindowLocationSetting = WindowLocationSetting,
                RatingSettings = RatingSettingsViewModel.SaveToNewModel(),
                PitBoardSettings = PitBoardSettingsViewModel.SaveToNewModel(),
                TrackRecordsSettings = TrackRecordsSettingsViewModel.SaveToNewModel(),
                PowerUnits = PowerUnits,
                TorqueUnits = TorqueUnits,
                EnablePedalInformation = EnablePedalInformation,
                EnableTemperatureInformation = EnableTemperatureInformation,
                EnableNonTemperatureInformation = EnableNonTemperatureInformation,
                EnableCamberVisualization = EnableCamberVisualization,
                CustomResourcesPath = CustomResourcesPath,
                DriversUpdatedPerTick = DriversUpdatedPerTick,
                IsHwAccelerationEnabled = IsHwAccelerationEnabled,
                LayoutSettings = LayoutSettingsViewModel.SaveToNewModel(),
                IsForceSingeClassEnabled = IsForceSingleClassEnabled,
                PitStopTimeDisplayKind = _pitStopTimeDisplayMap.FromHumanReadable(SelectedPitStopDisplayMode),
                ShowPitStopTimeRelative = ShowPitStopTimeRelative,
            };

            SessionOptionsViewModel.ApplyToModel(displaySettings);
            return displaySettings;
        }

        private void SelectCustomResourceFile()
        {
            using (OpenFileDialog fbd = new OpenFileDialog())
            {
                fbd.DefaultExt = ".xaml";
                fbd.Filter = @"xaml Files (*.xaml)|*.xaml";
                fbd.CheckFileExists = true;
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.FileName))
                {
                    CustomResourcesPath = fbd.FileName;
                }
            }
        }

        private void OpenLogDirectory()
        {
            string reportDirectory = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "SecondMonitor");
            Task.Run(
                () => { Process.Start(reportDirectory); });
        }

        public LayoutDescription GetDefaultLayout()
        {
            if (DefaultLayoutCallback == null)
            {
                throw new InvalidOperationException("Default Layout callback not initialized.");
            }

            return DefaultLayoutCallback();
        }

        public UnitsCollection GetUnitsCollection()
        {
            return new UnitsCollection()
            {
                DistanceUnitsVerySmall = DistanceUnitsVerySmall,
                DistanceUnits = DistanceUnitsSmall,
                VelocityUnits = VelocityUnits,
                VelocityUnitsSmall = VelocityUnitsVerySmall,
                TemperatureUnits = TemperatureUnits,
                PressureUnits = PressureUnits,
                AngleUnits = AngleUnits,
                ForceUnits = ForceUnits,
            };
        }
    }
}