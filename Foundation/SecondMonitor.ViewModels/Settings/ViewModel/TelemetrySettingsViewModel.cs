﻿namespace SecondMonitor.ViewModels.Settings.ViewModel
{
    using DataModel.BasicProperties;
    using Model;

    public class TelemetrySettingsViewModel : AbstractViewModel<TelemetrySettings>
    {
        private bool _isTelemetryLoggingEnabled;
        private int _loggingInterval;
        private int _maxSessionsKept;
        private bool _logInvalidLaps;
        private bool _logBestForEachDriver;
        private bool _logOpponentsBest;
        private bool _logOnlyPlayerClass;
        private int _logOnlyTop;
        private bool _logInPractice;
        private bool _logInQualification;
        private bool _logInRace;

        public bool IsTelemetryLoggingEnabled
        {
            get => _isTelemetryLoggingEnabled;
            set => SetProperty(ref _isTelemetryLoggingEnabled, value);
        }

        public int LoggingInterval
        {
            get => _loggingInterval;
            set => SetProperty(ref _loggingInterval, value);
        }

        public int MaxSessionsKept
        {
            get => _maxSessionsKept;
            set => SetProperty(ref _maxSessionsKept, value);
        }

        public bool LogInvalidLaps
        {
            get => _logInvalidLaps;
            set => SetProperty(ref _logInvalidLaps, value);
        }

        public bool LogOpponentsBest
        {
            get => _logOpponentsBest;
            set => SetProperty(ref _logOpponentsBest, value);
        }

        public bool LogBestForEachDriver
        {
            get => _logBestForEachDriver;
            set => SetProperty(ref _logBestForEachDriver, value);
        }

        public bool LogOnlyPlayerClass
        {
            get => _logOnlyPlayerClass;
            set => SetProperty(ref _logOnlyPlayerClass, value);
        }

        public int LogOnlyTop
        {
            get => _logOnlyTop;
            set => SetProperty(ref _logOnlyTop, value);
        }

        public bool LogInPractice
        {
            get => _logInPractice;
            set => SetProperty(ref _logInPractice, value);
        }

        public bool LogInQualification
        {
            get => _logInQualification;
            set => SetProperty(ref _logInQualification, value);
        }

        public bool LogInRace
        {
            get => _logInRace;
            set => SetProperty(ref _logInRace, value);
        }

        protected override void ApplyModel(TelemetrySettings model)
        {
            IsTelemetryLoggingEnabled = model.IsTelemetryLoggingEnabled;
            LoggingInterval = model.LoggingInterval;
            MaxSessionsKept = model.MaxSessionsKept;
            LogInvalidLaps = model.LogInvalidLaps;
            LogOpponentsBest = model.LogOpponentsBest;
            LogBestForEachDriver = model.LogBestForEachDriver;
            LogOnlyPlayerClass = model.LogOnlyPlayerClass;
            LogInPractice = model.LogInPractice;
            LogInQualification = model.LogInQualification;
            LogInRace = model.LogInRace;
            LogOnlyTop = model.LogOnlyTop;
        }

        public override TelemetrySettings SaveToNewModel()
        {
            return new TelemetrySettings()
            {
                IsTelemetryLoggingEnabled = IsTelemetryLoggingEnabled,
                LoggingInterval = LoggingInterval,
                MaxSessionsKept = MaxSessionsKept,
                LogInvalidLaps = LogInvalidLaps,
                LogOpponentsBest = LogOpponentsBest,
                LogBestForEachDriver = LogBestForEachDriver,
                LogOnlyPlayerClass = LogOnlyPlayerClass,
                LogInPractice = LogInPractice,
                LogInQualification = LogInQualification,
                LogInRace = LogInRace,
                LogOnlyTop = LogOnlyTop,
            };
        }

        public bool IsTelemetryEnabledForSession(SessionType sessionType)
        {
            return (sessionType == SessionType.Practice && LogInPractice) ||
                (sessionType == SessionType.Qualification && LogInQualification) ||
                (sessionType == SessionType.Race && LogInRace);
        }
    }
}