﻿namespace SecondMonitor.ViewModels.Settings.ViewModel
{
    using Model;

    public class PitBoardSettingsViewModel : AbstractViewModel<PitBoardSettings>
    {
        private bool _isEnabled;
        private int _displaySeconds;
        private bool _isYellowBoardEnabled;
        private HorizontalAlignment _horizontalAlignment;
        private VerticalAlignment _verticalAlignment;

        public bool IsEnabled
        {
            get => _isEnabled;
            set => SetProperty(ref _isEnabled, value);
        }

        public int DisplaySeconds
        {
            get => _displaySeconds;
            set => SetProperty(ref _displaySeconds, value);
        }

        public bool IsYellowBoardEnabled
        {
            get => _isYellowBoardEnabled;
            set => SetProperty(ref _isYellowBoardEnabled, value);
        }

        public HorizontalAlignment HorizontalAlignment
        {
            get => _horizontalAlignment;
            set => SetProperty(ref _horizontalAlignment, value);
        }

        public VerticalAlignment VerticalAlignment
        {
            get => _verticalAlignment;
            set => SetProperty(ref _verticalAlignment, value);
        }

        protected override void ApplyModel(PitBoardSettings model)
        {
            IsEnabled = model.IsEnabled;
            DisplaySeconds = model.DisplaySeconds;
            IsYellowBoardEnabled = model.IsYellowBoardEnabled;
            HorizontalAlignment = model.HorizontalAlignment;
            VerticalAlignment = model.VerticalAlignment;
        }

        public override PitBoardSettings SaveToNewModel()
        {
            return new PitBoardSettings()
            {
                DisplaySeconds = DisplaySeconds,
                IsEnabled = IsEnabled,
                IsYellowBoardEnabled = IsYellowBoardEnabled,
                VerticalAlignment = VerticalAlignment,
                HorizontalAlignment = HorizontalAlignment
            };
        }
    }
}