﻿namespace SecondMonitor.ViewModels.Settings.Model
{
    using System;
    using DataModel.BasicProperties;
    using DataModel.BasicProperties.Units;

    [Serializable]
    public class DisplaySettings
    {
        public DisplaySettings()
        {
        }

        public TemperatureUnits TemperatureUnits { get; set; } = TemperatureUnits.Celsius;

        public PressureUnits PressureUnits { get; set; } = PressureUnits.Kpa;

        public VolumeUnits VolumeUnits { get; set; } = VolumeUnits.Liters;

        public VelocityUnits VelocityUnits { get; set; } = VelocityUnits.Kph;

        public ForceUnits ForceUnits { get; set; } = ForceUnits.Newtons;

        public AngleUnits AngleUnits { get; set; } = AngleUnits.Degrees;

        public MultiClassDisplayKind MultiClassDisplayKind { get; set; } = MultiClassDisplayKind.ClassFirst;

        public FuelCalculationScope FuelCalculationScope { get; set; } = FuelCalculationScope.Lap;

        public TorqueUnits TorqueUnits { get; set; } = TorqueUnits.Nm;

        public PowerUnits PowerUnits { get; set; } = PowerUnits.KW;

        public PitStopTimeDisplayKind PitStopTimeDisplayKind { get; set; } = PitStopTimeDisplayKind.Both;

        public bool ShowPitStopTimeRelative { get; set; } = true;

        public int PaceLaps { get; set; } = 3;

        public int RefreshRate { get; set; } = 300;

        public bool ScrollToPlayer { get; set; } = true;

        public bool AnimateDriversPosition { get; set; } = false;

        public bool AnimateDeltaTimes { get; set; } = false;

        public bool IsGapVisualizationEnabled { get; set; } = false;

        public double MinimalGapForVisualization { get; set; } = 2;

        public double GapHeightForOneSecond { get; set; } = 25;

        public double MaximumGapHeight { get; set; } = 150;

        public bool EnablePedalInformation { get; set; } = true;

        public bool EnableTemperatureInformation { get; set; } = true;

        public bool EnableNonTemperatureInformation { get; set; } = true;

        public SessionOptions PracticeOptions { get; set; } = new SessionOptions { OrderingDisplayMode = DriverOrderKind.Absolute, TimesDisplayMode = DisplayModeEnum.Absolute, SessionName = "Practice" };

        public SessionOptions QualificationOptions { get; set; } = new SessionOptions { OrderingDisplayMode = DriverOrderKind.Absolute, TimesDisplayMode = DisplayModeEnum.Absolute, SessionName = "Quali" };

        public SessionOptions RaceOptions { get; set; } = new SessionOptions { OrderingDisplayMode = DriverOrderKind.Relative, TimesDisplayMode = DisplayModeEnum.Relative, SessionName = "Race" };

        public ReportingSettings ReportingSettings { get; set; } = new ReportingSettings();

        public MapDisplaySettings MapDisplaySettings { get; set; } = new MapDisplaySettings();

        public TelemetrySettings TelemetrySettings { get; set; } = new TelemetrySettings();

        public WindowLocationSetting WindowLocationSetting { get; set; }

        public RatingSettings RatingSettings { get; set; } = new RatingSettings();

        public PitBoardSettings PitBoardSettings { get; set; } = new PitBoardSettings();

        public TrackRecordsSettings TrackRecordsSettings { get; set; } = new TrackRecordsSettings();

        public bool EnableCamberVisualization { get; set; } = false;

        public string CustomResourcesPath { get; set; } = string.Empty;

        public int DriversUpdatedPerTick { get; set; } = 10;

        public bool IsHwAccelerationEnabled { get; set; } = false;

        public LayoutSettings LayoutSettings { get; set; } = new LayoutSettings();

        public bool IsForceSingeClassEnabled { get; set; } = false;
    }
}
