﻿namespace SecondMonitor.ViewModels.TrackRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataModel.TrackRecords;
    using Factory;

    public class TracksAllRecordsViewModel : AbstractViewModel<TrackRecord>
    {
        private readonly ViewModelFactory _viewModelFactory;
        private List<ClassRecordsViewModel> _classRecords;
        private TimeSpan _overallBest;
        private string _trackName;

        public TracksAllRecordsViewModel(ViewModelFactory viewModelFactory)
        {
            _viewModelFactory = viewModelFactory;
            _classRecords = new List<ClassRecordsViewModel>();
        }

        public string TrackName
        {
            get => _trackName;
            private set => SetProperty(ref _trackName, value);
        }

        public TimeSpan OverallBest
        {
            get => _overallBest;
            private set => SetProperty(ref _overallBest, value);
        }

        public List<ClassRecordsViewModel> ClassRecords
        {
            get => _classRecords;
            set => SetProperty(ref _classRecords, value);
        }

        protected override void ApplyModel(TrackRecord model)
        {
            if (model == null)
            {
                _classRecords = new List<ClassRecordsViewModel>();
                return;
            }

            TrackName = model.TrackName;
            OverallBest = model.OverallRecord.GetOverAllBest()?.LapTime ?? TimeSpan.Zero;

            var newClassRecords = new List<ClassRecordsViewModel>();
            var recordsGroupedByCar = model.VehicleRecords.Where(x => x.GetOverAllBest() != null).GroupBy(x => x.GetOverAllBest().CarClass);
            foreach (IGrouping<string, NamedRecordSet> recordForClass in recordsGroupedByCar)
            {
                ClassRecordsViewModel classRecordsViewModel = _viewModelFactory.Create<ClassRecordsViewModel>();
                classRecordsViewModel.FromModel(recordForClass);
                newClassRecords.Add(classRecordsViewModel);
            }

            ClassRecords = newClassRecords.OrderBy(x => x.OverallBest).ToList();
        }

        public override TrackRecord SaveToNewModel()
        {
            throw new NotSupportedException();
        }
    }
}