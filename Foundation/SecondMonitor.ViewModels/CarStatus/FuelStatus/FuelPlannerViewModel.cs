﻿namespace SecondMonitor.ViewModels.CarStatus.FuelStatus
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using Contracts.Commands;
    using DataModel.BasicProperties;
    using DataModel.Extensions;
    using DataModel.Summary.FuelConsumption;
    using Settings;

    public class FuelPlannerViewModel : AbstractViewModel<ICollection<SessionFuelConsumptionDto>>
    {
        private readonly ISettingsProvider _settingsProvider;
        private ISessionFuelConsumptionViewModel _selectedSession;
        private FuelCalculatorViewModel _calculatorForSelectedSession;
        private SessionFuelConsumptionViewModel _averageConsumption;
        private bool _isVisible;

        public FuelPlannerViewModel(ISettingsProvider settingsProvider)
        {
            _settingsProvider = settingsProvider;
            IsVisible = false;
            Sessions = new ObservableCollection<ISessionFuelConsumptionViewModel>();
            CalculatorForSelectedSession = new FuelCalculatorViewModel();
        }

        public bool IsVisible
        {
            get => _isVisible;
            set => SetProperty(ref _isVisible, value);
        }

        public ObservableCollection<ISessionFuelConsumptionViewModel> Sessions { get; }
        public ISessionFuelConsumptionViewModel SelectedSession
        {
            get => _selectedSession;
            set
            {
                _selectedSession = value;
                UpdateCalculatorViewModel();
                NotifyPropertyChanged();
            }
        }

        public FuelCalculatorViewModel CalculatorForSelectedSession
        {
            get => _calculatorForSelectedSession;
            set
            {
                _calculatorForSelectedSession = value;
                NotifyPropertyChanged();
            }
        }

        private void UpdateCalculatorViewModel()
        {
            CalculatorForSelectedSession.FuelConsumption = SelectedSession.FuelConsumption;
            CalculatorForSelectedSession.LapDistance = SelectedSession.LapDistance.InMeters;
        }

        protected override void ApplyModel(ICollection<SessionFuelConsumptionDto> model)
        {
            Sessions.ForEach(x => x.PropertyChanged -= SessionInfoOnPropertyChanged);
            if (model.Count == 0)
            {
                return;
            }

            List<SessionFuelConsumptionDto> drySessions = model.Where(x => !x.IsWetSession).ToList();
            if (drySessions.Count != 0)
            {
                _averageConsumption = new SessionFuelConsumptionViewModel(_settingsProvider) { IsCheckedVisible = false };
                _averageConsumption.FromModel(CreateAggregated(drySessions));
                _averageConsumption.SessionType = "Total (Dry)";
                Sessions.Add(_averageConsumption);
            }

            foreach (var sessionFuelConsumptionDto in model.Where(x => x != null))
            {
                SessionFuelConsumptionViewModel consumptionViewModel = new SessionFuelConsumptionViewModel(_settingsProvider)
                {
                    IsCheckedVisible = true,
                    IsChecked = !sessionFuelConsumptionDto.IsWetSession,
                    IsDeleteButtonVisible = true,
                    DeleteCommand = new RelayCommand(DeleteSelectedConsumptionInfo),
                };

                consumptionViewModel.FromModel(sessionFuelConsumptionDto);
                Sessions.Add(consumptionViewModel);
                consumptionViewModel.PropertyChanged += SessionInfoOnPropertyChanged;
            }

            SelectedSession = Sessions.First();
        }

        private void DeleteSelectedConsumptionInfo()
        {
            if (SelectedSession == null)
            {
                return;
            }

            int previouslySelectedIndex = Sessions.IndexOf(SelectedSession);
            Sessions.Remove(SelectedSession);
            SelectedSession = previouslySelectedIndex >= Sessions.Count ? Sessions.LastOrDefault() : Sessions[previouslySelectedIndex];
            RecalculateAverage();
        }

        private void SessionInfoOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != nameof(ISessionFuelConsumptionViewModel.IsChecked) || _averageConsumption == null)
            {
                return;
            }

            RecalculateAverage();
        }

        private void RecalculateAverage()
        {
            _averageConsumption.FromModel(CreateAggregated(Sessions.Where(x => x.IsChecked).Select(x => x.OriginalModel).ToList()));
            UpdateCalculatorViewModel();
        }

        private SessionFuelConsumptionDto CreateAggregated(ICollection<SessionFuelConsumptionDto> consumptionEntries)
        {
            if (!consumptionEntries.Any())
            {
                return new SessionFuelConsumptionDto(string.Empty, string.Empty, 0, string.Empty, SessionType.Na, 0, 0, Volume.FromLiters(0), DateTime.Now, false);
            }

            SessionFuelConsumptionDto baseConsumption = consumptionEntries.First();
            return new SessionFuelConsumptionDto(baseConsumption.Simulator, baseConsumption.TrackFullName, baseConsumption.LapDistance, baseConsumption.CarName,
                baseConsumption.SessionKind, consumptionEntries.Sum(x => x.ElapsedSeconds), consumptionEntries.Sum(x => x.TraveledDistanceMeters), Volume.FromLiters(consumptionEntries.Select(x => x.ConsumedFuel.InLiters).Sum(x => x)),
                DateTime.Now, baseConsumption.IsWetSession);
        }

        public override ICollection<SessionFuelConsumptionDto> SaveToNewModel()
        {
            throw new NotSupportedException();
        }
    }
}