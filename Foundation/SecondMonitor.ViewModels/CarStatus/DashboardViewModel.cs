﻿namespace SecondMonitor.ViewModels.CarStatus
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using Contracts.SimSettings;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Drivers;
    using DataModel.Snapshot.Systems;
    using SessionEvents;
    using StatusIcon;

    public class DashboardViewModel : AbstractViewModel, ISimulatorDataSetViewModel
    {
        public const string ViewModelLayoutName = "Icons and Warnings Board";
        private readonly ISimSettingsFactory _settingsFactory;
        private readonly Stopwatch _refreshStopwatch;

        public DashboardViewModel(ISimSettingsFactory settingsFactory, ISessionEventProvider sessionEventProvider)
        {
            _settingsFactory = settingsFactory;
            _refreshStopwatch = Stopwatch.StartNew();
            EngineStatus = new StatusIconViewModel();
            TransmissionStatus = new StatusIconViewModel();
            SuspensionStatus = new StatusIconViewModel();
            BodyworkStatus = new StatusIconViewModel();
            BrakesStatus = new StatusIconViewModel();

            PitLimiterStatus = new StatusIconViewModel();
            AlternatorStatus = new StatusIconViewModel();
            TyreDirtStatus = new StatusIconViewModel();
            DrsStatusIndication = new StatusIconViewModel();
            BoostIndication = new StatusIconViewModel();

            sessionEventProvider.SimulatorChanged += SessionEventProviderOnSimulatorChanged;
        }

        public StatusIconViewModel EngineStatus { get; }
        public StatusIconViewModel TransmissionStatus { get; }
        public StatusIconViewModel SuspensionStatus { get; }
        public StatusIconViewModel BodyworkStatus { get; }
        public StatusIconViewModel BrakesStatus { get; }
        public StatusIconViewModel PitLimiterStatus { get; }
        public StatusIconViewModel AlternatorStatus { get; }
        public StatusIconViewModel TyreDirtStatus { get; }
        public StatusIconViewModel DrsStatusIndication { get; }
        public StatusIconViewModel BoostIndication { get; }

        private static void ApplyDamage(DamageInformation damageInformation, StatusIconViewModel viewModel)
        {
            double damage = Math.Round(damageInformation.Damage * 100);
            viewModel.AdditionalText = damage > 0 ? damage.ToString("F0") : string.Empty;

            if (damageInformation.Damage < damageInformation.MediumDamageThreshold)
            {
                viewModel.IconState = StatusIconState.Unlit;
                return;
            }

            if (damageInformation.Damage < damageInformation.HeavyDamageThreshold)
            {
                viewModel.IconState = StatusIconState.Warning;
                return;
            }

            viewModel.IconState = StatusIconState.Error;
        }

        public void ApplyDateSet(SimulatorDataSet dataSet)
        {
            if (dataSet?.PlayerInfo?.CarInfo?.CarDamageInformation == null || _refreshStopwatch.ElapsedMilliseconds < 200)
            {
                return;
            }

            CarDamageInformation carDamage = dataSet.PlayerInfo?.CarInfo?.CarDamageInformation;

            ApplyDamage(carDamage.Bodywork, BodyworkStatus);
            ApplyDamage(carDamage.Engine, EngineStatus);
            ApplyDamage(carDamage.Suspension, SuspensionStatus);
            ApplyDamage(carDamage.Transmission, TransmissionStatus);
            ApplyBrakesDamage(dataSet);
            UpdatePitLimiterStatus(dataSet);
            UpdateAlternatorStatus(dataSet);
            UpdateDirtLevel(dataSet.PlayerInfo.CarInfo);
            UpdateDrsStatus(dataSet.PlayerInfo.CarInfo);
            UpdateBoostStatus(dataSet.PlayerInfo.CarInfo.BoostSystem);

            if (dataSet.PlayerInfo.CarInfo.WheelsInfo.AllWheels.Any(x => x.Detached))
            {
                SuspensionStatus.IconState = StatusIconState.Error;
            }

            _refreshStopwatch.Restart();
        }

        private void UpdateBoostStatus(BoostSystem boostSystem)
        {
            if (boostSystem.CooldownTimer == TimeSpan.Zero && boostSystem.TimeRemaining == TimeSpan.Zero)
            {
                BoostIndication.AdditionalText = boostSystem.ActivationsRemaining < 0 ? string.Empty : boostSystem.ActivationsRemaining.ToString();
            }
            else
            {
                BoostIndication.AdditionalText = boostSystem.TimeRemaining == TimeSpan.Zero ? boostSystem.CooldownTimer.TotalSeconds.ToString("F0") : boostSystem.TimeRemaining.TotalSeconds.ToString("F0");
            }

            switch (boostSystem.BoostStatus)
            {
                case BoostStatus.UnAvailable:
                    BoostIndication.IconState = StatusIconState.Unlit;
                    break;
                case BoostStatus.Available:
                    BoostIndication.IconState = StatusIconState.Information;
                    break;
                case BoostStatus.InUse:
                    BoostIndication.IconState = StatusIconState.Ok;
                    break;
                case BoostStatus.Cooldown:
                    BoostIndication.IconState = StatusIconState.Warning;
                    break;
            }
        }

        private void UpdateDrsStatus(CarInfo playerInfoCarInfo)
        {
            DrsStatusIndication.AdditionalText = playerInfoCarInfo.DrsSystem.DrsActivationLeft < 0 ? string.Empty : playerInfoCarInfo.DrsSystem.DrsActivationLeft.ToString();
            switch (playerInfoCarInfo.DrsSystem.DrsStatus)
            {
                case DrsStatus.Available:
                    DrsStatusIndication.IconState = StatusIconState.Information;
                    break;
                case DrsStatus.InUse:
                    DrsStatusIndication.IconState = StatusIconState.Ok;
                    break;
                default:
                    DrsStatusIndication.IconState = StatusIconState.Unlit;
                    break;
            }
        }

        private void UpdateDirtLevel(CarInfo playerCar)
        {
            double maxDirt = playerCar.WheelsInfo.AllWheels.Max(x => x.DirtLevel);
            if (maxDirt < 0.01)
            {
                TyreDirtStatus.IconState = StatusIconState.Unlit;
                TyreDirtStatus.AdditionalText = string.Empty;
                return;
            }

            TyreDirtStatus.AdditionalText = ((int)(maxDirt * 100)).ToString();
            TyreDirtStatus.IconState = maxDirt > 0.5 ? StatusIconState.Error : StatusIconState.Warning;
        }

        private void UpdateAlternatorStatus(SimulatorDataSet dataSet)
        {
            CarInfo playerInfoCarInfo = dataSet.PlayerInfo.CarInfo;
            AlternatorStatus.IconState = dataSet.SessionInfo.SessionType != SessionType.Na && playerInfoCarInfo.EngineRpm < 10 ? StatusIconState.Error : StatusIconState.Unlit;
        }

        public void Reset()
        {
        }

        private void UpdatePitLimiterStatus(SimulatorDataSet dataSet)
        {
            DriverInfo driver = dataSet.PlayerInfo;
            if (driver.InPits)
            {
                PitLimiterStatus.AdditionalText = string.Empty;
                PitLimiterStatus.IconState = driver.CarInfo.SpeedLimiterEngaged ? StatusIconState.Ok : StatusIconState.Warning;
                return;
            }

            if (driver.CarInfo.SpeedLimiterEngaged)
            {
                PitLimiterStatus.AdditionalText = string.Empty;
                PitLimiterStatus.IconState = StatusIconState.Error;
                return;
            }

            PitWindowInformation sessionInfoPitWindow = dataSet.SessionInfo.PitWindow;
            if (sessionInfoPitWindow.PitWindowState == PitWindowState.BeforePitWindow)
            {
                PitLimiterStatus.IconState = StatusIconState.Unlit;
                PitLimiterStatus.AdditionalText = dataSet.SessionInfo.SessionLengthType == SessionLengthType.Laps ? (sessionInfoPitWindow.PitWindowStart - driver.CompletedLaps) + "L" :
                    Math.Ceiling((TimeSpan.FromMinutes(dataSet.SessionInfo.PitWindow.PitWindowStart) - dataSet.SessionInfo.SessionTime).TotalMinutes).ToString("N0") + "M";
                return;
            }

            if (sessionInfoPitWindow.PitWindowState == PitWindowState.InPitWindow)
            {
                PitLimiterStatus.AdditionalText = dataSet.SessionInfo.SessionLengthType == SessionLengthType.Laps ? (sessionInfoPitWindow.PitWindowEnd - driver.CompletedLaps) + "L" :
                    Math.Ceiling((TimeSpan.FromMinutes(dataSet.SessionInfo.PitWindow.PitWindowEnd) - dataSet.SessionInfo.SessionTime).TotalMinutes).ToString("N0") + "M";
                PitLimiterStatus.IconState = StatusIconState.Information;
                return;
            }

            PitLimiterStatus.AdditionalText = string.Empty;
            PitLimiterStatus.IconState = StatusIconState.Unlit;
        }

        private void SessionEventProviderOnSimulatorChanged(object sender, DataSetArgs e)
        {
            ISimSettings simSettings = _settingsFactory.Create(e.DataSet.Source);
            ApplySimSettings(simSettings);
        }

        private void ApplySimSettings(ISimSettings simSettings)
        {
            EngineStatus.IsVisible = simSettings.IsEngineDamageProvided;
            TransmissionStatus.IsVisible = simSettings.IsTransmissionDamageProvided;
            SuspensionStatus.IsVisible = simSettings.IsSuspensionDamageProvided;
            BodyworkStatus.IsVisible = simSettings.IsBodyworkDamageProvided;
            TyreDirtStatus.IsVisible = simSettings.IsTyresDirtProvided;
            DrsStatusIndication.IsVisible = simSettings.IsDrsInformationProvided;
            BoostIndication.IsVisible = simSettings.IsBoostInformationProvided;
            AlternatorStatus.IsVisible = simSettings.IsAlternatorStatusShown;
            BrakesStatus.IsVisible = simSettings.IsBrakesDamageProvided;
        }

        private void ApplyBrakesDamage(SimulatorDataSet dataSet)
        {
            Wheels wheels = dataSet.PlayerInfo.CarInfo.WheelsInfo;
            var brakesDamageOrdered = wheels.AllWheels.OrderByDescending(x => x.BrakesDamage.Damage).ToList();
            var mostlyDamaged = brakesDamageOrdered.First();
            ApplyDamage(mostlyDamaged.BrakesDamage, BrakesStatus);

            var frontDamage = wheels.FrontLeft.BrakesDamage.Damage > wheels.FrontRight.BrakesDamage.Damage ? wheels.FrontLeft : wheels.FrontRight;
            var rearDamage = wheels.RearLeft.BrakesDamage.Damage > wheels.RearRight.BrakesDamage.Damage ? wheels.RearLeft : wheels.RearRight;

            double frontDamageValue = Math.Round(frontDamage.BrakesDamage.Damage * 100);
            double rearDamageValue = Math.Round(rearDamage.BrakesDamage.Damage * 100);

            BrakesStatus.AdditionalText = frontDamageValue > 0 || rearDamageValue > 0 ? $"{frontDamageValue:F0}/{rearDamageValue:F0}" : string.Empty;

            if (mostlyDamaged.BrakesDamage.Damage < mostlyDamaged.BrakesDamage.MediumDamageThreshold)
            {
                BrakesStatus.IconState = StatusIconState.Unlit;
                return;
            }

            if (mostlyDamaged.BrakesDamage.Damage < mostlyDamaged.BrakesDamage.HeavyDamageThreshold)
            {
                BrakesStatus.IconState = StatusIconState.Warning;
                return;
            }

            BrakesStatus.IconState = StatusIconState.Error;
        }
    }
}