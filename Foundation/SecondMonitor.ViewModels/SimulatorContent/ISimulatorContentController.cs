﻿namespace SecondMonitor.ViewModels.SimulatorContent
{
    using Controllers;
    using DataModel.Snapshot;

    public interface ISimulatorContentController : IController, ISimulatorDataSetVisitor
    {
    }
}