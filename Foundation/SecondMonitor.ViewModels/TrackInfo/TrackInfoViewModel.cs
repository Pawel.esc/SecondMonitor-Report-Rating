﻿namespace SecondMonitor.ViewModels.TrackInfo
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;

    public class TrackInfoViewModel : AbstractViewModel, ISimulatorDataSetViewModel
    {
        public const string ViewModelLayoutName = "Track Name and Weather";
        private const int TemperaturesDeltaTimeMs = 5 * 60 * 1000;
        private const int DeltaSnapshotPeriod = 10 * 1000;
        private const int MaxSnapshots = (TemperaturesDeltaTimeMs / DeltaSnapshotPeriod) + 1;

        private readonly Queue<Temperature> _storedTrackTemperatures;
        private readonly Queue<Temperature> _storedAirTemperatures;

        private string _trackName;
        private string _sessionType;
        private string _rainIntensity;
        private string _trackWetness;
        private string _airTemperatureInfo;
        private string _trackTemperatureInfo;
        private string _airTemperatureSecondaryInfo;
        private string _trackTemperatureSecondaryInfo;
        private bool _weatherInfoAvailable;
        private Distance _layoutLength;

        private TemperatureUnits _temperatureUnits;
        private DistanceUnits _distanceUnits;

        private Temperature _lastAirTemperature;
        private Temperature _lastTrackTemperature;
        private Temperature _startSessionAirTemperature;
        private Temperature _startSessionTrackTemperature;

        private string _trackTemperatureDeltaFormatted;
        private string _airTemperatureDeltaFormatted;
        private Stopwatch _storeTemperatureStopwatch;

        public TrackInfoViewModel()
        {
            _storedTrackTemperatures = new Queue<Temperature>();
            _storedAirTemperatures = new Queue<Temperature>();
            Reset();
        }

        public string TrackName
        {
            get => _trackName;
            set
            {
                if (_trackName == value)
                {
                    return;
                }

                _trackName = value;
                NotifyPropertyChanged();
            }
        }

        public Distance LayoutLength
        {
            get => _layoutLength;
            private set
            {
                _layoutLength = value;
                NotifyPropertyChanged();
            }
        }

        public DistanceUnits DistanceUnits
        {
            get => _distanceUnits;
            set
            {
                _distanceUnits = value;
                NotifyPropertyChanged();
            }
        }

        public string SessionType
        {
            get => _sessionType;
            set
            {
                if (_sessionType == value)
                {
                    return;
                }

                _sessionType = value;
                NotifyPropertyChanged();
            }
        }

        public string RainIntensity
        {
            get => _rainIntensity;
            set
            {
                _rainIntensity = value;
                NotifyPropertyChanged();
            }
        }

        public string AirTemperatureInfo
        {
            get => _airTemperatureInfo;
            set
            {
                if (_airTemperatureInfo == value)
                {
                    return;
                }

                _airTemperatureInfo = value;
                NotifyPropertyChanged();
            }
        }

        public string TrackTemperatureInfo
        {
            get => _trackTemperatureInfo;
            set
            {
                if (_trackTemperatureInfo == value)
                {
                    return;
                }

                _trackTemperatureInfo = value;
                NotifyPropertyChanged();
            }
        }

        public string AirTemperatureSecondaryInfo
        {
            get => _airTemperatureSecondaryInfo;
            set => SetProperty(ref _airTemperatureSecondaryInfo, value);
        }

        public string TrackTemperatureSecondaryInfo
        {
            get => _trackTemperatureSecondaryInfo;
            set => SetProperty(ref _trackTemperatureSecondaryInfo, value);
        }

        public TemperatureUnits TemperatureUnits
        {
            get => _temperatureUnits;
            set
            {
                _temperatureUnits = value;
                RefreshTemperatures();
            }
        }

        public bool WeatherInfoAvailable
        {
            get => _weatherInfoAvailable;
            set
            {
                if (_weatherInfoAvailable == value)
                {
                    return;
                }

                _weatherInfoAvailable = value;
                NotifyPropertyChanged();
            }
        }

        public string TrackWetness
        {
            get => _trackWetness;
            set => SetProperty(ref _trackWetness, value);
        }

        public void ApplyDateSet(SimulatorDataSet dataSet)
        {
            if (dataSet?.SessionInfo?.TrackInfo == null || dataSet.SessionInfo?.WeatherInfo == null)
            {
                return;
            }

            _lastTrackTemperature = dataSet.SessionInfo.WeatherInfo.TrackTemperature;
            _lastAirTemperature = dataSet.SessionInfo.WeatherInfo.AirTemperature;

            if (_startSessionAirTemperature == null && dataSet.SessionInfo.SessionPhase == SessionPhase.Green)
            {
                _startSessionAirTemperature = _lastAirTemperature;
                _startSessionTrackTemperature = _lastTrackTemperature;
                StoreCurrentTemperaturesAndUpdateDeltas();
            }

            if (dataSet.SessionInfo.SessionPhase == SessionPhase.Green && _storeTemperatureStopwatch.ElapsedMilliseconds > DeltaSnapshotPeriod)
            {
                StoreCurrentTemperaturesAndUpdateDeltas();
            }

            SessionType = dataSet.SessionInfo.SessionType.ToString();
            FormatTrackName(dataSet.SessionInfo.TrackInfo.TrackName, dataSet.SessionInfo.TrackInfo.TrackLayoutName);
            RainIntensity = dataSet.SessionInfo.WeatherInfo.RainIntensity + "%";
            TrackWetness = dataSet.SessionInfo.WeatherInfo.TrackWetness > 0 ? dataSet.SessionInfo.WeatherInfo.TrackWetness + "%" : string.Empty;
            LayoutLength = dataSet.SessionInfo.TrackInfo.LayoutLength;
            RefreshTemperatures();
        }

        private void StoreCurrentTemperaturesAndUpdateDeltas()
        {
            _storedTrackTemperatures.Enqueue(_lastTrackTemperature);
            _storedAirTemperatures.Enqueue(_lastAirTemperature);
            _storeTemperatureStopwatch.Restart();

            _trackTemperatureDeltaFormatted = (_lastTrackTemperature.GetValueInUnits(TemperatureUnits) - _storedTrackTemperatures.Peek().GetValueInUnits(TemperatureUnits)).ToString("+0.0;-0.0;0") + Temperature.GetUnitSymbol(TemperatureUnits);
            _airTemperatureDeltaFormatted = (_lastAirTemperature.GetValueInUnits(TemperatureUnits) - _storedAirTemperatures.Peek().GetValueInUnits(TemperatureUnits)).ToString("+0.0;-0.0;0") + Temperature.GetUnitSymbol(TemperatureUnits);

            if (_storedTrackTemperatures.Count > MaxSnapshots)
            {
                _storedAirTemperatures.Dequeue();
                _storedTrackTemperatures.Dequeue();
            }
        }

        private void FormatTrackName(string trackName, string trackLayout)
        {
            if (string.IsNullOrEmpty(trackLayout))
            {
                TrackName = trackName;
                return;
            }

            TrackName = trackName + " (" + trackLayout + ")";
        }

        private void RefreshTemperatures()
        {
            if (_lastAirTemperature == null || _lastTrackTemperature == null)
            {
                return;
            }

            WeatherInfoAvailable =
                _lastTrackTemperature != Temperature.Zero || _lastTrackTemperature != Temperature.Zero;

            AirTemperatureInfo = _lastAirTemperature.GetFormattedWithUnits(1, TemperatureUnits);
            TrackTemperatureInfo = _lastTrackTemperature.GetFormattedWithUnits(1, TemperatureUnits);

            AirTemperatureSecondaryInfo = $"{_airTemperatureDeltaFormatted} / {GetAirDifferenceFromStart()}";
            TrackTemperatureSecondaryInfo = $"{_trackTemperatureDeltaFormatted} / {GetTrackDifferenceFromStart()}";
        }

        private string GetTrackDifferenceFromStart()
        {
            if (_startSessionTrackTemperature == null)
            {
                return "0" + Temperature.GetUnitSymbol(TemperatureUnits);
            }

            return (_lastTrackTemperature.GetValueInUnits(TemperatureUnits) - _startSessionTrackTemperature.GetValueInUnits(TemperatureUnits)).ToString("+0.0;-0.0;0") + Temperature.GetUnitSymbol(TemperatureUnits);
        }

        private string GetAirDifferenceFromStart()
        {
            if (_startSessionAirTemperature == null)
            {
                return "0" + Temperature.GetUnitSymbol(TemperatureUnits);
            }

            return (_lastAirTemperature.GetValueInUnits(TemperatureUnits) - _startSessionAirTemperature.GetValueInUnits(TemperatureUnits)).ToString("+0.0;-0.0;0") + Temperature.GetUnitSymbol(TemperatureUnits);
        }

        public void Reset()
        {
            _trackName = "No Track";
            _sessionType = "No Session";
            _rainIntensity = "0%";
            _trackWetness = string.Empty;
            _airTemperatureInfo = string.Empty;
            _trackTemperatureInfo = string.Empty;
            _weatherInfoAvailable = false;
            _storeTemperatureStopwatch = Stopwatch.StartNew();
            _storedTrackTemperatures.Clear();
            _storedAirTemperatures.Clear();
            _startSessionAirTemperature = null;
            _startSessionTrackTemperature = null;
            _trackTemperatureDeltaFormatted = _airTemperatureDeltaFormatted = "0" + Temperature.GetUnitSymbol(TemperatureUnits);
        }
    }
}