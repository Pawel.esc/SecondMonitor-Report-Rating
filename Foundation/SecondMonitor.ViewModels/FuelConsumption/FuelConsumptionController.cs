﻿namespace SecondMonitor.ViewModels.FuelConsumption
{
    using System;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Threading.Tasks;
    using CarStatus;
    using CarStatus.FuelStatus;
    using Contracts.Commands;
    using Controllers;
    using DataModel.BasicProperties;
    using DataModel.Extensions;
    using DataModel.Snapshot;
    using DataModel.Summary.FuelConsumption;
    using Factory;
    using SessionEvents;
    using Settings;

    public class FuelConsumptionController : IController, IFuelPredictionProvider
    {
        private static readonly TimeSpan MinimumSessionLength = TimeSpan.FromMinutes(2);
        private readonly FuelConsumptionRepository _fuelConsumptionRepository;
        private readonly Lazy<OverallFuelConsumptionHistory> _overallFuelHistoryLazy;
        private readonly ISessionEventProvider _sessionEventProvider;
        private readonly IViewModelFactory _viewModelFactory;
        private bool _isFuelCalculatorShown;
        private SimulatorDataSet _lastDataSet;
        private bool _autoOpened;

        public FuelConsumptionController(FuelConsumptionRepository fuelConsumptionRepository, IPaceProvider paceProvider, ISessionEventProvider sessionEventProvider, ISettingsProvider settingsProvider, IViewModelFactory viewModelFactory)
        {
            _fuelConsumptionRepository = fuelConsumptionRepository;
            _sessionEventProvider = sessionEventProvider;
            _viewModelFactory = viewModelFactory;
            _overallFuelHistoryLazy = new Lazy<OverallFuelConsumptionHistory>(LoadOverallFuelConsumptionHistory);
            FuelOverviewViewModel = new FuelOverviewViewModel(new SessionRemainingCalculator(paceProvider), settingsProvider, viewModelFactory)
            {
                IsVisible = true,
                ShowFuelCalculatorCommand = new RelayCommand(ShowFuelCalculator),
                HideFuelCalculatorCommand = new RelayCommand(HideFuelCalculator),
            };
        }

        public FuelOverviewViewModel FuelOverviewViewModel { get; }
        private OverallFuelConsumptionHistory OverallFuelConsumptionHistory => _overallFuelHistoryLazy.Value;

        private bool IsFuelCalculatorShown
        {
            get => _isFuelCalculatorShown;
            set
            {
                _isFuelCalculatorShown = value;
                //FuelOverviewViewModel.IsVisible = !value;
                FuelOverviewViewModel.FuelPlannerViewModel.IsVisible = value;
            }
        }

        public Task StartControllerAsync()
        {
            return Task.CompletedTask;
        }

        public Task StopControllerAsync()
        {
            StoreCurrentSessionConsumption();
            if (_overallFuelHistoryLazy.IsValueCreated)
            {
                _fuelConsumptionRepository.Save(OverallFuelConsumptionHistory);
            }

            return Task.CompletedTask;
        }

        public void ApplyDataSet(SimulatorDataSet dataSet)
        {
            _lastDataSet = dataSet;
            FuelOverviewViewModel.ApplyDateSet(dataSet);
            if (dataSet.SessionInfo.SessionType == SessionType.Race && dataSet.SessionInfo.SessionPhase == SessionPhase.Countdown && dataSet.PlayerInfo?.Speed.InKph < 5 && !_autoOpened)
            {
                AutoOpenFuelCalculator(dataSet);
            }

            if (_autoOpened && (dataSet.SessionInfo.SessionPhase != SessionPhase.Countdown || dataSet.SessionInfo.SessionType != SessionType.Race || dataSet.PlayerInfo?.Speed.InKph > 60))
            {
                _autoOpened = false;
                HideFuelCalculator();
            }
        }

        public void Reset()
        {
            StoreCurrentSessionConsumption();
            FuelOverviewViewModel.Reset();
        }

        private void AutoOpenFuelCalculator(SimulatorDataSet dataSet)
        {
            _autoOpened = true;
            ShowFuelCalculator();
            if (!_sessionEventProvider.CurrentSimulatorSettings.IsSessionLengthAvailableBeforeStart)
            {
                return;
            }

            int totalMinutes = 0;
            int totalLaps = 2;
            switch (dataSet.SessionInfo.SessionLengthType)
            {
                case SessionLengthType.Na:
                    break;
                case SessionLengthType.Laps:
                    totalLaps += dataSet.SessionInfo.TotalNumberOfLaps;
                    break;
                case SessionLengthType.Time:
                    totalMinutes = (int)Math.Ceiling(dataSet.SessionInfo.SessionTimeRemaining / 60);
                    break;
                case SessionLengthType.TimeWithExtraLap:
                    totalMinutes = (int)Math.Ceiling(dataSet.SessionInfo.SessionTimeRemaining / 60);
                    totalLaps++;
                    break;
            }

            FuelOverviewViewModel.FuelPlannerViewModel.CalculatorForSelectedSession.RequiredLaps = totalLaps;
            FuelOverviewViewModel.FuelPlannerViewModel.CalculatorForSelectedSession.RequiredMinutes = totalMinutes;
        }

        private void ShowFuelCalculator()
        {
            if (_lastDataSet?.PlayerInfo == null)
            {
                return;
            }

            var fuelConsumptionEntries = OverallFuelConsumptionHistory.GetTrackConsumptionHistoryEntries(_lastDataSet.Source, _sessionEventProvider.LastTrackFullName, _sessionEventProvider.CurrentCarName);
            var currentSessionEntry = CreateCurrentSessionFuelConsumptionDto();
            if (currentSessionEntry != null)
            {
                fuelConsumptionEntries = new[] { currentSessionEntry }.Concat(fuelConsumptionEntries).ToList();
            }

            if (fuelConsumptionEntries.Count == 0)
            {
                return;
            }

            if (FuelOverviewViewModel.FuelPlannerViewModel != null)
            {
                FuelOverviewViewModel.FuelPlannerViewModel.Sessions.CollectionChanged -= SessionsOnCollectionChanged;
            }

            FuelPlannerViewModel fuelPlannerViewModel = _viewModelFactory.Create<FuelPlannerViewModel>();
            fuelPlannerViewModel.FromModel(fuelConsumptionEntries.ToList());
            FuelOverviewViewModel.FuelPlannerViewModel = fuelPlannerViewModel;
            IsFuelCalculatorShown = FuelOverviewViewModel.FuelPlannerViewModel.Sessions.Count != 0;

            fuelPlannerViewModel.Sessions.CollectionChanged += SessionsOnCollectionChanged;
        }

        private void SessionsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action != NotifyCollectionChangedAction.Remove)
            {
                return;
            }

            var removedInfo = e.OldItems.OfType<ISessionFuelConsumptionViewModel>().Select(x => x.OriginalModel);
            removedInfo.ForEach(x => OverallFuelConsumptionHistory.RemoveTrackConsumptionHistoryEntry(x));
        }

        private void HideFuelCalculator()
        {
            IsFuelCalculatorShown = false;
        }

        private void StoreCurrentSessionConsumption()
        {
            if (_lastDataSet?.PlayerInfo == null || FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.ElapsedTime < MinimumSessionLength ||
                FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.ConsumedFuel.InLiters < 5)
            {
                return;
            }

            OverallFuelConsumptionHistory.AddTrackConsumptionHistoryEntry(CreateCurrentSessionFuelConsumptionDto());
        }

        private SessionFuelConsumptionDto CreateCurrentSessionFuelConsumptionDto()
        {
            if (_lastDataSet?.PlayerInfo == null || FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.ElapsedTime < MinimumSessionLength)
            {
                return null;
            }

            var currentSessionFuelConsumption = new SessionFuelConsumptionDto()
            {
                IsWetSession = FuelOverviewViewModel.IsWetSession,
                CarName = _sessionEventProvider.CurrentCarName,
                LapDistance = _lastDataSet.SessionInfo.TrackInfo.LayoutLength.InMeters,
                RecordDate = DateTime.Now,
                SessionKind = _lastDataSet.SessionInfo.SessionType,
                Simulator = _lastDataSet.Source,
                TrackFullName = _lastDataSet.SessionInfo.TrackInfo.TrackFullName,
                TraveledDistanceMeters = FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.TraveledDistance.InMeters,
                ConsumedFuel = FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.ConsumedFuel,
                ElapsedSeconds = FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.ElapsedTime.TotalSeconds,
            };
            return currentSessionFuelConsumption;
        }

        private OverallFuelConsumptionHistory LoadOverallFuelConsumptionHistory()
        {
            return _fuelConsumptionRepository.LoadRatingsOrCreateNew();
        }

        public TimeSpan GetRemainingFuelTime()
        {
            return FuelOverviewViewModel.TimeLeft;
        }
    }
}