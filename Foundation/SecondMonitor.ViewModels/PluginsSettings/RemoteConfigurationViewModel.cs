﻿namespace SecondMonitor.ViewModels.PluginsSettings
{
    using System.Net;
    using System.Net.Sockets;
    using Factory;
    using PluginsConfiguration.Common.DataModel;

    public class RemoteConfigurationViewModel : AbstractViewModel<RemoteConfiguration>, IRemoteConfigurationViewModel
    {
        private readonly IViewModelFactory _viewModelFactory;
        private readonly IHostAddressValidator _hostAddressValidator;
        private string _hostAddress;
        private bool _isFindInLanEnabled;
        private int _port;
        private bool _isRemoteConnectorEnabled;
        private ProtocolVersion _protocolVersion;
        private IBroadcastLimitSettingsViewModel _borBroadcastLimitSettingsViewModel;

        public RemoteConfigurationViewModel(IViewModelFactory viewModelFactory, IHostAddressValidator hostAddressValidator)
        {
            _viewModelFactory = viewModelFactory;
            _hostAddressValidator = hostAddressValidator;
        }

        public string HostAddress
        {
            get => _hostAddress;
            set
            {
                if (_hostAddressValidator.IsValidHostAddress(value))
                {
                    SetProperty(ref _hostAddress, value);
                }
            }
        }

        public bool IsFindInLanEnabled
        {
            get => _isFindInLanEnabled;
            set => SetProperty(ref _isFindInLanEnabled, value);
        }

        public bool IsRemoteConnectorEnabled
        {
            get => _isRemoteConnectorEnabled;
            set => SetProperty(ref _isRemoteConnectorEnabled, value);
        }

        public int Port
        {
            get => _port;
            set => SetProperty(ref _port, value);
        }

        public ProtocolVersion ProtocolVersion
        {
            get => this._protocolVersion;
            set => SetProperty(ref this._protocolVersion, value);
        }

        public IBroadcastLimitSettingsViewModel BroadcastLimitSettingsViewModel
        {
            get => _borBroadcastLimitSettingsViewModel;
            set => SetProperty(ref _borBroadcastLimitSettingsViewModel, value);
        }

        protected override void ApplyModel(RemoteConfiguration model)
        {
            if (string.IsNullOrEmpty(model.HostAddress))
            {
                HostAddress = model.IpAddress;
            }
            else
            {
                _hostAddress = model.HostAddress;
            }

            IsFindInLanEnabled = model.IsFindInLanEnabled;
            Port = model.Port;
            IsRemoteConnectorEnabled = model.IsRemoteConnectorEnabled;
            ProtocolVersion = model.ProtocolVersion;

            IBroadcastLimitSettingsViewModel newBroadcastLimitSettingsViewModel =
                _viewModelFactory.Create<IBroadcastLimitSettingsViewModel>();
            newBroadcastLimitSettingsViewModel.FromModel(model.BroadcastLimitSettings);
            BroadcastLimitSettingsViewModel = newBroadcastLimitSettingsViewModel;
        }

        public override RemoteConfiguration SaveToNewModel()
        {
            return new RemoteConfiguration()
            {
                HostAddress = HostAddress,
                IsFindInLanEnabled = IsFindInLanEnabled,
                Port = Port,
                IsRemoteConnectorEnabled = IsRemoteConnectorEnabled,
                ProtocolVersion = ProtocolVersion,
                BroadcastLimitSettings = BroadcastLimitSettingsViewModel.SaveToNewModel()
            };
        }

        private static bool IsValidIpAddress(string value)
        {
            if (IPAddress.TryParse(value, out IPAddress ipAddress))
            {
                return ipAddress.AddressFamily == AddressFamily.InterNetwork;
            }

            return false;
        }
    }
}