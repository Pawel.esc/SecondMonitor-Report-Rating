﻿namespace SecondMonitor.ViewModels.PluginsSettings
{
    using System.Net;
    using System.Net.Sockets;

    internal class HostAddressValidator : IHostAddressValidator
    {
        public bool IsValidHostAddress(string value)
        {
            if (IPAddress.TryParse(value, out var ipAddress))
            {
                return ipAddress.AddressFamily == AddressFamily.InterNetwork;
            }

            return false;
        }
    }
}