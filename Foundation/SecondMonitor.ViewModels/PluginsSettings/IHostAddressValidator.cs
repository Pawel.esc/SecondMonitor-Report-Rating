﻿namespace SecondMonitor.ViewModels.PluginsSettings
{
    public interface IHostAddressValidator
    {
        bool IsValidHostAddress(string value);
    }
}