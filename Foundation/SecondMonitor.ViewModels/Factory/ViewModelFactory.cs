﻿namespace SecondMonitor.ViewModels.Factory
{
    using System.Collections.Generic;
    using Contracts.NInject;
    using Ninject.Syntax;
    using ViewModels;

    public class ViewModelFactory : GenericFactory, IViewModelFactory
    {
        public ViewModelFactory(IResolutionRoot resolutionRoot) : base(resolutionRoot)
        {
        }

        public new T Create<T>() where T : IViewModel
        {
            return base.Create<T>();
        }

        public new IEnumerable<T> CreateAll<T>() where T : IViewModel
        {
            return base.CreateAll<T>();
        }
    }
}