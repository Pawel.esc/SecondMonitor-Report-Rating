﻿namespace SecondMonitor.ViewModels.Track
{
    public enum DriverState
    {
        Normal,
        Yellow,
        Lapping,
        Lapped,
        InPits,
        InPitsMoving,
        Finished
    }
}