﻿namespace SecondMonitor.Contracts.NInject
{
    using System.Collections.Generic;
    using Ninject;
    using Ninject.Parameters;
    using Ninject.Syntax;

    public class NamedConstructorParameter<TFactory>
    {
        private readonly IResolutionRoot _resolutionRoot;
        private readonly string _parameterName;
        private readonly List<IParameter> _allParameters;

        public NamedConstructorParameter(IResolutionRoot resolutionRoot, string parameterName, params IParameter[] previousParameters)
        {
            _resolutionRoot = resolutionRoot;
            _parameterName = parameterName;
            _allParameters = new List<IParameter>(previousParameters);
        }

        public TFactory To(object instance)
        {
            _allParameters.Add(new ConstructorArgument(_parameterName, instance));
            IParameter[] parameters = _allParameters.ToArray();
            TypeMatchingConstructorArgument newFactoryArgument = new TypeMatchingConstructorArgument(typeof(IParameter[]), (_, __) => parameters);
            TFactory factory = _resolutionRoot.Get<TFactory>(newFactoryArgument);
            _allParameters.Clear();
            return factory;
        }
    }
}