﻿namespace SecondMonitor.Contracts.NInject
{
    using System.Collections.Generic;
    using Ninject;
    using Ninject.Parameters;
    using Ninject.Syntax;

    public class GenericFactory
    {
        private readonly IParameter[] _constructorParameters;

        public GenericFactory(IResolutionRoot resolutionRoot, params IParameter[] constructorParameters)
        {
            ResolutionRoot = resolutionRoot;
            _constructorParameters = constructorParameters;
        }

        protected IResolutionRoot ResolutionRoot { get; }

        public T Create<T>()
        {
            return ResolutionRoot.Get<T>(_constructorParameters);
        }

        public IEnumerable<T> CreateAll<T>()
        {
            return ResolutionRoot.GetAll<T>(_constructorParameters);
        }

        public NamedConstructorParameter<GenericFactory> Set(string parameterName)
        {
            return new NamedConstructorParameter<GenericFactory>(ResolutionRoot, parameterName, _constructorParameters);
        }
    }
}