﻿namespace SecondMonitor.Contracts.TrackMap
{
    public interface IMapsLoaderFactory
    {
        IMapsLoader Create();
    }
}