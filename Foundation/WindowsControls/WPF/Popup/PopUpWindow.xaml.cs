﻿namespace SecondMonitor.WindowsControls.WPF.Popup
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for PopUpWindow.xaml
    /// </summary>
    public partial class PopUpWindow : Window
    {
        public PopUpWindow()
        {
            InitializeComponent();
        }
    }
}
