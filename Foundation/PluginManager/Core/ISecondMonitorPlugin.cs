﻿namespace SecondMonitor.PluginManager.Core
{
    using System.Threading.Tasks;

    public interface ISecondMonitorPlugin
    {
        PluginsManager PluginManager
        {
            get;
            set;
        }

        bool IsDaemon
        {
            get;
        }

        string PluginName { get; }
        bool IsEnabledByDefault { get; }

        Task RunPlugin();
    }
}
