﻿namespace ControlTestingApp.ViewModels
{
    using Ninject;
    using SecondMonitor.Bootstrapping;
    using SecondMonitor.Rating.Application.Championship.ViewModels.Creation.Calendar.Predefined;
    using SecondMonitor.Rating.Application.Championship.ViewModels.Overview;
    using SecondMonitor.Rating.Common.Championship.Calendar.Templates.CalendarGroups;
    using SecondMonitor.ViewModels;
    using SecondMonitor.ViewModels.Factory;

    public class MainTestWindowViewModel : AbstractViewModel
    {

        public PredefinedCalendarSelectionViewModel CalendarTemplateGroupViewModel { get; }

        public SequenceViewTestViewModel SequenceViewTestViewModel { get; }

        public TrophyViewModel TrophyViewModel { get; }
        public MainTestWindowViewModel()
        {
            var kernel = new StandardKernel();
            kernel.LoadCore();
            IViewModelFactory viewModelFactory = kernel.Get<IViewModelFactory>();

            CalendarTemplateGroupViewModel = viewModelFactory.Create<PredefinedCalendarSelectionViewModel>();
            CalendarTemplateGroupViewModel.FromModel(AllGroups.MainGroup);

            SequenceViewTestViewModel = new SequenceViewTestViewModel();
            TrophyViewModel = new TrophyViewModel()
            {
                DriverName = "Fooo Foookovic",
                Position = 3,
            };
        }
    }
}