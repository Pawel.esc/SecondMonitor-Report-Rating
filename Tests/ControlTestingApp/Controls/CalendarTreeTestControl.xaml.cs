﻿using System.Windows.Controls;

namespace ControlTestingApp.Controls
{
    /// <summary>
    /// Interaction logic for CalendarTreeTestControl.xaml
    /// </summary>
    public partial class CalendarTreeTestControl : UserControl
    {
        public CalendarTreeTestControl()
        {
            InitializeComponent();
        }
    }
}
