namespace RemoteTests
{
    using System.IO;
    using Ninject;
    using NUnit.Framework;
    using SecondMonitor.DataModel.Snapshot;
    using SecondMonitor.Remote.Common;
    using SecondMonitor.Remote.Common.Adapter;
    using SecondMonitor.Remote.Common.Model;

    [TestFixture]
    public class DatagramPayloadV2Tests
    {
        private IDatagramPayloadPackerV2 _datagramPayloadPackerV2;
        private IDatagramPayloadUnpackerV2 _datagramPayloadUnpackerV2;

        [OneTimeSetUp]
        public void Bootstrap()
        {
            var remoteCommonModule = new RemoteCommonModule();
            var kernel = new StandardKernel(remoteCommonModule);

            _datagramPayloadPackerV2 = kernel.Get<IDatagramPayloadPackerV2>();
            this._datagramPayloadUnpackerV2 = kernel.Get<IDatagramPayloadUnpackerV2>();
        }

        [Test]
        public void TestHeartbeatPacksCorrectly()
        {
            using (var memoryStream = new MemoryStream())
            {
                _datagramPayloadPackerV2.PackHandshake(memoryStream);
                memoryStream.Seek(0, SeekOrigin.Begin);
                
                var type = this._datagramPayloadUnpackerV2.UnpackKind(memoryStream);
                
                Assert.AreEqual(DatagramPayloadKindV2.Heartbeat, type);
            }
        }

        [Test]
        public void TestSessionStartPacksCorrectly()
        {
            using (var memoryStream = new MemoryStream())
            {
                var simulatorDataSet = new SimulatorDataSet("Test");
                this._datagramPayloadPackerV2.PackSessionStart(memoryStream, simulatorDataSet);
                memoryStream.Seek(0, SeekOrigin.Begin);
                
                var type = this._datagramPayloadUnpackerV2.UnpackKind(memoryStream);

                Assert.AreEqual(DatagramPayloadKindV2.SessionStart, type);

                var payload = this._datagramPayloadUnpackerV2.UnpackSessionStartPayload(memoryStream);
                
                Assert.AreEqual(simulatorDataSet.Source, payload.Source);
            }
        }
    }
}